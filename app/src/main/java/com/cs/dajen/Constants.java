package com.cs.dajen;

import com.cs.dajen.Models.MenuItems;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by CS on 24-04-2017.
 */

public class Constants {

//    static String TEST_URL = "http://csadms.com/DajenServices/";
    static String TEST_URL = "http://dajenapp.ircfood.com/";
//
    public static String IMAGE_URL = "http://dajenapp.ircfood.com/backend/images/";
//    public static String ADDRESS_IMAGE_URL = "http://csadms.com/dajenservices/UploadedImages/";
    public static String ADDRESS_IMAGE_URL = "http://dajenapp.ircfood.com/UploadedImages/";
    public static String INSERT_ORDER_URL = TEST_URL+"api/OrderItems/InsertOrderDetailsVat";

    public static String GET_BANNERS = TEST_URL+"api/OrderItems/TrackOrderBanner?oId=";
    public static String GET_ITEMS = TEST_URL+"api/OrderItems/GetSubCategory?Mid=";
    public static String ADDITIONALS_URL = TEST_URL+"api/OrderItems/GetAdditions?additionalId=";
    public static String REGISTRATION_URL = TEST_URL+"api/UserRegistration/RegisterUser";
    public static String LOGIN_URL = TEST_URL+"api/UserRegistration/Signin/";
    public static String VERIFY_MOBILE = TEST_URL+"api/UserRegistration/CheckMobileNo?MobileNo=";
    public static String FORGOT_PASSWORD_URL = TEST_URL+"api/UserRegistration/SendOTP?MobileNo=";
    public static String RESET_PASSWORD_URL = TEST_URL+"api/UserRegistration/ForgetPassword?Username=";
    public static String SAVED_ADDRESS_URL = TEST_URL+"api/UserRegistration/GetAddressDetails?userid=";
    public static String SAVE_ADDRESS_URL = "http://csadms.com/dajenservices/api/UserRegistration/SaveUserAdderss/";
    public static String DELETE_ADDRESS_URL = TEST_URL+"api/UserRegistration/DeleteUserAddress?addressid=";
    public static String SAVE_ADDRESS_NEW = TEST_URL+"api/UserRegistration/InserUserAddress";
    public static String EDIT_ADDRESS_URL = TEST_URL+"api/UserRegistration/UpdateUserAddress";
    public static String GET_CURRENT_TIME_URL = TEST_URL+"api/StoreInformation/getCurrentTime";
    public static String STORES_URL = TEST_URL+"api/StoreInformation/GetStoreDetails?day=";
//    public static String INSERT_ORDER_URL = TEST_URL+"api/OrderItems/InsertOrder";
    public static String HISTORY_URL = TEST_URL+"api/OrderItems/OrderHistory?uId=";
    public static String TRACK_ORDER_STEPS_URL = TEST_URL+"api/OrderItems/TrackOrder?oId=";
    public static String CANCEL_ORDER_URL = TEST_URL+"api/OrderItems/CancelOrder?oId=";
    public static String INSERT_FAVORITE_ORDER_URL = TEST_URL+"api/OrderItems/IFavOrder?OId=";
    public static String DELETE_FAVORITE_ORDER_URL = TEST_URL+"api/OrderItems/DeleteFavOrder?oId=";
    public static String GET_FAVORITE_ORDER_URL = TEST_URL+"api/OrderItems/FaviorateOrder?uId=";
    public static String ORDERD_DETAILS_URL = TEST_URL+"api/OrderItems/OrderDetails?uId=";
    public static String UPDATE_PROFILE_URL = TEST_URL+"api/UserRegistration/UpdateUserProfile";
    public static String CHANGE_PASSWORD = TEST_URL+"api/UserRegistration/ChangePassword/";
    public static String GET_MESSAGES_URL = TEST_URL+"api/PushMessages/get?userid=";
    public static String UPDATE_MESSAGE_URL = TEST_URL+"api/PushMessages/UpdatePushMsg";
    public static String DELETE_ORDER_FROM_HISTORY = TEST_URL+"api/OrderItems/DeleteOrder?oId=";
    public static String LIVE_TRACKING_URL = TEST_URL+"api/OrderItems/OrderLocation?oId=";
    public static String STORE_DETAILS = TEST_URL+"api/StoreInformation/GetStoreTimings?storeId=";
    public static String UPLOAD_IMAGE = TEST_URL+"api/UserRegistration/PostTest";
    public static String INSERT_RATING = TEST_URL+"api/OrderItems/PostInsertRating";
    public static String GET_PROMOS_URL = TEST_URL+"api/DajenPromotions/GetPromotions?userId=";
//    public static String UPLOAD_IMAGE = TEST_URL+"api/UserRegistration/SaveImage";


    public static ArrayList<MenuItems> menuItems = new ArrayList<>();
    public static ArrayList<MenuItems> ChickenMenu = new ArrayList<>();
    public static ArrayList<MenuItems> BBQMenu = new ArrayList<>();
    public static int BBQPosition = 0;
    public static int ItemPosition = 0;
    public static int Itemid = 0;
    public static String CurrentOrderActivity = "";
    public static String CurrentMoreActivity = "";
    public static String COMMENTS = "";
    public static String ORDER_TYPE = "";
    public static String MENU_TYPE = "";
    public static String TRACK_ID = "";

    public static DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public static String convertToArabic(String value)
    {
        String newValue = (value
                .replaceAll("١","1" ).replaceAll("٢","2" )
                .replaceAll("٣","3" ).replaceAll("٤","4" )
                .replaceAll("٥","5" ).replaceAll("٦","6" )
                .replaceAll("٧","7" ).replaceAll("٨","8")
                .replaceAll("٩","9" ).replaceAll("٠","0" )
                .replaceAll("٫","."));
        return newValue;
    }
}
