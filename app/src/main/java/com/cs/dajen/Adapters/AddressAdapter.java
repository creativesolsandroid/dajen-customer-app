package com.cs.dajen.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.Models.Address;
import com.cs.dajen.R;
import com.cs.dajen.activity.EditAddressActivity;

import java.util.ArrayList;

/**
 * Created by CS on 20-06-2016.
 */
public class AddressAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Address> addressList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id;
    String language;
    //public ImageLoader imageLoader;

    public AddressAdapter(Context context, ArrayList<Address> addressList, String language) {
        this.context = context;
        this.addressList = addressList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return addressList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        TextView flatNo, landmark, address, addressName, address_type_name;
        ImageView addressType, edit;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")) {
                convertView = inflater.inflate(R.layout.address_list, null);
            }
            else{
                convertView = inflater.inflate(R.layout.address_list_ar, null);
            }

            holder.flatNo = (TextView) convertView
                    .findViewById(R.id.flat_number);
            holder.address_type_name = (TextView) convertView
                    .findViewById(R.id.address_type_name);
            holder.landmark = (TextView) convertView
                    .findViewById(R.id.landmark_name);
            holder.address = (TextView) convertView
                    .findViewById(R.id.full_address);
            holder.addressName = (TextView) convertView.findViewById(R.id.address_name);
            holder.addressType = (ImageView) convertView.findViewById(R.id.address_type);
            holder.edit = (ImageView) convertView.findViewById(R.id.address_edit);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(!addressList.get(position).getHouseNo().equals("null")) {
            holder.flatNo.setText("" + addressList.get(position).getHouseNo());
        }
        else{
            holder.flatNo.setText("");
        }
        if(!addressList.get(position).getLandmark().equals("null")) {
            holder.landmark.setText("" + addressList.get(position).getLandmark());
        }
        else{
            holder.landmark.setText("");
        }
        if(!addressList.get(position).getAddress().equals("null")) {
            if(language.equalsIgnoreCase("En")) {
                holder.address.setText("Address : " + addressList.get(position).getAddress());
            }
            else{
                holder.address.setText("العنوان : " + addressList.get(position).getAddress());
            }
        }
        else{
            holder.address.setText("");
        }
        if(!addressList.get(position).getAddressName().equals("null")) {
            holder.addressName.setText("" + addressList.get(position).getAddressName());
        }
        else{
            holder.addressName.setText("");
        }

        if(language.equalsIgnoreCase("En")) {
            if(addressList.get(position).getAddressType().equalsIgnoreCase("1")){
                holder.address_type_name.setText("Home : ");
            }else if(addressList.get(position).getAddressType().equalsIgnoreCase("2")){
                holder.address_type_name.setText("Office : ");
            }else{
                holder.address_type_name.setText("Other : ");
            }
        }else{

            if(addressList.get(position).getAddressType().equalsIgnoreCase("1")){
                holder.address_type_name.setText(context.getResources().getString(R.string.address_home_ar));
            }else if(addressList.get(position).getAddressType().equalsIgnoreCase("2")){
                holder.address_type_name.setText(context.getResources().getString(R.string.address_office_ar));
            }else{
                holder.address_type_name.setText("Other : ");
            }
        }

        if(!addressList.get(position).getAddressImage().equals(null) && !addressList.get(position).getAddressImage().equals("null") &&addressList.get(position).getAddressImage().length()>0){
            Glide.with(context).load(Constants.ADDRESS_IMAGE_URL+addressList.get(position).getAddressImage()).into(holder.addressType);
        }
        else{
                if(addressList.get(position).getAddressType().equalsIgnoreCase("1")){
                    holder.addressType.setImageResource(R.drawable.address_home);
//                }else if(addressList.get(position).getAddressType().equalsIgnoreCase("2")){
//                    holder.addressType.setImageResource(R.drawable.address_office);
                }else{
                    holder.addressType.setImageResource(R.drawable.address_office);
                }
        }

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EditAddressActivity.class);
                intent.putExtra("address", addressList.get(position).getAddress());
                intent.putExtra("latitude", addressList.get(position).getLatitude());
                intent.putExtra("longitude", addressList.get(position).getLongitude());
                intent.putExtra("image", addressList.get(position).getAddressImage());
                intent.putExtra("id", addressList.get(position).getId());
                if(!addressList.get(position).getLandmark().equals("null")) {
                    intent.putExtra("landmark", addressList.get(position).getLandmark());
                }
                else{
                    intent.putExtra("landmark", "");
                }
                intent.putExtra("address_type", addressList.get(position).getAddressType());
                intent.putExtra("house_no", addressList.get(position).getHouseNo());
                intent.putExtra("house_name", addressList.get(position).getAddressName());
                ((Activity)context).startActivityForResult(intent, 3);
            }
        });
        return convertView;
    }
}