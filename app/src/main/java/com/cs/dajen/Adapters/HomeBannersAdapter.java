package com.cs.dajen.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.Models.Banners;
import com.cs.dajen.R;

import java.util.ArrayList;

/**
 * Created by cs android on 23-02-2017.
 */

public class HomeBannersAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ImageView Banner;
    LinearLayout layout;
    Context context;
    Activity parentActivity;

    String language;
    ArrayList<Banners> banners;

    public HomeBannersAdapter(Context context, ArrayList<Banners> banners, String language, Activity parentActivity) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.offerBanner = false;
        this.context = context;
        this.language = language;
        this.banners = banners;
        this.parentActivity = parentActivity;
    }

    @Override
    public int getCount() {
        return banners.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = null;
        itemView = mLayoutInflater.inflate(R.layout.home_banners_list, container, false);

        Banner=(ImageView)itemView.findViewById(R.id.banner_image);
        layout=(LinearLayout) itemView.findViewById(R.id.layout);

        Glide.with(parentActivity).load(Constants.IMAGE_URL+ banners.get(position).getImageName())
                .into(Banner);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
