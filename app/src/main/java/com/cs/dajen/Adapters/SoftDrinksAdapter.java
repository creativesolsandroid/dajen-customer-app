package com.cs.dajen.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.R;
import com.cs.dajen.activity.InsideProductActivity;

import java.util.ArrayList;

/**
 * Created by CS on 27-04-2017.
 */

public class SoftDrinksAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Additionals> orderList = new ArrayList<>();
    String language;
    private DataBaseHelper myDbHelper;

    public SoftDrinksAdapter(Context context, ArrayList<Additionals> orderList, String language) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        ImageView image, tick ;
        LinearLayout item_layout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.soft_drinks_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.soft_drinks_list_ar, null);
            }

            myDbHelper = new DataBaseHelper(context);

            holder.image = (ImageView) convertView.findViewById(R.id.fanta);
            holder.tick = (ImageView) convertView.findViewById(R.id.fanta_tick);
            holder.item_layout = (LinearLayout) convertView.findViewById(R.id.drinks_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(InsideProductActivity.refreshCount==0) {
            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(position).getImages()).into(holder.image);
            Log.i("TAG","Image "+ Constants.IMAGE_URL + orderList.get(position).getImages() );

        }



        if(InsideProductActivity.drinks_pos==position){
            holder.tick.setVisibility(View.VISIBLE);
        }
        else{
            holder.tick.setVisibility(View.INVISIBLE);
        }

        holder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InsideProductActivity.refreshCount = 1;
                InsideProductActivity.drinks_pos = position;
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
