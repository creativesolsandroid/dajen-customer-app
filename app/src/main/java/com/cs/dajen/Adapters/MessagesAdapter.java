package com.cs.dajen.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cs.dajen.Models.Messages;
import com.cs.dajen.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by SKT on 22-02-2016.
 */
public class MessagesAdapter extends BaseAdapter {

    public Context context;
    public LayoutInflater inflater;
    ArrayList<Messages> favOrderList = new ArrayList<>();
    int pos;
    public static String subscriptions = "no";
    String id, language;
    //public ImageLoader imageLoader;

    public MessagesAdapter(Context context, ArrayList<Messages> favOrderList, String language) {
        this.context = context;
        this.favOrderList = favOrderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        //DBcontroller = new DatabaseHandler(context);

        //imageLoader=new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return favOrderList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        // menu type count
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;
        if(favOrderList.get(position).getIsRead().equals("true")){
            type = 1;
        }
        else{
            type = 1;
        }
        // current menu type
        return type;
    }

    public static class ViewHolder {
        TextView status, date,desc;
        ImageView imageIcon;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        pos = position;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
                convertView = inflater.inflate(R.layout.messages_list, null);
            }
            else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.messages_list_ar, null);
            }


            holder.status = (TextView) convertView
                    .findViewById(R.id.messages_title);
            holder.date = (TextView) convertView
                    .findViewById(R.id.messages_date);
            holder.desc = (TextView) convertView
                    .findViewById(R.id.messages_text);
            holder.imageIcon = (ImageView) convertView
                    .findViewById(R.id.messages_icon);



            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


//        holder.status.setText(favOrderList.get(position).getFavoriteName());
        holder.desc.setText(favOrderList.get(position).getPushMessage());
//

        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        Date dateObj = null;
        String[] parts = favOrderList.get(position).getSentDate().split(" ");
        try {
            dateObj = curFormater.parse(favOrderList.get(position).getSentDate().replace("  ", " 0"));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM ''yy 'at' hh:mm a", Locale.US);
        String date = postFormater.format(dateObj);

        holder.date.setText(date);
        if(language.equalsIgnoreCase("En")) {
            if (favOrderList.get(position).getPushNotificationType().equals("1")) {
                holder.status.setText("Confirmed");
                holder.imageIcon.setImageResource(R.drawable.confirmed_done);
            } else if (favOrderList.get(position).getPushNotificationType().equals("2")) {
                holder.status.setText("Ready");
                holder.imageIcon.setImageResource(R.drawable.ready_done);
            } else if (favOrderList.get(position).getPushNotificationType().equals("3")) {
                holder.status.setText("Served");
                holder.imageIcon.setImageResource(R.drawable.delivered_done);
            } else if (favOrderList.get(position).getPushNotificationType().equals("4")) {
                holder.status.setText("Cancel");
            holder.imageIcon.setImageResource(R.drawable.step_cancel);
            } else if (favOrderList.get(position).getPushNotificationType().equals("5")) {
                holder.status.setText("On The Way");
                holder.imageIcon.setImageResource(R.drawable.ontheway_done);
            }else if (favOrderList.get(position).getPushNotificationType().equals("6")) {
                holder.status.setText("Delivered");
                holder.imageIcon.setImageResource(R.drawable.delivered_done);
            }
            else if (favOrderList.get(position).getPushNotificationType().equals("7")) {
                holder.status.setText("Offer");
//                holder.imageIcon.setImageResource(R.drawable.notification7);
            }else if (favOrderList.get(position).getPushNotificationType().equals("8")) {
                holder.status.setText("Welcome");
                holder.imageIcon.setImageResource(R.drawable.confirmed_done);
            }
        }
        else if(language.equalsIgnoreCase("Ar")){
            if (favOrderList.get(position).getPushNotificationType().equals("1")) {
                holder.status.setText("تأكيد");
                holder.imageIcon.setImageResource(R.drawable.confirmed_done);
            } else if (favOrderList.get(position).getPushNotificationType().equals("2")) {
                holder.status.setText("جاهز");
                holder.imageIcon.setImageResource(R.drawable.ready_done);
            } else if (favOrderList.get(position).getPushNotificationType().equals("3")) {
                holder.status.setText("تم استلامه");
                holder.imageIcon.setImageResource(R.drawable.delivered_done);
            } else if (favOrderList.get(position).getPushNotificationType().equals("4")) {
                holder.status.setText("الغاء");
                holder.imageIcon.setImageResource(R.drawable.step_cancel);
            } else if (favOrderList.get(position).getPushNotificationType().equals("5")) {
                holder.status.setText("في الطريق");
                holder.imageIcon.setImageResource(R.drawable.ontheway_done);
            }else if (favOrderList.get(position).getPushNotificationType().equals("6")) {
                holder.status.setText("تم التوصيل");
                holder.imageIcon.setImageResource(R.drawable.delivered_done);
            }else if (favOrderList.get(position).getPushNotificationType().equals("7")) {
                holder.status.setText(" عرض خاص");
//                holder.imageIcon.setImageResource(R.drawable.notification7_arabic);
            }else if (favOrderList.get(position).getPushNotificationType().equals("8")) {
                holder.status.setText("مرحباً ");
                holder.imageIcon.setImageResource(R.drawable.confirmed_done);
            }
        }

//        if(favOrderList.get(position).getIsRead().equals("true") || favOrderList.get(position).getPushNotificationType().equals("5")){
//            holder.dot.setVisibility(View.VISIBLE);
//        }else {
//            holder.dot.setVisibility(View.INVISIBLE);
//        }



        return convertView;
    }
}