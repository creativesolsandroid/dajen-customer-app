package com.cs.dajen.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.R;
import com.cs.dajen.activity.BBQProductDetails;

import java.util.ArrayList;


/**
 * Created by cs android on 15-02-2017.
 */

public class BBQSticksAdapter extends BaseAdapter {
    public Context context;
    public LayoutInflater inflater;
    ArrayList<Additionals> orderList = new ArrayList<>();
    private DataBaseHelper myDbHelper;
    Activity parentActivity;
    String language;
    AlertDialog customDialog;

    public BBQSticksAdapter(Context context, ArrayList<Additionals> orderList, String language, Activity parentActivity) {
        this.context = context;
        this.orderList = orderList;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.language = language;
        this.parentActivity = parentActivity;

        myDbHelper = new DataBaseHelper(context);

    }


    public int getCount() {
        return orderList.size();

    }

    public String getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        TextView title,qty;
        ImageView mainImage,plus,minus;
        RelativeLayout GridLayout;

    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            if(language.equalsIgnoreCase("En")){
            convertView = inflater.inflate(R.layout.bbq_stick_list, null);
            }else if(language.equalsIgnoreCase("Ar")){
                convertView = inflater.inflate(R.layout.bbq_stick_list_ar, null);
            }

            holder.title = (TextView) convertView.findViewById(R.id.grid_title);
            holder.qty = (TextView) convertView.findViewById(R.id.stick_count);
            holder.mainImage = (ImageView) convertView.findViewById(R.id.grid_image);
            holder.plus = (ImageView) convertView.findViewById(R.id.stick_plus);
            holder.minus = (ImageView) convertView.findViewById(R.id.stick_minus);
            holder.GridLayout = (RelativeLayout) convertView.findViewById(R.id.item_layout);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(BBQProductDetails.sticktsQty!=null){
            int count = 0;
            for(int i=0; i<BBQProductDetails.sticktsQty.size(); i++){
                if(BBQProductDetails.sticktsQty.get(i).equals(orderList.get(position).getAdditionalName())){
                    count = count + 1;
                }
            }
            if(count>0) {
                String pos = Integer.toString(position);
                if (!BBQProductDetails.sticksPos.contains(pos)) {
                    BBQProductDetails.sticksPos.add(pos);
                }
                holder.qty.setText("" + count);
                holder.qty.setAlpha(1);
            }
            else{
                String pos = Integer.toString(position);
                if (BBQProductDetails.sticksPos.contains(pos)) {
                    BBQProductDetails.sticksPos.remove(pos);
                }
                holder.qty.setText("0");
                holder.qty.setAlpha(0.5f);
            }
        }
        else{

            holder.qty.setText("0");
            holder.qty.setAlpha(0.5f);
        }

        BBQProductDetails.boxSticksQty.setText(BBQProductDetails.sticksCount+"/9");
        if(language.equalsIgnoreCase("En")) {
            holder.title.setText(orderList.get(position).getAdditionalName());
        }
        else{
            holder.title.setText(orderList.get(position).getAdditionalNameAr());
        }

        if(BBQProductDetails.refreshCount == 0) {
            Glide.with(context).load(Constants.IMAGE_URL + orderList.get(position).getImages()).into(holder.mainImage);
        }

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(BBQProductDetails.sticksCount<9) {
                    BBQProductDetails.sticksCount = BBQProductDetails.sticksCount +1;
                    BBQProductDetails.sticktsQty.add(orderList.get(position).getAdditionalName());
                    BBQProductDetails.refreshCount = 1;
                    notifyDataSetChanged();
                }
                else{

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(parentActivity, android.R.style.Theme_Material_Light_Dialog));

                    if (language.equalsIgnoreCase("En")) {
                        // set title
                        alertDialogBuilder.setTitle(R.string.app_name);

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(R.string.max_9_sticks)
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    } else if (language.equalsIgnoreCase("Ar")) {
                        // set title
                        alertDialogBuilder.setTitle(R.string.app_name);

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(R.string.max_9_sticks_ar)
                                .setCancelable(false)
                                .setPositiveButton("تم", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                    }

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Integer.parseInt(holder.qty.getText().toString())>0) {
                    BBQProductDetails.sticksCount = BBQProductDetails.sticksCount -1;
                    BBQProductDetails.sticktsQty.remove(orderList.get(position).getAdditionalName());
                    BBQProductDetails.refreshCount = 1;
                    notifyDataSetChanged();
                }
            }
        });

        if(BBQProductDetails.sticksCount==0){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick0));
        }
        else if(BBQProductDetails.sticksCount==1){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick1));
        }
        else if(BBQProductDetails.sticksCount==2){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick2));
        }
        else if(BBQProductDetails.sticksCount==3){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick3));
        }
        else if(BBQProductDetails.sticksCount==4){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick4));
        }
        else if(BBQProductDetails.sticksCount==5){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick5));
        }
        else if(BBQProductDetails.sticksCount==6){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick6));
        }
        else if(BBQProductDetails.sticksCount==7){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick7));
        }
        else if(BBQProductDetails.sticksCount==8){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick8));
        }
        else if(BBQProductDetails.sticksCount==9){
            BBQProductDetails.sticksImage.setImageDrawable(context.getResources().getDrawable(R.drawable.stick9));
        }


        return convertView;
    }
}