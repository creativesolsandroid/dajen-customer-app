package com.cs.dajen.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Adapters.TrackItemsAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.GPSTracker;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.OrderHistory;
import com.cs.dajen.Models.Rating;
import com.cs.dajen.Models.RatingDetails;
import com.cs.dajen.Models.TrackItems;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.cs.dajen.SplashScreenActivity;
import com.cs.dajen.widgets.CustomListView;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;


public class TrackOrder extends AppCompatActivity {

    TextView emptyView;
    LinearLayout acceptImage;
//    View viewcancel;
    TextView orderTime, acceptTime, readyTime, onthewayTime, deliveryTime, expectedTime;
    private Timer timer = new Timer();

    public static String driverName, driverNumber, driverId;
    ScrollView scrollView;

    ArrayList<OrderHistory> orderHistory;
    Double lat, longi;
    String latitude, longitude, phone, totalPrice, subTotal, vatCharges, vatPercentage;;
    String orderNumber, OrderStatus, StoreName, StoreNameAr, OrderType = "", StoreAddressAr, StoreAddress, TrackingTime, ExpectedTime, address;
    String orderId, ordernumber;
    SharedPreferences userPrefs;
    FrameLayout mframe_layout;
    String userId;
    SharedPreferences languagePrefs;
    SharedPreferences orderPrefs;
    SharedPreferences.Editor orderPrefsEditor;
    String language;

    GPSTracker gps;
    private static final String[] LOCATION_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int INITIAL_REQUEST = 1337;
    private static final int LOCATION_REQUEST = 4;
    private static final String[] PHONE_PERMS = {
            Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    int ratingFromService;
    RelativeLayout rate_layout;
    MaterialRatingBar ratingBar;
    TextView cancel;
    final ArrayList<RatingDetails> rateArray = new ArrayList<>();

    private ArrayList<TrackItems> ItemsList = new ArrayList<>();

    public static TextView totalQty;
    ListView orderListView;
    TextView orderId_tv, totalAmount, promoAmount, netAmount, vatAmount, mamount;;
    RelativeLayout summary_layout,list_expand;
    ImageView plus;
    Boolean show=true;
    TrackItemsAdapter mOrderAdapter;
    AlertDialog customDialog;
    ImageView lineAboveOrder, lineBelowOrder, lineAboveConfirmed, lineBelowConfirmed, lineAboveReady, lineBelowReady, lineAboveOnTheWay, lineBelowOnTheWay, lineAboveDelivered;
    ImageView OrderImage, ConfirmedImage, ReadyImage, OntheWayImage, DeliveredImage;
    TextView OrderTitle, ConfirmedTitle, ReadyTitle, OntheWayTitle, DeliveredTitle;
    TextView OrderNumberText, ExpTimeText, OrderNumber;
    TextView ConfirmedText, ReadyText, OntheWayText, DeliveredText;
    RelativeLayout cancelOrder,TrackShare, TrackCall, TrackLocation, OnTheWayLayout, ReadyLayout, DeliveredLayout;
    View OnTheWayLine;
    ImageView back_btn;
    TextView NewOrder;
    private DataBaseHelper myDbHelper;
    Animation animZoomIn,animZoomOut;
    String screen = "track";
    LinearLayout share_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.track_order);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.track_order_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        orderPrefs = getSharedPreferences("ORDER_PREFS", Context.MODE_PRIVATE);
        orderPrefsEditor = orderPrefs.edit();

        orderId = Constants.TRACK_ID;

        myDbHelper = new DataBaseHelper(getApplicationContext());

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;

        try {
            screen = getIntent().getStringExtra("screen");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }
        Constants.CurrentOrderActivity = "tracking";
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());


        emptyView = (TextView) findViewById(R.id.empty_view);
        cancelOrder = (RelativeLayout) findViewById(R.id.cancel_order);
        TrackLocation = (RelativeLayout) findViewById(R.id.track_location);
        TrackCall = (RelativeLayout) findViewById(R.id.track_call);
        TrackShare = (RelativeLayout) findViewById(R.id.track_share);
        OnTheWayLayout = (RelativeLayout) findViewById(R.id.ontheway_layout);
        ReadyLayout = (RelativeLayout) findViewById(R.id.ready_layout);
        DeliveredLayout = (RelativeLayout) findViewById(R.id.delivered_layout);
        share_layout = (LinearLayout) findViewById(R.id.share_layout);
        OnTheWayLine = (View) findViewById(R.id.line_ontheway);

        mframe_layout = (FrameLayout) findViewById(R.id.frame_layout);

        scrollView = (ScrollView) findViewById(R.id.scrollView);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        NewOrder = (TextView) findViewById(R.id.new_order);

        orderTime = (TextView) findViewById(R.id.order_time);
        acceptTime = (TextView) findViewById(R.id.confirmed_time);
        readyTime = (TextView) findViewById(R.id.ready_time);
        onthewayTime = (TextView) findViewById(R.id.ontheway_time);
        deliveryTime = (TextView) findViewById(R.id.delivered_time);
        expectedTime = (TextView) findViewById(R.id.exp_time);
        OrderNumber = (TextView) findViewById(R.id.order_number);
        OrderNumberText = (TextView) findViewById(R.id.number);
        ExpTimeText = (TextView) findViewById(R.id.expected_time_text);

        OrderTitle = (TextView) findViewById(R.id.order_title);
        ConfirmedTitle = (TextView) findViewById(R.id.confirmed_title);
        ReadyTitle = (TextView) findViewById(R.id.ready_title);
        OntheWayTitle = (TextView) findViewById(R.id.ontheway_title);
        DeliveredTitle = (TextView) findViewById(R.id.delivered_title);

        ConfirmedText = (TextView) findViewById(R.id.confirmed_text);
        ReadyText = (TextView) findViewById(R.id.ready_text);
        OntheWayText = (TextView) findViewById(R.id.ontheway_text);
        DeliveredText = (TextView) findViewById(R.id.delivered_text);

        OrderImage = (ImageView) findViewById(R.id.order);
        ConfirmedImage = (ImageView) findViewById(R.id.confirmed);
        ReadyImage = (ImageView) findViewById(R.id.ready);
        OntheWayImage = (ImageView) findViewById(R.id.ontheway);
        DeliveredImage = (ImageView) findViewById(R.id.delivered);

        lineAboveOrder = (ImageView) findViewById(R.id.line_above_order);
        lineAboveConfirmed = (ImageView) findViewById(R.id.line_above_confirmed);
        lineAboveReady = (ImageView) findViewById(R.id.line_above_ready);
        lineAboveOnTheWay = (ImageView) findViewById(R.id.line_above_ontheway);
        lineAboveDelivered = (ImageView) findViewById(R.id.line_above_delivered);

        lineBelowOrder = (ImageView) findViewById(R.id.line_below_order);
        lineBelowConfirmed = (ImageView) findViewById(R.id.line_below_confirmed);
        lineBelowReady = (ImageView) findViewById(R.id.line_below_ready);
        lineBelowOnTheWay = (ImageView) findViewById(R.id.line_below_ontheway);

        ratingBar = (MaterialRatingBar) findViewById(R.id.ratingBar1);
        cancel = (TextView) findViewById(R.id.skip_rating);
        rate_layout = (RelativeLayout) findViewById(R.id.rate_layout);
//        rate_layout.setVisibility(View.GONE);

//        viewcancel = (View) findViewById(R.id.view_cancel);

//        acceptImage = (LinearLayout) findViewById(R.id.accept_image);

        orderListView = (CustomListView) findViewById(R.id.horizontal_listview);
        orderId_tv = (TextView) findViewById(R.id.orderId);
        totalQty = (TextView) findViewById(R.id.totalQty);
        mamount = (TextView) findViewById(R.id.subAmount);
        netAmount = (TextView) findViewById(R.id.netAmount);
        vatAmount = (TextView) findViewById(R.id.vatAmount);
        totalAmount = (TextView) findViewById(R.id.totalAmount);
        promoAmount = (TextView) findViewById(R.id.promoAmount);
        netAmount = (TextView) findViewById(R.id.netAmount);
        plus = (ImageView) findViewById(R.id.list_plus);
        summary_layout = (RelativeLayout) findViewById(R.id.summary_layout);
        list_expand = (RelativeLayout) findViewById(R.id.list_expand);
        list_expand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(show){
                    orderListView.setVisibility(View.VISIBLE);
                    summary_layout.setVisibility(View.VISIBLE);
                    plus.setImageResource(R.drawable.track_minus);
                    show=false;
                }
                else {
                    orderListView.setVisibility(View.GONE);
                    summary_layout.setVisibility(View.GONE);
                    plus.setImageResource(R.drawable.track_plus);
                    show=true;
                }
            }
        });

        summary_layout.setVisibility(View.GONE);
        orderListView.setVisibility(View.GONE);

        mOrderAdapter = new TrackItemsAdapter(TrackOrder.this, ItemsList, language);
        orderListView.setAdapter(mOrderAdapter);

//        Animation zoomin= AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_in);
//        Animation zoomout=AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_out);
//
//        orderImage.setAnimation(zoomin);
//        orderImage.setAnimation(zoomout);

//        orderImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                orderImage.startAnimation(animZoomIn);
//            }
//        });

        driverName = "";
        driverNumber = "";
        driverId = "";
        new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL + orderId + "&uId=" + userId);

        timer.schedule(new MyTimerTask(), 30000, 30000);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(screen.equalsIgnoreCase("completed")){
                    FooterActivity.tabBarPosition = 1;
                    Intent intent = new Intent(TrackOrder.this, OrderActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
                else {
                    startActivity(new Intent(TrackOrder.this, OrderHistoryActivity.class));
                    finish();
                }
            }
        });

        NewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TrackOrder.this, MenuType.class);
                intent.putExtra("startWith", 2);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        TrackShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessLocation()) {
                        requestPermissions(LOCATION_PERMS, LOCATION_REQUEST);
                    } else {
                        getGPSCoordinates();
                    }
                } else {
                    getGPSCoordinates();
                }
            }
        });

        TrackLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (!OrderType.equalsIgnoreCase("Delivery")) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
//                }
            }
        });

        TrackCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int currentapiVersion = Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                        }
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + phone));
                        if (ActivityCompat.checkSelfPermission(TrackOrder.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + phone));
                    startActivity(intent);
                }
            }
        });

        cancelOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrder.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                if(language.equalsIgnoreCase("En")) {
                    title.setText(getResources().getString(R.string.dajen));
                    yes.setText(getResources().getString(R.string.yes));
                    no.setText(getResources().getString(R.string.no));
                    desc.setText(getResources().getString(R.string.cancel_order));
                }
                else{
                    title.setText(getResources().getString(R.string.dajen_ar));
                    yes.setText(getResources().getString(R.string.yes_ar));
                    no.setText(getResources().getString(R.string.no_ar));
                    desc.setText(getResources().getString(R.string.cancel_order_ar));
                }

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                        JSONObject parent = new JSONObject();
                        try {

                            JSONArray mainItem = new JSONArray();

                            JSONObject mainObj = new JSONObject();
                            mainObj.put("OrderId", orderId);
                            mainObj.put("UserId", userId);
                            mainObj.put("Device_token", SplashScreenActivity.regId);

                            mainItem.put(mainObj);

                            parent.put("CancelOrder", mainItem);
                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                        new CancelOrder().execute(Constants.CANCEL_ORDER_URL+orderId);
                    }
                });
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                            }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
        });
    }

    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL + orderId + "&uId=" + userId);
                    Log.e("orderid", orderId);
                    Log.e("userid", userId);

                }
            });
        }
    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        timer.cancel();
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    public class GetOrderTrackDetails extends AsyncTask<String, Integer, String> {
        String networkStatus;
        MaterialDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrder.this);
            dialog = new MaterialDialog.Builder(TrackOrder.this)
                    .title(R.string.app_name)
                    .content("Reloading...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.e("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                    finish();
                } else {
                    if (result.equals("")) {
                        scrollView.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                        cancelOrder.setVisibility(View.INVISIBLE);
//                        share.setVisibility(View.GONE);
                        Toast.makeText(TrackOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                String failure = jo.getString("Failure");
                                scrollView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                                cancelOrder.setVisibility(View.INVISIBLE);
                                share_layout.setVisibility(View.GONE);
//                                share.setVisibility(View.GONE);
                                list_expand.setVisibility(View.GONE);
                            } catch (JSONException jse) {

                                try {
                                    JSONArray ja = jo.getJSONArray("Received");
                                    cancelOrder.setEnabled(true);
                                    cancelOrder.setClickable(true);
                                    cancelOrder.setAlpha(1f);
                                    JSONObject jo1 = ja.getJSONObject(0);
                                    orderNumber = jo1.getString("InvoiceNo");
                                    orderId = jo1.getString("OrderId");
                                    String[] orderNum = orderNumber.split("-");
                                    orderId_tv.setText("#"+orderNum[1]);
                                    OrderStatus = "Order!";
                                    StoreName = jo1.getString("StoreName");
                                    StoreNameAr = jo1.getString("StoreName_ar");
                                    StoreAddress = jo1.getString("StoreAddress");
                                    StoreAddressAr = jo1.getString("StoreAddress_ar");
                                    OrderType = jo1.getString("OrderType");
                                    TrackingTime = jo1.getString("TrackingTime");
                                    ExpectedTime = jo1.getString("ExpectedTime");
                                    latitude = jo1.getString("Latitude");
                                    longitude = jo1.getString("Longitude");
                                    phone = jo1.getString("phone");
                                    address = jo1.getString("Address");
                                    totalPrice = jo1.getString("Total_Price");
                                    subTotal = jo1.getString("SubTotal");
                                    vatCharges = jo1.getString("VatCharges");
                                    vatPercentage = jo1.getString("VatPercentage");
                                    float promoAmt = jo1.getInt("PromoAmt");
                                    float total = Float.parseFloat(Constants.convertToArabic(subTotal)) - Float.parseFloat(String.valueOf(promoAmt));
                                    promoAmount.setText(""+Constants.decimalFormat.format(Float.parseFloat(String.valueOf(promoAmt))));
                                    mamount.setText(""+Constants.decimalFormat.format(Float.parseFloat(String.valueOf(total))));
                                    vatAmount.setText(""+Constants.decimalFormat.format(Float.parseFloat(vatCharges)));
                                    netAmount.setText(""+Constants.decimalFormat.format(Float.parseFloat(String.valueOf(totalPrice))));
                                    totalAmount.setText(""+Constants.decimalFormat.format(Float.parseFloat(subTotal)));

//                                    int promoAmt = Integer.parseInt(jo1.getString("PromoAmt"));
//                                    int total = promoAmt+jo1.getInt("Total_Price");
//                                    netAmount.setText(""+totalPrice);
//                                    promoAmount.setText(""+promoAmt);
//                                    totalAmount.setText(""+total);


                                    if(Integer.parseInt(jo1.getString("QuantityCount"))<10) {
                                        totalQty.setText("0"+jo1.getString("QuantityCount"));
                                    }
                                    else{
                                        totalQty.setText(jo1.getString("QuantityCount"));
                                    }

                                    if(OrderType.equalsIgnoreCase("delivery")){
                                        TrackLocation.setVisibility(View.GONE);
                                    }
                                    else{
                                        TrackLocation.setVisibility(View.VISIBLE);
                                    }

                                    ratingFromService = jo1.getInt("Rating");

                                    ItemsList.clear();
                                    JSONArray itemsArray = jo1.getJSONArray("items");
                                    for(int i=0; i<itemsArray.length(); i++){
                                        TrackItems ti = new TrackItems();

                                        JSONObject jo2 = itemsArray.getJSONObject(i);
                                        ti.setItem_name(jo2.getString("ItmName"));
                                        ti.setItem_name_ar(jo2.getString("ItmName_ar"));
                                        ti.setPrice(jo2.getString("price"));
                                        ti.setQty(jo2.getString("Qty"));

                                        ItemsList.add(ti);
                                    }

                                    if(ratingFromService == 0) {
                                        try {
                                            rateArray.clear();
                                            RatingDetails rd = new RatingDetails();
                                            JSONArray jA = jo1.getJSONArray("Ratings");
                                            JSONObject obj = jA.getJSONObject(0);
                                            rd.setGivenrating("5");
                                            JSONArray ratingArray = obj.getJSONArray("5");
                                            ArrayList<Rating> ratingsArrayList = new ArrayList<>();
                                            for (int i = 0; i < ratingArray.length(); i++) {
                                                JSONObject object1 = ratingArray.getJSONObject(i);
                                                Rating rating = new Rating();
                                                rating.setRatingId(object1.getString("RatingId"));
                                                rating.setComment(object1.getString("Comment_En"));
                                                rating.setComment_ar(object1.getString("Comment_Ar"));
                                                rating.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(rating);

                                            }
                                            rd.setArrayList(ratingsArrayList);
                                            JSONArray ratingArray1 = obj.getJSONArray("4");
                                            rd.setGivenrating("4");
                                            for (int i = 0; i < ratingArray1.length(); i++) {
                                                JSONObject object1 = ratingArray1.getJSONObject(i);
                                                Rating rating = new Rating();
                                                rating.setRatingId(object1.getString("RatingId"));
                                                rating.setComment(object1.getString("Comment_En"));
                                                rating.setComment_ar(object1.getString("Comment_Ar"));
                                                rating.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(rating);
                                            }
                                            rd.setArrayList(ratingsArrayList);
                                            JSONArray ratingArray2 = obj.getJSONArray("3");
                                            rd.setGivenrating("3");
                                            for (int i = 0; i < ratingArray2.length(); i++) {
                                                JSONObject object1 = ratingArray2.getJSONObject(i);
                                                Rating rating = new Rating();
                                                rating.setRatingId(object1.getString("RatingId"));
                                                rating.setComment(object1.getString("Comment_En"));
                                                rating.setComment_ar(object1.getString("Comment_Ar"));
                                                rating.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(rating);
                                            }
                                            rd.setArrayList(ratingsArrayList);

                                            JSONArray ratingArray3 = obj.getJSONArray("2");
                                            rd.setGivenrating("2");
                                            for (int i = 0; i < ratingArray3.length(); i++) {
                                                JSONObject object1 = ratingArray3.getJSONObject(i);
                                                Rating rating = new Rating();
                                                rating.setRatingId(object1.getString("RatingId"));
                                                rating.setComment(object1.getString("Comment_En"));
                                                rating.setComment_ar(object1.getString("Comment_Ar"));
                                                rating.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(rating);
                                            }
                                            rd.setArrayList(ratingsArrayList);
                                            JSONArray ratingArray4 = obj.getJSONArray("1");
                                            rd.setGivenrating("1");
                                            for (int i = 0; i < ratingArray4.length(); i++) {
                                                JSONObject object1 = ratingArray4.getJSONObject(i);
                                                Rating rating = new Rating();
                                                rating.setRatingId(object1.getString("RatingId"));
                                                rating.setComment(object1.getString("Comment_En"));
                                                rating.setComment_ar(object1.getString("Comment_Ar"));
                                                rating.setRating(object1.getString("Rating"));
                                                ratingsArrayList.add(rating);
                                            }
                                            rd.setArrayList(ratingsArrayList);

                                            rateArray.add(rd);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    String[] order_no = orderNumber.split("-");
                                    orderNumber = order_no[1];
                                    OrderNumber.setText("#"+orderNumber);
                                    SimpleDateFormat datetime = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                                    SimpleDateFormat date2 = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                                    SimpleDateFormat time2 = new SimpleDateFormat("hh:mm a", Locale.US);
                                    Date date3 = null, time3 = null;
                                    try {

                                        time3 = datetime.parse(ExpectedTime);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    String date1 = date2.format(time3);
                                    String time1 = time2.format(time3);

                                    expectedTime.setText(date1+"\n"+time1);
                                    SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                    SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    String date;
                                    Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                    try {
                                        dateObj = curFormater.parse(TrackingTime);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        try {
                                            dateObj = curFormater1.parse(TrackingTime);
                                        } catch (ParseException pe) {

                                        }

                                    }
                                    SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                    date = postFormater.format(dateObj);
                                    orderTime.setText(date);

                                    OrderNumberText.setAlpha(1.0f);
                                    OrderTitle.setAlpha(1.0f);
                                    ExpTimeText.setAlpha(1.0f);
                                    lineAboveOrder.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                    lineBelowOrder.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                    lineAboveConfirmed.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                    OrderImage.setImageDrawable(getResources().getDrawable(R.drawable.order_done));
                                } catch (JSONException je) {
                                    je.printStackTrace();
                                    scrollView.setVisibility(View.GONE);
                                    emptyView.setVisibility(View.VISIBLE);
                                    cancelOrder.setVisibility(View.GONE);
//                                    share.setVisibility(View.GONE);
                                }


                                try {
                                    JSONArray ja1 = jo.getJSONArray("Cancel");
                                    JSONObject jo2 = ja1.getJSONObject(0);
                                    cancelOrder.setVisibility(View.GONE);

                                    if (language.equalsIgnoreCase("En")) {
//                                        accepted.setText("Cancel");
                                        ConfirmedText.setText("Your Order has been cancelled");
                                        ConfirmedTitle.setText("Cancel");

                                    } else if (language.equalsIgnoreCase("Ar")) {
                                        ConfirmedText.setText("الغاء");
                                        ConfirmedTitle.setText("طلبك تم الغاءه");
                                    }
//                                    acceptImagecancel.setVisibility(View.VISIBLE);
                                    ReadyLayout.setVisibility(View.GONE);
                                    OnTheWayLayout.setVisibility(View.GONE);
                                    DeliveredLayout.setVisibility(View.GONE);
                                    cancelOrder.setVisibility(View.GONE);
                                    lineBelowConfirmed.setVisibility(View.INVISIBLE);
                                    share_layout.setVisibility(View.GONE);

                                    TrackingTime = jo2.getString("TrackingTime");

                                    SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                    SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                    String date;
                                    Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                    try {
                                        dateObj = curFormater.parse(TrackingTime);
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        try {
                                            dateObj = curFormater1.parse(TrackingTime);
                                        } catch (ParseException pe) {

                                        }

                                    }
                                    SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                    date = postFormater.format(dateObj);
                                    acceptTime.setText(date);

                                    ConfirmedText.setAlpha(1.0f);
                                    ConfirmedTitle.setAlpha(1.0f);
                                    ConfirmedImage.setImageDrawable(getResources().getDrawable(R.drawable.step_cancel));

//                                }
                                } catch (JSONException jsone) {

                                    try {
                                        JSONArray ja = jo.getJSONArray("Confirmed");
                                        JSONObject jo1 = ja.getJSONObject(0);
                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setClickable(true);
                                        cancelOrder.setAlpha(1f);
                                        TrackingTime = jo1.getString("TrackingTime");
                                        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                        String date;
                                        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                        try {
                                            dateObj = curFormater.parse(TrackingTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            try {
                                                dateObj = curFormater1.parse(TrackingTime);
                                            } catch (ParseException pe) {

                                            }

                                        }
                                        SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                        date = postFormater.format(dateObj);
//                                        if(OrderType.equalsIgnoreCase("Delivery")){
//                                            expectedTime.setText("Your order is accepted, will be delivered by "+ExpectedTime);
//                                        }else {
//                                            trackDelivery.setText("SERVED");
//                                            trackDeliveryText.setText("Your order is served");
//                                            expectedTime.setText("Your order is accepted, will be served by "+ExpectedTime);
//                                        }

                                        acceptTime.setText(date);

                                        ConfirmedText.setAlpha(1.0f);
                                        ConfirmedTitle.setAlpha(1.0f);
                                        lineBelowConfirmed.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                        lineAboveReady.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                        ConfirmedImage.setImageDrawable(getResources().getDrawable(R.drawable.confirmed_done));
//                                        acceptLayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    } catch (JSONException je) {

                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setClickable(true);
                                        cancelOrder.setAlpha(1f);
//                                        acceptLayout.setAlpha(0.15f);
//                                        readyLayout.setAlpha(0.15f);
//                                        if (language.equalsIgnoreCase("En")) {
                                            if (OrderType.equalsIgnoreCase("Delivery")) {
//                                            expectedTime.setText("Your order is accepted, will be delivered by ");
                                                OnTheWayLayout.setVisibility(View.VISIBLE);
                                                OnTheWayLine.setVisibility(View.VISIBLE);
//                                                onthewayLayout.setAlpha(0.15f);
//                                                deliveryLayout.setAlpha(0.15f);
                                            } else {
                                                OnTheWayLayout.setVisibility(View.GONE);
                                                OnTheWayLine.setVisibility(View.GONE);
                                            }
//                                        } else if (language.equalsIgnoreCase("Ar")) {
//                                            if (OrderType.equalsIgnoreCase("Delivery")) {
////                                            expectedTime.setText("Your order is accepted, will be delivered by ");
//                                                onthewayLayout.setAlpha(0.15f);
//                                                deliveryLayout.setAlpha(0.15f);
//                                            } else {
//                                                trackDelivery.setText("تم استلامه");
//                                                trackDeliveryText.setText("تم تقديم طلبك");
////                                            expectedTime.setText("Your order is accepted, will be served by ");
//                                                onthewayLayout.setVisibility(View.GONE);
//                                                deliveryLayout.setAlpha(0.15f);
//                                            }
//                                        }
                                    }

                                    try {
                                        JSONArray ja = jo.getJSONArray("Ready");
                                        JSONObject jo1 = ja.getJSONObject(0);
                                        cancelOrder.setEnabled(false);
                                        cancelOrder.setClickable(false);
                                        cancelOrder.setAlpha(0.5f);
                                        TrackingTime = jo1.getString("TrackingTime");
                                        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                        SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                        String date;
                                        Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                        try {
                                            dateObj = curFormater.parse(TrackingTime);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                            try {
                                                dateObj = curFormater1.parse(TrackingTime);
                                            } catch (ParseException pe) {

                                            }

                                        }
                                        SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                        date = postFormater.format(dateObj);
                                        readyTime.setText(date);

                                        ReadyText.setAlpha(1.0f);
                                        ReadyTitle.setAlpha(1.0f);
                                        lineBelowReady.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                        ReadyImage.setImageDrawable(getResources().getDrawable(R.drawable.ready_done));
                                        if (OrderType.equalsIgnoreCase("Delivery")) {
                                            lineAboveOnTheWay.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                        } else {
                                            lineAboveDelivered.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                        }
                                    } catch (JSONException je) {
                                        cancelOrder.setEnabled(true);
                                        cancelOrder.setAlpha(1f);
                                        cancelOrder.setClickable(true);
//                                        readyLayout.setAlpha(0.15f);
//                                        if (language.equalsIgnoreCase("En")) {
                                            if (OrderType.equalsIgnoreCase("Delivery")) {
                                                if(language.equalsIgnoreCase("En")) {
                                                    ReadyText.setText(getResources().getString(R.string.ready_text)+" "+ StoreAddress);
                                                }
                                                else{
                                                    ReadyText.setText(StoreAddressAr +" "+getResources().getString(R.string.ready_text_ar));
                                                }
//                                                onthewayLayout.setAlpha(0.15f);
//                                                deliveryLayout.setAlpha(0.15f);
                                            } else {
                                                if (language.equalsIgnoreCase("En")) {
                                                    ReadyText.setText("Your Order is Ready");
                                                }
                                                else{
                                                    ReadyText.setText(getResources().getString(R.string.ready_text_ar));
                                                }
//                                                onthewayLayout.setVisibility(View.GONE);
//                                                deliveryLayout.setAlpha(0.15f);
                                            }
//                                        } else if (language.equalsIgnoreCase("Ar")) {
//                                            if (OrderType.equalsIgnoreCase("Delivery")) {
//                                                mready_text.setText("طلبك جاهز في" + StoreNameAr);
//                                                onthewayLayout.setAlpha(0.15f);
//                                                deliveryLayout.setAlpha(0.15f);
//                                            } else {
//                                                mready_text.setText("طلبك جاهز في");
//                                                onthewayLayout.setVisibility(View.GONE);
//                                                deliveryLayout.setAlpha(0.15f);
//                                            }
//                                        }
                                    }

                                    if (OrderType.equalsIgnoreCase("Delivery")) {
                                        OnTheWayLayout.setVisibility(View.VISIBLE);
                                        OnTheWayLine.setVisibility(View.VISIBLE);
                                        try {
                                            JSONArray ja = jo.getJSONArray("OnTheWay");
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
                                            JSONArray driverArray = jo1.getJSONArray("DriverName");
                                            JSONObject driverObj = driverArray.getJSONObject(0);
                                            driverName = driverObj.getString("FullName");
                                            driverNumber = driverObj.getString("Mobile");
                                            driverId = jo1.getString("UserId");
                                            TrackingTime = jo1.getString("TrackingTime");
                                            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                            SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                            String date;
                                            Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
//                                            if (language.equalsIgnoreCase("En")) {
//                                                montheway_text.setText("Your food is on the way to \n" + address);
//                                            } else if (language.equalsIgnoreCase("Ar")) {
//                                                montheway_text.setText("\n طلبك الآن في الطريق إلى" + address);
//                                            }
                                            try {
                                                dateObj = curFormater.parse(TrackingTime);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                try {
                                                    dateObj = curFormater1.parse(TrackingTime);
                                                } catch (ParseException pe) {

                                                }

                                            }
                                            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                            date = postFormater.format(dateObj);
                                            onthewayTime.setText(date);

                                            OntheWayText.setAlpha(1.0f);
                                            OntheWayTitle.setAlpha(1.0f);
                                            lineBelowOnTheWay.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                            lineAboveDelivered.setImageDrawable(getResources().getDrawable(R.drawable.brown_line));
                                            OntheWayImage.setImageDrawable(getResources().getDrawable(R.drawable.ontheway_done));
//                                            onthewayLayout.setAlpha(1f);

                                            animZoomIn= AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_in);
                                            animZoomOut=AnimationUtils.loadAnimation(TrackOrder.this,R.anim.zoom_out);

                                            OntheWayImage.setAnimation(animZoomIn);
                                            OntheWayImage.setAnimation(animZoomOut);

                                            animZoomIn.setAnimationListener(new Animation.AnimationListener() {
                                                @Override
                                                public void onAnimationStart(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationRepeat(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationEnd(Animation animation) {

                                                    OntheWayImage.startAnimation(animZoomOut);
                                                }
                                            });

                                            animZoomOut.setAnimationListener(new Animation.AnimationListener() {
                                                @Override
                                                public void onAnimationStart(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationRepeat(Animation animation) {

                                                }

                                                @Override
                                                public void onAnimationEnd(Animation animation) {

                                                    OntheWayImage.startAnimation(animZoomIn);
                                                }
                                            });

                                            OnTheWayLayout.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent i = new Intent(TrackOrder.this, LiveTrackingActivity.class);
                                                    i.putExtra("driver_name", driverName);
                                                    i.putExtra("driver_number", driverNumber);
                                                    i.putExtra("driver_id", driverId);
                                                    i.putExtra("order_id", orderId);
                                                    i.putExtra("exp_time", ExpectedTime);
                                                    startActivity(i);
                                                }
                                            });

                                        } catch (JSONException je) {
                                            je.printStackTrace();
                                        }


                                        try {
                                            JSONArray ja = jo.getJSONArray("Delivered");
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
                                            TrackingTime = jo1.getString("TrackingTime");
                                            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                            SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                            String date;
                                            Date dateObj = null;
                                            OrderStatus = "Delivery";

                                            OntheWayImage.clearAnimation();
                                            OnTheWayLayout.setClickable(false);

                                            try {
                                                dateObj = curFormater.parse(TrackingTime);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                try {
                                                    dateObj = curFormater1.parse(TrackingTime);
                                                } catch (ParseException pe) {

                                                }

                                            }
                                            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                            date = postFormater.format(dateObj);
                                            deliveryTime.setText(date);

                                            DeliveredText.setAlpha(1.0f);
                                            DeliveredTitle.setAlpha(1.0f);
                                            DeliveredImage.setImageDrawable(getResources().getDrawable(R.drawable.delivered_done));
//                                            deliveryLayout.setAlpha(1f);
//                                            onthewayLayout.setClickable(false);

                                            Log.i("TAG","rating from service "+ratingFromService);
                                            if(ratingFromService == 0 && rateArray.size()>0) {
//                                                RatingDetails rd = new RatingDetails();
//                                                JSONArray jA = jo1.getJSONArray("Ratings");
//                                                JSONObject obj = jA.getJSONObject(0);
//                                                rd.setGivenrating("5");
//                                                JSONArray ratingArray = obj.getJSONArray("5");
//                                                ArrayList<Rating> ratingsArrayList = new ArrayList<>();
//                                                for (int i = 0; i < ratingArray.length(); i++) {
//                                                    JSONObject object1 = ratingArray.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//                                                JSONArray ratingArray1 = obj.getJSONArray("4");
//                                                rd.setGivenrating("4");
//                                                for (int i = 0; i < ratingArray1.length(); i++) {
//                                                    JSONObject object1 = ratingArray1.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//                                                JSONArray ratingArray2 = obj.getJSONArray("3");
//                                                rd.setGivenrating("3");
//                                                for (int i = 0; i < ratingArray2.length(); i++) {
//                                                    JSONObject object1 = ratingArray2.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//
//                                                JSONArray ratingArray3 = obj.getJSONArray("2");
//                                                rd.setGivenrating("2");
//                                                for (int i = 0; i < ratingArray3.length(); i++) {
//                                                    JSONObject object1 = ratingArray3.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//                                                JSONArray ratingArray4 = obj.getJSONArray("1");
//                                                rd.setGivenrating("1");
//                                                for (int i = 0; i < ratingArray4.length(); i++) {
//                                                    JSONObject object1 = ratingArray4.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//
//                                                rateArray.add(rd);

                                                rate_layout.setVisibility(View.VISIBLE);
                                                cancel.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        rate_layout.setVisibility(View.GONE);
                                                    }
                                                });

                                                ratingBar.setRating(0);
                                                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                                    @Override
                                                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                        if (fromUser) {
                                                            Intent intent = new Intent(TrackOrder.this, RatingActivity.class);
                                                            intent.putExtra("rating", rating);
                                                            intent.putExtra("array", rateArray);
                                                            intent.putExtra("userid", userId);
                                                            intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                            startActivity(intent);
                                                            rate_layout.setVisibility(View.GONE);
                                                        }
                                                    }
                                                });
                                            }

                                        } catch (JSONException je) {
                                            je.printStackTrace();
//                                            deliveryLayout.setAlpha(0.15f);
                                        }

                                    } else {
                                        OnTheWayLayout.setVisibility(View.GONE);
                                        OnTheWayLine.setVisibility(View.GONE);
                                        try {
                                            JSONArray ja = jo.getJSONArray("Delivered");
                                            JSONObject jo1 = ja.getJSONObject(0);
                                            cancelOrder.setEnabled(false);
                                            cancelOrder.setAlpha(0.5f);
                                            cancelOrder.setClickable(false);
                                            TrackingTime = jo1.getString("TrackingTime");
                                            SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
                                            SimpleDateFormat curFormater1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
                                            String date;
                                            Date dateObj = null;
//        String[] parts = favOrderList.get(position).getOrderDate().split(".");
                                            OrderStatus = "Served";
                                            if (language.equalsIgnoreCase("En")) {
                                                DeliveredTitle.setText("SERVED");
                                                DeliveredText.setText("Your food has been Served");
                                            } else if (language.equalsIgnoreCase("Ar")) {
                                                DeliveredTitle.setText("تم استلامه");
                                                DeliveredText.setText("تم تقديم طلبك ");
                                            }
                                            try {
                                                dateObj = curFormater.parse(TrackingTime);
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                                try {
                                                    dateObj = curFormater1.parse(TrackingTime);
                                                } catch (ParseException pe) {

                                                }

                                            }
                                            SimpleDateFormat postFormater = new SimpleDateFormat("hh:mma", Locale.US);
                                            date = postFormater.format(dateObj);
                                            deliveryTime.setText(date);

                                            DeliveredText.setAlpha(1.0f);
                                            DeliveredTitle.setAlpha(1.0f);
                                            DeliveredImage.setImageDrawable(getResources().getDrawable(R.drawable.delivered_done));
//                                            deliveryLayout.setAlpha(1f);

                                            Log.i("TAG","rating from service1 "+rateArray.size());
                                            if(ratingFromService == 0 && rateArray.size()>0) {
//                                                RatingDetails rd = new RatingDetails();
//                                                JSONArray jA = jo1.getJSONArray("Ratings");
//                                                JSONObject obj = jA.getJSONObject(0);
//                                                rd.setGivenrating("5");
//                                                JSONArray ratingArray = obj.getJSONArray("5");
//                                                ArrayList<Rating> ratingsArrayList = new ArrayList<>();
//                                                for (int i = 0; i < ratingArray.length(); i++) {
//                                                    JSONObject object1 = ratingArray.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//                                                JSONArray ratingArray1 = obj.getJSONArray("4");
//                                                rd.setGivenrating("4");
//                                                for (int i = 0; i < ratingArray1.length(); i++) {
//                                                    JSONObject object1 = ratingArray1.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//                                                JSONArray ratingArray2 = obj.getJSONArray("3");
//                                                rd.setGivenrating("3");
//                                                for (int i = 0; i < ratingArray2.length(); i++) {
//                                                    JSONObject object1 = ratingArray2.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//
//                                                JSONArray ratingArray3 = obj.getJSONArray("2");
//                                                rd.setGivenrating("2");
//                                                for (int i = 0; i < ratingArray3.length(); i++) {
//                                                    JSONObject object1 = ratingArray3.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//                                                JSONArray ratingArray4 = obj.getJSONArray("1");
//                                                rd.setGivenrating("1");
//                                                for (int i = 0; i < ratingArray4.length(); i++) {
//                                                    JSONObject object1 = ratingArray4.getJSONObject(i);
//                                                    Rating rating = new Rating();
//                                                    rating.setRatingId(object1.getString("RatingId"));
//                                                    rating.setComment(object1.getString("Comment_En"));
//                                                    rating.setComment_ar(object1.getString("Comment_Ar"));
//                                                    rating.setRating(object1.getString("Rating"));
//                                                    ratingsArrayList.add(rating);
//                                                }
//                                                rd.setArrayList(ratingsArrayList);
//
//                                                rateArray.add(rd);

                                                Log.i("TAG","rating visible");
                                                rate_layout.setVisibility(View.VISIBLE);
                                                cancel.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        rate_layout.setVisibility(View.GONE);
                                                    }
                                                });

                                                ratingBar.setRating(0);
                                                ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

                                                    @Override
                                                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

//                                                    showDialog2(rating);
                                                        if (fromUser) {
                                                            Intent intent = new Intent(TrackOrder.this, RatingActivity.class);
                                                            intent.putExtra("rating", rating);
                                                            intent.putExtra("array", rateArray);
                                                            intent.putExtra("userid", userId);
                                                            intent.putExtra("orderid", orderId);
//                                                    intent.putExtra("reqid", ratingsArrayList.get(0).getReqId());
                                                            startActivity(intent);
                                                            rate_layout.setVisibility(View.GONE);
                                                        }
                                                    }
                                                });
                                            }

//                                            }
                                        } catch (JSONException je) {
                                            je.printStackTrace();
//                                            deliveryLayout.setAlpha(0.15f);
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            share_layout.setVisibility(View.GONE);
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(TrackOrder.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                finish();
            }
            try {
                if (dialog != null) {
                    dialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            if (orderPrefs.getString("order_id", "").equals(orderId)) {
//                if (orderPrefs.getString("order_status", "close").equalsIgnoreCase("close")) {
//                    cancelOrder.setEnabled(false);
//                    cancelOrder.setAlpha(0.5f);
//                    cancelOrder.setClickable(false);
//                } else {
//                    cancelOrder.setEnabled(true);
//                    cancelOrder.setAlpha(1f);
//                    cancelOrder.setClickable(true);
//                }
//            } else {
//                Log.i("TAG", "order id not matched ");
//                cancelOrder.setEnabled(false);
//                cancelOrder.setAlpha(0.5f);
//                cancelOrder.setClickable(false);
//            }
            super.onPostExecute(result);

        }

    }

    public void getGPSCoordinates() {
        gps = new GPSTracker(TrackOrder.this);
        if (gps != null) {
            if (gps.canGetLocation()) {
                lat = gps.getLatitude();
                longi = gps.getLongitude();
                // Create a LatLng object for the current location
                LatLng latLng = new LatLng(lat, longi);
//                new GetStoresInfo().execute(Constants.STORES_URL);
//                listView.setAdapter(mAdapter);
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT,
                        "Dajen Order Details \n Order No: " + orderNumber + "\n Expected Time: " + ExpectedTime + "\n Store: " + StoreName + "\n Amount: " + totalPrice + "\nhttp://maps.google.com/maps?saddr=" + lat + "," + longi + "&daddr=" + latitude + "," + longitude);

                startActivity(Intent.createChooser(intent, "Share"));
                Log.i("Location TAG", "outside" + lat + " " + longi);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }
        }
    }


    private boolean canAccessPhonecalls() {
        return (hasPermission(Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(TrackOrder.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: +" + phone));
                    if (ActivityCompat.checkSelfPermission(TrackOrder.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(intent);
                } else {
                    Toast.makeText(TrackOrder.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;

            case LOCATION_REQUEST:
                if(canAccessLocation()){
                    getGPSCoordinates();
                }
                else{
                    Toast.makeText(TrackOrder.this, "Location permission denied, Unable to share", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }


    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean canAccessLocation1() {
        return (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION));
    }

//    private boolean hasPermission(String perm) {
//        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(TrackOrder.this, perm));
//    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//
//        switch (requestCode) {
//
//
//            case LOCATION_REQUEST:
//                if (canAccessLocation()) {
//                    getGPSCoordinates();
//                } else {
//                    Toast.makeText(TrackOrder.this, "Location permission denied, Unable to show nearby stores", Toast.LENGTH_LONG).show();
//                }
//                break;
//        }
//    }

    public class CancelOrder extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String responce = null;
        String networkStatus;
        JSONObject parent = new JSONObject();
        InputStream inputStream = null;
        String result = "";
        MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(TrackOrder.this);
            dialog = new MaterialDialog.Builder(TrackOrder.this)
                    .title(R.string.app_name)
                    .content("Please Wait...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {

                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.CANCEL_ORDER_URL+orderId);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(arg0[0]);

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            result = convertInputStreamToString(inputStream);
                            return result;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("Responce", "" + result);
            } else {
                return "no internet";
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (result1 != null) {
                Log.i("TAG","user resposne: "+result1);
                if (result1.equalsIgnoreCase("no internet")) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TrackOrder.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.connection_error));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.connection_error_ar));
                    }
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                } else {
                    try {
                        JSONObject jo = new JSONObject(result);

//                        JSONObject ja = jo.getJSONObject("Success");

                        new GetOrderTrackDetails().execute(Constants.TRACK_ORDER_STEPS_URL + orderId + "&uId=" + userId);
                        Toast.makeText(TrackOrder.this, "Order cancelled successfully", Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            super.onPostExecute(result1);
        }
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }

    @Override
    public void onBackPressed() {
        if(screen.equalsIgnoreCase("completed")){
            FooterActivity.tabBarPosition = 1;
            Intent intent = new Intent(TrackOrder.this, OrderActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        else {
            startActivity(new Intent(TrackOrder.this, OrderHistoryActivity.class));
            finish();
        }
    }
}
