package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.cs.dajen.Adapters.CustomSaladAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.AdditionalPrices;
import com.cs.dajen.Models.Additionals;
import com.cs.dajen.Models.MenuItems;
import com.cs.dajen.Models.Modifiers;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by CS on 04-05-2017.
 */

public class SaladActivity extends Activity implements View.OnClickListener{

    RelativeLayout default_layout, custom_layout;
    ImageView default_checkbox, custom_checkbox, itemImage, footer_plus, footer_minus;
    TextView itemName_title, itemName, itemPrice, footer_amt, footer_qty, footer_add;
    ArrayList<MenuItems> menuItems = new ArrayList<>();
    int position, qty;
    float price;
    LinearLayout gridLayout, pricesLayout;
    private DataBaseHelper myDbHelper;
    public static boolean isCustom = false;
    ImageView back_btn, comment;
//    public static TextView saladQty;
    public static ImageView empty_bowl;
    public static RelativeLayout additionals;
    public static ArrayList<String> addlImages = new ArrayList<>();
    public static Boolean addlRefresh = false;
    Context context;
    private ArrayList<Modifiers> SaladList = new ArrayList<>();
    CustomSaladAdapter mSaladAdapter;
    GridView saladGrid;
    public static int refreshCount = 0, additionals_count, total_additionals;
    public static ArrayList<String> addl_ids = new ArrayList<>();
    public static ArrayList<String> addl_pos = new ArrayList<>();
    public static TextView grid_count;
    AlertDialog customDialog;
    View line_spice;

    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    SharedPreferences languagePrefs;
    String language;
    String screen;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.salad_inside);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.salad_inside_ar);
        }
        context = this;

        refreshCount = 0;

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        Constants.CurrentOrderActivity = "salad";
        FooterActivity.tabBarPosition = 2;

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        myDbHelper = new DataBaseHelper(getApplicationContext());

        position = getIntent().getIntExtra("position",0);
        menuItems = (ArrayList<MenuItems>) getIntent().getSerializableExtra("arraylist");
        try {
            screen = getIntent().getStringExtra("screen");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Constants.ItemPosition = position;

        additionals = (RelativeLayout) findViewById(R.id.additional_layout);
        default_layout = (RelativeLayout) findViewById(R.id.salad_deafult_layout);
        custom_layout = (RelativeLayout) findViewById(R.id.salad_custom_layout);
        gridLayout = (LinearLayout) findViewById(R.id.grid_layout);
        pricesLayout = (LinearLayout) findViewById(R.id.prices_layout);
        saladGrid = (GridView) findViewById(R.id.products_grid);
        line_spice = (View) findViewById(R.id.line_spice);

        default_checkbox = (ImageView) findViewById(R.id.salad_deafult);
        custom_checkbox = (ImageView) findViewById(R.id.salad_custom);
        itemImage = (ImageView) findViewById(R.id.grid_image);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        comment = (ImageView) findViewById(R.id.comment);
        empty_bowl = (ImageView) findViewById(R.id.empty_bowl);
        empty_bowl.setVisibility(View.INVISIBLE);

        footer_plus = (ImageView) findViewById(R.id.footer_plus);
        footer_minus = (ImageView) findViewById(R.id.footer_minus);

        itemName_title = (TextView) findViewById(R.id.title);
        itemName = (TextView) findViewById(R.id.salad_default_name);
        itemPrice = (TextView) findViewById(R.id.grid_price);
        grid_count = (TextView) findViewById(R.id.grid_count);

        footer_amt = (TextView) findViewById(R.id.footer_amount);
        footer_qty = (TextView) findViewById(R.id.footer_qty);
        footer_add = (TextView) findViewById(R.id.footer_addmore);

        Constants.COMMENTS = "";
        gridLayout.setVisibility(View.INVISIBLE);
        isCustom = false;
        try {
            addlImages.clear();
            addl_ids.clear();
            addl_pos.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(screen!=null && screen.equalsIgnoreCase("chicken")){
            new GetMenuItems().execute(Constants.GET_ITEMS+Constants.Itemid);
        }
        else{
            menuItems = Constants.menuItems;
            setData();
        }

        default_layout.setOnClickListener(this);
        custom_layout.setOnClickListener(this);
        footer_plus.setOnClickListener(this);
        footer_minus.setOnClickListener(this);
        footer_add.setOnClickListener(this);
        comment.setOnClickListener(this);
        back_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_btn:
                Intent sideItemsIntent = new Intent(SaladActivity.this,ProductActivity.class);
                sideItemsIntent.putExtra("id",5);
                startActivity(sideItemsIntent);
                finish();
                break;

            case R.id.comment:
                Intent commentIntent = new Intent(SaladActivity.this, CommentActivity.class);
                if(language.equalsIgnoreCase("En")) {
                    commentIntent.putExtra("name",menuItems.get(position).getItemName());
                }else if(language.equalsIgnoreCase("Ar")){
                    commentIntent.putExtra("name",menuItems.get(position).getItemName_ar());
                }
                commentIntent.putExtra("image",menuItems.get(position).getImage());
                commentIntent.putExtra("screen","salad");
                startActivity(commentIntent);
                break;

            case R.id.salad_deafult_layout:
                itemImage.setVisibility(View.VISIBLE);
                empty_bowl.setVisibility(View.INVISIBLE);
                gridLayout.setVisibility(View.INVISIBLE);
                additionals.setVisibility(View.INVISIBLE);
                grid_count.setVisibility(View.INVISIBLE);
                isCustom = false;
                default_checkbox.setImageResource(R.drawable.salad_selected);
                custom_checkbox.setImageResource(R.drawable.salad_unselected);
                break;

            case R.id.salad_custom_layout:
                if(SaladList.size()>0) {
                    itemImage.setVisibility(View.INVISIBLE);
//                    empty_bowl.setVisibility(View.VISIBLE);
                    gridLayout.setVisibility(View.VISIBLE);
//                    additionals.setVisibility(View.VISIBLE);
                    grid_count.setVisibility(View.VISIBLE);
                    mSaladAdapter.notifyDataSetChanged();
                    addlRefresh = true;
                    isCustom = true;
                    default_checkbox.setImageResource(R.drawable.salad_unselected);
                    custom_checkbox.setImageResource(R.drawable.salad_selected);
                }
                break;

            case R.id.footer_plus:
                qty = qty + 1;
                footer_amt.setText(Constants.decimalFormat.format(price*qty));
                footer_qty.setText(""+qty);
                break;

            case R.id.footer_minus:
                if(qty>1){
                    qty = qty - 1;
                }
                footer_amt.setText(Constants.decimalFormat.format(price*qty));
                footer_qty.setText(""+qty);
                break;

            case R.id.footer_addmore:

                if(!isCustom) {
                    HashMap<String, String> values = new HashMap<>();
                    values.put("CategoryId", menuItems.get(position).getCatId());
                    values.put("ItemId", menuItems.get(position).getItemId());
                    values.put("ItemName", menuItems.get(position).getItemName());
                    values.put("ItemName_Ar", menuItems.get(position).getItemName_ar());
                    values.put("ItemDescription_En", menuItems.get(position).getDesc());
                    values.put("ItemDescription_Ar", menuItems.get(position).getDesc_ar());
                    values.put("ItemImage", menuItems.get(position).getImage());
                    values.put("ItemTypeId", "1");
                    values.put("TypeName", "REGULAR");
                    values.put("Price", Float.toString(price));
                    values.put("ModifierId", menuItems.get(position).getModifierID());
                    values.put("ModifierName", "0");
                    values.put("ModifierName_Ar", "0");
                    values.put("MdfrImage", "0");
                    values.put("AdditionalID", "0");
                    values.put("AdditionalName", "0");
                    values.put("AdditionalName_Ar", "0");
                    values.put("AddlImage", "0");
                    values.put("AddlTypeId", "0");
                    values.put("AdditionalPrice", "0");
                    values.put("Qty", Integer.toString(qty));
                    values.put("TotalAmount", Float.toString(price * qty));
                    values.put("Comment", Constants.COMMENTS);
                    values.put("Custom", "0");
                    values.put("SinglePrice", "0");
                    myDbHelper.insertOrder(values);

                    if(Constants.MENU_TYPE.equals("main")) {
                        userPrefEditor.putString("menu", "main");
                        userPrefEditor.commit();
                    }
                    else if(Constants.MENU_TYPE.equals("bbq")){
                        userPrefEditor.putString("menu", "bbq");
                        userPrefEditor.commit();
                    }

                    Intent intent = new Intent(SaladActivity.this,ProductActivity.class);
                    intent.putExtra("id",Constants.Itemid);
                    startActivity(intent);
                    finish();
                }
                else {
                    if (addl_pos.size() > 0) {
                        String addl_id = null, addl_name = null, addl_name_ar = null, addl_image = null, addl_price = null, addl_qty = "1";
                        for (int i = 0; i < addl_pos.size(); i++) {
                            int pos = Integer.parseInt(addl_pos.get(i).toString());
                            if (i == 0) {
                                addl_id = SaladList.get(0).getChildItems().get(pos).getAdditionalsId();
                                addl_name = SaladList.get(0).getChildItems().get(pos).getAdditionalName();
                                addl_name_ar = SaladList.get(0).getChildItems().get(pos).getAdditionalNameAr();
                                addl_image = SaladList.get(0).getChildItems().get(pos).getImages();
                                addl_price = "0";
                            } else {
                                addl_id = addl_id + "," + SaladList.get(0).getChildItems().get(pos).getAdditionalsId();
                                addl_name = addl_name + "," + SaladList.get(0).getChildItems().get(pos).getAdditionalName();
                                addl_name_ar = addl_name_ar + "," + SaladList.get(0).getChildItems().get(pos).getAdditionalNameAr();
                                addl_image = addl_image + "," + SaladList.get(0).getChildItems().get(pos).getImages();
                                addl_price = addl_price + ",0";
                                addl_qty = addl_qty + ",1";
                            }
                        }
                        HashMap<String, String> values = new HashMap<>();
                        values.put("CategoryId", menuItems.get(position).getCatId());
                        values.put("ItemId", menuItems.get(position).getItemId());
                        values.put("ItemName", menuItems.get(position).getItemName());
                        values.put("ItemName_Ar", menuItems.get(position).getItemName_ar());
                        values.put("ItemDescription_En", menuItems.get(position).getDesc());
                        values.put("ItemDescription_Ar", menuItems.get(position).getDesc_ar());
                        values.put("ItemImage", menuItems.get(position).getImage());
                        values.put("ItemTypeId", "1");
                        values.put("TypeName", "REGULAR");
                        values.put("Price", Float.toString(price));
                        values.put("ModifierId", menuItems.get(position).getModifierID());
                        values.put("ModifierName", "0");
                        values.put("ModifierName_Ar", "0");
                        values.put("MdfrImage", "");
                        values.put("AdditionalID", addl_id);
                        values.put("AdditionalName", addl_name);
                        values.put("AdditionalName_Ar", addl_name_ar);
                        values.put("AddlImage", addl_image);
                        values.put("AddlTypeId", "");
                        values.put("AdditionalPrice", addl_price);
                        values.put("Qty", Integer.toString(qty));
                        values.put("TotalAmount", Float.toString(price * qty));
                        values.put("Comment", Constants.COMMENTS);
                        values.put("Custom", "1");
                        values.put("SinglePrice", addl_qty);
                        myDbHelper.insertOrder(values);

                        if (Constants.MENU_TYPE.equals("main")) {
                            userPrefEditor.putString("menu", "main");
                            userPrefEditor.commit();
                        } else if (Constants.MENU_TYPE.equals("bbq")) {
                            userPrefEditor.putString("menu", "bbq");
                            userPrefEditor.commit();
                        }

                        Intent intent = new Intent(SaladActivity.this, ProductActivity.class);
                        intent.putExtra("id", Constants.Itemid);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SaladActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        no.setVisibility(View.GONE);
                        vert.setVisibility(View.GONE);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.dajen));
                            yes.setText(getResources().getString(R.string.ok));
                            desc.setText("please select atleast one item");
                        }
                        else{
                            title.setText(getResources().getString(R.string.dajen_ar));
                            yes.setText(getResources().getString(R.string.ok));
                            desc.setText("من فضلك إختر على الأقل سلطة واحدة");
                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);
                    }
                }
                break;
        }

    }

    private class GetAdditionals extends AsyncTask<String, Integer, String> {

        String response;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
//            Constants.menuItems.clear();
            dialog = new MaterialDialog.Builder(context)
                    .title(R.string.app_name)
                    .content("Loading...")
                    .progress(true, 0)
                    .cancelable(false)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONArray ja = new JSONArray(result);
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo = ja.getJSONObject(i);
                                Modifiers mod = new Modifiers();
                                ArrayList<Additionals> additionalsList = new ArrayList<>();
                                JSONArray key = jo.getJSONArray("Key");

                                for (int j = 0; j < key.length(); j++) {


                                    JSONObject jo1 = key.getJSONObject(j);
                                    if (j == 0) {
                                        String modifierName = jo1.getString("ModifierName");
                                        String modifierName_Ar = jo1.getString("ModifierName_Ar");
                                        String modifierId = jo1.getString("ModifierId");
//                                        if(modifierId.equalsIgnoreCase("1")){
//                                            continue Outer;
//                                        }
                                        mod.setModifierName(modifierName);
                                        mod.setModifierNameAr(modifierName_Ar);
                                        mod.setModifierId(modifierId);
                                    } else {

//                                        JSONObject issueObj = jo.getJSONObject("Value");
                                        Iterator iterator = jo1.keys();
                                        while (iterator.hasNext()) {
                                            Additionals adis = new Additionals();
                                            String additonalId = (String) iterator.next();
                                            ArrayList<AdditionalPrices> additionalPrices = new ArrayList<>();
                                            JSONArray ja1 = jo1.getJSONArray(additonalId);

                                            for (int k = 0; k < ja1.length(); k++) {
                                                JSONArray additionals = ja1.getJSONArray(k);
                                                if (k == 0) {
                                                    JSONObject additionalDetails = additionals.getJSONObject(0);
                                                    String additionalName = additionalDetails.getString("AdditionalName");
                                                    String additionalName_Ar = additionalDetails.getString("AdditionalName_Ar");
                                                    String images = additionalDetails.getString("Images");
                                                    String additionalId = additionalDetails.getString("AdditionalId");
                                                    adis.setAdditionalName(additionalName);
                                                    adis.setAdditionalNameAr(additionalName_Ar);
                                                    adis.setAdditionalsId(additionalId);
                                                    adis.setImages(images);
                                                    adis.setModifierId(mod.getModifierId());
                                                } else {
                                                    for (int l = 0; l < additionals.length(); l++) {
                                                        AdditionalPrices ap = new AdditionalPrices();
                                                        JSONObject jo2 = additionals.getJSONObject(l);
                                                        ap.setItemType(jo2.getString("Type"));
                                                        ap.setPrice(jo2.getString("Price"));
                                                        additionalPrices.add(ap);
                                                    }
                                                    adis.setAdditionalPriceList(additionalPrices);
                                                }
                                            }
                                            additionalsList.add(adis);
                                        }
                                    }
                                }
                                mod.setChildItems(additionalsList);
                                SaladList.add(mod);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(SaladList.size()>0) {
                    mSaladAdapter = new CustomSaladAdapter(getApplicationContext(), SaladList.get(0).getChildItems(), language, SaladActivity.this);
                    saladGrid.setAdapter(mSaladAdapter);
                    mSaladAdapter.notifyDataSetChanged();
                }

            }
            else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent sideItemsIntent = new Intent(SaladActivity.this,ProductActivity.class);
        sideItemsIntent.putExtra("id",5);
        startActivity(sideItemsIntent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }

    public void setData(){
        Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + menuItems.get(position).getImage()).into(itemImage);

        itemPrice.setText(""+Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice1())));
        footer_amt.setText(""+Constants.decimalFormat.format(Float.parseFloat(menuItems.get(position).getPrice1())));

        if(language.equalsIgnoreCase("En")) {
            itemName_title.setText("" + menuItems.get(position).getItemName());
            itemName.setText("" + menuItems.get(position).getDesc());
        }
        else {
            itemName_title.setText("" + menuItems.get(position).getItemName_ar());
            itemName.setText("" + menuItems.get(position).getDesc_ar());
        }

        qty = 1;
        price = Float.parseFloat(menuItems.get(position).getPrice1());

        if(menuItems.get(position).getItemId().equals("32")){
            additionals_count = 16;
            total_additionals = 16;
            grid_count.setText("0/"+additionals_count);
        }
        else if(menuItems.get(position).getItemId().equals("33")){
            additionals_count = 16;
            total_additionals = 16;
            grid_count.setText("0/"+additionals_count);
        }
        grid_count.setVisibility(View.INVISIBLE);

        try {
            addl_ids.clear();
            addl_pos.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(!menuItems.get(position).getAdditionalId().equals("null")) {
            new GetAdditionals().execute(Constants.ADDITIONALS_URL + menuItems.get(position).getAdditionalId());
        }
        else{
            custom_layout.setVisibility(View.INVISIBLE);
            line_spice.setVisibility(View.GONE);
            Glide.with(getApplicationContext()).load(Constants.IMAGE_URL + menuItems.get(position).getImage()).into(itemImage);
        }
    }

    public class GetMenuItems extends AsyncTask<String, Integer, String>{

        String response;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            Constants.menuItems.clear();
            dialog = new MaterialDialog.Builder(context)
                    .title(R.string.app_name)
                    .content("Loading...")
                    .progress(true, 0)
                    .cancelable(false)
                    .progressIndeterminateStyle(true)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .show();

        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONArray jA = new JSONArray(result);
                            for (int i =0; i<jA.length(); i++){
                                JSONObject jsonObject = jA.getJSONObject(i);
                                MenuItems items = new MenuItems();

                                JSONObject keyObj = jsonObject.getJSONObject("Key");
                                items.setCatId(keyObj.getString("CategoryId"));
                                items.setItemId(keyObj.getString("ItemId"));
                                items.setItemName(keyObj.getString("ItemName"));
                                items.setModifierID(keyObj.getString("ModifierId"));
                                items.setItemName_ar(keyObj.getString("ItemName_Ar"));
                                items.setDesc(keyObj.getString("Description"));
                                items.setDesc_ar(keyObj.getString("Description_Ar"));
                                items.setAdditionalId(keyObj.getString("AdditionalsId"));
                                items.setImage(keyObj.getString("Images"));

                                if(position == Integer.parseInt(keyObj.getString("ItemId"))){
                                    position = i;
                                    Constants.ItemPosition = position;
                                }

                                int count = 0;
                                JSONObject priceObj = jsonObject.getJSONObject("Value");
                                if(priceObj.has("1")){
                                    count = count+1;
                                    items.setPrice1(priceObj.getString("1"));
                                }
                                else{
                                    items.setPrice1("-1");
                                }

                                if(priceObj.has("2")){
                                    count = count+1;
                                    items.setPrice2(priceObj.getString("2"));
                                }
                                else{
                                    items.setPrice2("-1");
                                }

                                if(priceObj.has("3")){
                                    count = count+1;
                                    items.setPrice3(priceObj.getString("3"));
                                }
                                else{
                                    items.setPrice3("-1");
                                }

                                if(priceObj.has("4")){
                                    count = count+1;
                                    items.setPrice4(priceObj.getString("4"));
                                }
                                else{
                                    items.setPrice4("-1");
                                }

                                if(priceObj.has("5")){
                                    count = count+1;
                                    items.setPrice5(priceObj.getString("5"));
                                }
                                else{
                                    items.setPrice5("-1");
                                }

                                if(priceObj.has("6")){
                                    count = count+1;
                                    items.setPrice6(priceObj.getString("6"));
                                }
                                else{
                                    items.setPrice6("-1");
                                }

                                if(priceObj.has("7")){
                                    count = count+1;
                                    items.setPrice7(priceObj.getString("7"));
                                }
                                else{
                                    items.setPrice7("-1");
                                }

                                if(count>1){
                                    items.setTwoCount(true);
                                }
                                else{
                                    items.setTwoCount(false);
                                }
                                Constants.menuItems.add(items);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(Constants.menuItems.size()>0) {
                    menuItems = Constants.menuItems;
                    setData();
                }
                else{
                    FooterActivity.tabBarPosition = 1;
                    Intent intent = new Intent(SaladActivity.this, HomeScreenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
        }
    }
}
