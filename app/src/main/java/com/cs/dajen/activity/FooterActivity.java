package com.cs.dajen.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.R;
import com.nex3z.notificationbadge.NotificationBadge;

/**
 * Created by CS on 24-04-2017.
 */

public class FooterActivity extends LinearLayout {

    private ImageView main;
    private ImageView order;
    private ImageView store;
    private ImageView more;
    public static LinearLayout tabbar;
    public static int tabBarPosition = 1;
    public static NotificationBadge mBadge;
    private DataBaseHelper myDbHelper;

    SharedPreferences languagePrefs, userPrefs;
    String language;

    private Activity mActivity;

    public FooterActivity(Context context, AttributeSet attrs) {
        super(context, attrs);

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li;

        languagePrefs = getContext().getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        userPrefs = getContext().getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        li = (LayoutInflater) getContext().getSystemService(infService);

        if(language.equalsIgnoreCase("En")) {
            li.inflate(R.layout.home_footer, this, true);
        }else if(language.equalsIgnoreCase("Ar")){
            li.inflate(R.layout.home_footer_ar, this, true);
        }

        myDbHelper = new DataBaseHelper(getContext());
        mBadge = (NotificationBadge) findViewById(R.id.badge);

        main = (ImageView) findViewById(R.id.tabbar_main);
        order = (ImageView) findViewById(R.id.tabbar_order);
        store = (ImageView) findViewById(R.id.tabbar_stores);
        more = (ImageView) findViewById(R.id.tabbar_more);
        tabbar = (LinearLayout) findViewById(R.id.tabbar);

        main.setOnClickListener(mFooterListener);
        order.setOnClickListener(mFooterListener);
        store.setOnClickListener(mFooterListener);
        more.setOnClickListener(mFooterListener);
    }

    public void setActivity(Activity activity) {
        // set init otherwise of ctor and call externally...
        mActivity = activity;
    }

    // Create an anonymous implementation of OnClickListener
    private OnClickListener mFooterListener = new OnClickListener() {
        public void onClick(View v) {
            // Intent myIntent;
            switch (v.getId()) {
                case R.id.tabbar_main:
                    if(language.equalsIgnoreCase("En")) {
                        tabbar.setBackgroundResource(R.drawable.tabbar1);
                    }
                    else{
                        tabbar.setBackgroundResource(R.drawable.tabbar4);
                    }

                    if(tabBarPosition!=1) {
                        tabBarPosition = 1;
                        if(Constants.CurrentOrderActivity.equals("fav_home")) {
                            Intent intent = new Intent(mActivity, FavoriteOrdersActivity.class);
                            intent.putExtra("screen","home");
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            mActivity.startActivity(intent);
                            mActivity.finish();
                        }
                        else {
                            Intent intent = new Intent(mActivity, HomeScreenActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            mActivity.startActivity(intent);
                            mActivity.finish();
                        }
//                        mActivity.startActivity(new Intent(mActivity, HomeScreenActivity.class));
//                        mActivity.finish();
                    }
                    break;
                case R.id.tabbar_order:
                    if(language.equalsIgnoreCase("En")) {
                        tabbar.setBackgroundResource(R.drawable.tabbar2);
                    }
                    else{
                        tabbar.setBackgroundResource(R.drawable.tabbar3);
                    }

                    if(tabBarPosition!=2) {
                        tabBarPosition = 2;
//                        if(userPrefs.getString("menu","1").equals("main")) {
                            Log.i("TAG","main");
                            if (Constants.CurrentOrderActivity.equals("inside")) {
                                Intent insideIntent = new Intent(mActivity, InsideProductActivity.class);
                                insideIntent.putExtra("position", Constants.ItemPosition);
                                insideIntent.putExtra("arraylist", Constants.menuItems);
                                mActivity.startActivity(insideIntent);
                                mActivity.finish();
                            } else if (Constants.CurrentOrderActivity.equals("salad")) {
                                Log.i("TAG","menu "+Constants.menuItems.size());
                                Intent sideItemsIntent = new Intent(mActivity, SaladActivity.class);
                                sideItemsIntent.putExtra("position", Constants.ItemPosition);
                                sideItemsIntent.putExtra("arraylist", Constants.menuItems);
                                mActivity.startActivity(sideItemsIntent);
                                mActivity.finish();
                            }
                            else if (Constants.CurrentOrderActivity.equals("dajenBBQ")) {
                                Intent insideIntent = new Intent(mActivity, DajenBBQActivity.class);
                                insideIntent.putExtra("position", Constants.ItemPosition);
                                insideIntent.putExtra("arraylist", Constants.menuItems);
                                mActivity.startActivity(insideIntent);
                                mActivity.finish();
                            }
                            else if (Constants.CurrentOrderActivity.equals("burger")) {
                                Intent insideIntent = new Intent(mActivity, BBQBurgerActivity.class);
                                insideIntent.putExtra("position", Constants.ItemPosition);
                                insideIntent.putExtra("arraylist", Constants.menuItems);
                                mActivity.startActivity(insideIntent);
                                mActivity.finish();
                            }else if (Constants.CurrentOrderActivity.equals("product")) {
                                Intent sideItemsIntent = new Intent(mActivity, ProductActivity.class);
                                sideItemsIntent.putExtra("id", Constants.Itemid);
                                mActivity.startActivity(sideItemsIntent);
                                mActivity.finish();
                            } else if (Constants.CurrentOrderActivity.equals("menu")) {
                                Intent menuIntent = new Intent(mActivity, MenuActivity.class);
                                menuIntent.putExtra("type", "main");
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            }else if (Constants.CurrentOrderActivity.equals("bbq")) {
                                Intent menuIntent = new Intent(mActivity, BBQatHome.class);
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            }
                            else if (Constants.CurrentOrderActivity.equals("checkout")) {
                                Intent menuIntent = new Intent(mActivity, CheckoutActivity.class);
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            } else if (Constants.CurrentOrderActivity.equals("ordertype")) {
                                Intent menuIntent = new Intent(mActivity, OrderTypeActivity.class);
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            }   else if (Constants.CurrentOrderActivity.equals("bbqMenu")) {
                                    Intent menuIntent = new Intent(mActivity, BBQatHomeMenu.class);
                                    mActivity.startActivity(menuIntent);
                                    mActivity.finish();
                                }
                                else if(Constants.CurrentOrderActivity.equals("bbqProduct")){
                                    Intent menuIntent = new Intent(mActivity, BBQProductDetails.class);
                                    mActivity.startActivity(menuIntent);
                                    mActivity.finish();
                                }
                             else if (Constants.CurrentOrderActivity.equals("stores")) {
                                Intent menuIntent = new Intent(mActivity, OrderTypeActivity.class);
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            }
                             else if (Constants.CurrentOrderActivity.equals("address")) {
                                Intent menuIntent = new Intent(mActivity, AddressActivity.class);
                                menuIntent.putExtra("confirm_order", true);
                                menuIntent.putExtra("class", "footer");
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            } else if (Constants.CurrentOrderActivity.equals("confirmation")) {
                                Intent menuIntent = new Intent(mActivity, OrderTypeActivity.class);
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            } else if (Constants.CurrentOrderActivity.equals("completed")) {
                                Intent menuIntent = new Intent(mActivity, OrderCompleted.class);
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            } else if (Constants.CurrentOrderActivity.equals("menutype")) {
                                Intent menuIntent = new Intent(mActivity, MenuType.class);
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            }
                            else if (Constants.CurrentOrderActivity.equals("tracking")) {
                                Intent menuIntent = new Intent(mActivity, TrackOrder.class);
                                menuIntent.putExtra("screen","home");
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            }
                            else if (Constants.CurrentOrderActivity.equals("testing")) {
                                Intent menuIntent = new Intent(mActivity, TestingMenuActivity.class);
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            }
                            else if (Constants.CurrentOrderActivity.equals("fav_order")) {
                                Intent menuIntent = new Intent(mActivity, FavoriteOrdersActivity.class);
                                menuIntent.putExtra("screen", "order");
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            } else if (Constants.CurrentOrderActivity.equals("history")) {
                                Intent menuIntent = new Intent(mActivity, OrderHistoryActivity.class);
                                mActivity.startActivity(menuIntent);
                                mActivity.finish();
                            }  else {
                                if (myDbHelper.getTotalOrderQty() > 0) {
                                    if (userPrefs.getString("menu", "main").equals("main")) {
                                        Intent menuIntent = new Intent(mActivity, MenuActivity.class);
                                        mActivity.startActivity(menuIntent);
                                        mActivity.finish();
                                    } else if (userPrefs.getString("menu", "main").equals("bbq")) {
                                        Intent menuIntent = new Intent(mActivity, BBQatHome.class);
                                        mActivity.startActivity(menuIntent);
                                        mActivity.finish();
                                    }
                                } else {
                                    Intent menuIntent = new Intent(mActivity, OrderActivity.class);
                                    mActivity.startActivity(menuIntent);
                                    mActivity.finish();
                                }
                            }
                        }
//                        else if (userPrefs.getString("menu", "main").equals("bbq")) {
//                            Log.i("TAG","bbq");
//                            if (Constants.CurrentOrderActivity.equals("bbqMenu")) {
//                                Intent menuIntent = new Intent(mActivity, BBQatHomeMenu.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            }
//                            else if(Constants.CurrentOrderActivity.equals("bbqProduct")){
//                                Intent menuIntent = new Intent(mActivity, BBQProductDetails.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            }
//                            else if (Constants.CurrentOrderActivity.equals("checkout")) {
//                                Intent menuIntent = new Intent(mActivity, CheckoutActivity.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("ordertype")) {
//                                Intent menuIntent = new Intent(mActivity, OrderTypeActivity.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
////                            } else if (Constants.CurrentOrderActivity.equals("stores")) {
////                                Intent menuIntent = new Intent(mActivity, SelectStoresActivity.class);
////                                mActivity.startActivity(menuIntent);
////                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("address")) {
//                                Intent menuIntent = new Intent(mActivity, AddressActivity.class);
//                                menuIntent.putExtra("confirm_order", true);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("confirmation")) {
//                                Intent menuIntent = new Intent(mActivity, Confirmation.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("completed")) {
//                                Intent menuIntent = new Intent(mActivity, OrderCompleted.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("menutype")) {
//                                Intent menuIntent = new Intent(mActivity, MenuType.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("fav_order")) {
//                                Intent menuIntent = new Intent(mActivity, FavoriteOrdersActivity.class);
//                                menuIntent.putExtra("screen", "order");
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("history")) {
//                                Intent menuIntent = new Intent(mActivity, OrderHistoryActivity.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            }
//                            else {
//                                if (myDbHelper.getTotalOrderQty() > 0) {
//                                        Intent menuIntent = new Intent(mActivity, MenuType.class);
//                                        getContext().startActivity(menuIntent);
//                                        mActivity.finish();
//                                } else {
//                                    Intent menuIntent = new Intent(mActivity, OrderActivity.class);
//                                    mActivity.startActivity(menuIntent);
//                                    mActivity.finish();
//                                }
//                            }
//                        }
//                        else{
//                            Log.i("TAG","else");
//                            if (Constants.CurrentOrderActivity.equals("inside")) {
//                                Intent insideIntent = new Intent(mActivity, InsideProductActivity.class);
//                                insideIntent.putExtra("position", Constants.ItemPosition);
//                                insideIntent.putExtra("arraylist", Constants.menuItems);
//                                mActivity.startActivity(insideIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("salad")) {
//                                Intent sideItemsIntent = new Intent(mActivity, SaladActivity.class);
//                                sideItemsIntent.putExtra("position", Constants.ItemPosition);
//                                sideItemsIntent.putExtra("arraylist", Constants.menuItems);
//                                mActivity.startActivity(sideItemsIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("product")) {
//                                Intent sideItemsIntent = new Intent(mActivity, ProductActivity.class);
//                                sideItemsIntent.putExtra("id", Constants.Itemid);
//                                mActivity.startActivity(sideItemsIntent);
//                                mActivity.finish();
//                            }
//                            else if (Constants.CurrentOrderActivity.equals("bbqMenu")) {
//                                Intent menuIntent = new Intent(mActivity, BBQatHomeMenu.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            }
//                            else if(Constants.CurrentOrderActivity.equals("bbqProduct")){
//                                Intent menuIntent = new Intent(mActivity, BBQProductDetails.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            }else if (Constants.CurrentOrderActivity.equals("menu")) {
//                                Intent menuIntent = new Intent(mActivity, MenuActivity.class);
//                                menuIntent.putExtra("type", "main");
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("checkout")) {
//                                Intent menuIntent = new Intent(mActivity, CheckoutActivity.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("ordertype")) {
//                                Intent menuIntent = new Intent(mActivity, OrderTypeActivity.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
////                            } else if (Constants.CurrentOrderActivity.equals("stores")) {
////                                Intent menuIntent = new Intent(mActivity, SelectStoresActivity.class);
////                                mActivity.startActivity(menuIntent);
////                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("address")) {
//                                Intent menuIntent = new Intent(mActivity, AddressActivity.class);
//                                menuIntent.putExtra("confirm_order", true);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("confirmation")) {
//                                Intent menuIntent = new Intent(mActivity, Confirmation.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("completed")) {
//                                Intent menuIntent = new Intent(mActivity, OrderCompleted.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("menutype")) {
//                                Intent menuIntent = new Intent(mActivity, MenuType.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("fav_order")) {
//                                Intent menuIntent = new Intent(mActivity, FavoriteOrdersActivity.class);
//                                menuIntent.putExtra("screen", "order");
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            } else if (Constants.CurrentOrderActivity.equals("history")) {
//                                Intent menuIntent = new Intent(mActivity, OrderHistoryActivity.class);
//                                mActivity.startActivity(menuIntent);
//                                mActivity.finish();
//                            }  else {
//                                if (myDbHelper.getTotalOrderQty() > 0) {
//                                    Intent menuIntent = new Intent(mActivity, MenuType.class);
//                                    getContext().startActivity(menuIntent);
//                                    mActivity.finish();
//                                } else {
//                                    Intent menuIntent = new Intent(mActivity, OrderActivity.class);
//                                    mActivity.startActivity(menuIntent);
//                                    mActivity.finish();
//                                }
//                            }
//                        }
//                    }
//                    mActivity.startActivity(new Intent(mActivity,Info.class));

                    break;
                case R.id.tabbar_stores:

                    if(language.equalsIgnoreCase("En")) {
                        tabbar.setBackgroundResource(R.drawable.tabbar3);
                    }
                    else{
                        tabbar.setBackgroundResource(R.drawable.tabbar2);
                    }

                    if(tabBarPosition!=3) {
                        tabBarPosition = 3;
                        Intent intent = new Intent(mActivity, SelectStoresActivity.class);
                        intent.putExtra("class", "main");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mActivity.startActivity(intent);
                        mActivity.finish();
                    }
                    break;

                case R.id.tabbar_more:

                    if(language.equalsIgnoreCase("En")) {
                        tabbar.setBackgroundResource(R.drawable.tabbar4);
                    }
                    else{
                        tabbar.setBackgroundResource(R.drawable.tabbar1);
                    }

                    if(tabBarPosition!=4) {
                        tabBarPosition = 4;
                        Intent intent1 = new Intent(mActivity, MoreActivity.class);
                        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        mActivity.startActivity(intent1);
                        mActivity.finish();
                    }
                    break;
                default:
                    break;
            }

        }
    };

}
