package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.dajen.Adapters.AddressAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.Address;
import com.cs.dajen.Models.StoreInfo;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;

/**
 * Created by CS on 12-06-2016.
 */
public class AddressActivity extends AppCompatActivity {
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int SAVE_ADDRESS_REQUEST = 2;
    private static final int EDIT_ADDRESS_REQUEST = 3;

    private String timeResponse = null;
    String serverTime;

    ImageView addAddress;
    ArrayList<Address> addressList = new ArrayList<>();
    SwipeMenuListView addressListView;
    AddressAdapter mAdapter;
    Toolbar toolbar;
    Double lat, longi;
    String response;
    int mPosition;
    private ArrayList<StoreInfo> storesList = new ArrayList<>();
    boolean toConfirmOrder;
    SharedPreferences userPrefs;
    String userId, mWhichclasss;
    TextView title;
    private DataBaseHelper myDbHelper;
    AlertDialog customDialog;
    MaterialDialog dialog;
    ImageView back_btn;
    SharedPreferences languagePrefs;
    String language;

    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(24.70321657, 46.68097073), new LatLng(24.80321657, 47.68097073));

    //    String language;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.address_activity);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.address_activity_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);

        try {
            toConfirmOrder = getIntent().getExtras().getBoolean("confirm_order", false);
        } catch (Exception npe) {
            toConfirmOrder = false;
        }

        try {
            mWhichclasss = getIntent().getStringExtra("class");
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (toConfirmOrder) {
            FooterActivity.tabBarPosition = 2;
            Constants.CurrentOrderActivity = "address";
            if (language.equalsIgnoreCase("En")) {
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
            } else {
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
            }
        } else {
            FooterActivity.tabBarPosition = 4;
            if (language.equalsIgnoreCase("En")) {
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
            } else {
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar1);
            }
        }

        addAddress = (ImageView) findViewById(R.id.add_address_btn);
        title = (TextView) findViewById(R.id.header_title);
        back_btn = (ImageView) findViewById(R.id.address_backbtn);
//        addressTxt = (TextView) findViewById(R.id.address);
        addressListView = (SwipeMenuListView) findViewById(R.id.address_listView);
        mAdapter = new AddressAdapter(AddressActivity.this, addressList, language);
        addressListView.setAdapter(mAdapter);
//        if(language.equalsIgnoreCase("En")){
//        }else if(language.equalsIgnoreCase("Ar")){
//            title.setText("عنواني");
//            addAddress.setText("إضافة عنوان جديد");
//        }

        myDbHelper = new DataBaseHelper(this);
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mWhichclasss != null && mWhichclasss.equals("footer")) {
                    startActivity(new Intent(AddressActivity.this, OrderTypeActivity.class));
                    finish();
                } else {
                    finish();
                }
            }
        });
        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(AddressActivity.this, MapsActivity.class);
//                startActivityForResult(i, PLACE_PICKER_REQUEST);

                try {
                    PlacePicker.IntentBuilder intentBuilder =
                            new PlacePicker.IntentBuilder();
//                    intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    Intent intent = intentBuilder.build(AddressActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        addressListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (toConfirmOrder) {
                    mPosition = position;
                    if (addressList.get(mPosition).getId() == null || addressList.get(mPosition).getId().equals("")) {
                        Toast.makeText(AddressActivity.this, getResources().getString(R.string.select_another_address), Toast.LENGTH_SHORT).show();
                    } else {
                        lat = Double.parseDouble(addressList.get(position).getLatitude());
                        longi = Double.parseDouble(addressList.get(position).getLongitude());
                        new GetCurrentTime().execute();
                    }
                }
            }
        });

        // step 1. create a MenuCreator
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {


                // create "edit" item
                SwipeMenuItem editItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                editItem.setBackground(new ColorDrawable(Color.rgb(0x25,
                        0xAE, 0x88)));
                // set item width
                editItem.setWidth(dp2px(90));

                if (language.equalsIgnoreCase("En")) {
                    editItem.setTitle("Edit");
                } else {
                    editItem.setTitle(R.string.edit_ar);
                }
                // set item title fontsize
                editItem.setTitleSize(18);
                // set item title font color
                editItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(editItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(dp2px(90));

                if (language.equalsIgnoreCase("En")) {
                    deleteItem.setTitle("Delete");
                } else if (language.equalsIgnoreCase("Ar")) {
                    deleteItem.setTitle("حذف");
                }
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        // set creator
        addressListView.setMenuCreator(creator);

        // step 2. listener item click event
        addressListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        Intent intent = new Intent(AddressActivity.this, EditAddressActivity.class);
                        intent.putExtra("address", addressList.get(position).getAddress());
                        intent.putExtra("latitude", addressList.get(position).getLatitude());
                        intent.putExtra("longitude", addressList.get(position).getLongitude());
                        intent.putExtra("image", addressList.get(position).getAddressImage());
                        intent.putExtra("id", addressList.get(position).getId());
                        if (!addressList.get(position).getLandmark().equals("null")) {
                            intent.putExtra("landmark", addressList.get(position).getLandmark());
                        }
                        intent.putExtra("address_type", addressList.get(position).getAddressType());
                        intent.putExtra("house_no", addressList.get(position).getHouseNo());
                        intent.putExtra("house_name", addressList.get(position).getAddressName());
                        startActivityForResult(intent, EDIT_ADDRESS_REQUEST);
                        break;
                    case 1:
                        new DeleteAddress().execute(Constants.DELETE_ADDRESS_URL + addressList.get(position).getId());
                        break;
                }
                return false;
            }
        });

        new GetAddressDetails().execute(Constants.SAVED_ADDRESS_URL + userId);
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (mWhichclasss != null && mWhichclasss.equals("footer")) {
            startActivity(new Intent(AddressActivity.this, OrderTypeActivity.class));
            finish();
        } else {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST
                && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(this, data);

            if ("".equals(place.getAddress())) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddressActivity.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                TextView title = (TextView) dialogView.findViewById(R.id.title);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                no.setVisibility(View.GONE);
                vert.setVisibility(View.GONE);
                if (language.equalsIgnoreCase("En")) {
                    title.setText(getResources().getString(R.string.dajen));
                    yes.setText(getResources().getString(R.string.ok));
                    desc.setText(R.string.cannot_detect_location);
                } else {
                    title.setText(getResources().getString(R.string.dajen_ar));
                    yes.setText(getResources().getString(R.string.ok_ar));
                    desc.setText(R.string.cannot_detect_location_ar);
                }
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                    }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth * 0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            } else {
//                Intent intent = new Intent(AddressActivity.this, SaveAddressActivity.class);
//                intent.putExtra("address", data.getStringExtra("address"));
//                intent.putExtra("latitude", Double.toString(data.getDoubleExtra("lat",0)));
//                intent.putExtra("longitude", Double.toString(data.getDoubleExtra("longi",0)));
//                startActivityForResult(intent, SAVE_ADDRESS_REQUEST);

                Intent intent = new Intent(AddressActivity.this, SaveAddressActivity.class);
                intent.putExtra("address", place.getAddress());
                intent.putExtra("latitude", Double.toString(place.getLatLng().latitude));
                intent.putExtra("longitude", Double.toString(place.getLatLng().longitude));
                startActivityForResult(intent, SAVE_ADDRESS_REQUEST);
            }

        } else if (requestCode == SAVE_ADDRESS_REQUEST
                && resultCode == Activity.RESULT_OK) {
            new GetAddressDetails().execute(Constants.SAVED_ADDRESS_URL + userId);
        } else if (requestCode == EDIT_ADDRESS_REQUEST && resultCode == Activity.RESULT_OK) {
            new GetAddressDetails().execute(Constants.SAVED_ADDRESS_URL + userId);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public class GetAddressDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(AddressActivity.this);
            dialog = new MaterialDialog.Builder(AddressActivity.this)
                    .title(R.string.app_name)
                    .content(R.string.loading_address)
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
            addressList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "address:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            addressList.clear();
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(AddressActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    Address oh = new Address();
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    String Id = jo1.getString("Id");
                                    String Address = jo1.getString("Address");
                                    String AddressType = jo1.getString("AddressType");
                                    String HouseNo = jo1.getString("HouseNo");
                                    String LandMark = jo1.getString("LandMark");
                                    String Longitude = jo1.getString("Longitude");
                                    String Latitude = jo1.getString("Latitude");
                                    String houseName = jo1.getString("HouseName");
                                    String AddressImage = jo1.getString("AddressImage");

                                    oh.setId(Id);
                                    oh.setAddress(Address);
                                    oh.setAddressType(AddressType);
                                    oh.setHouseNo(HouseNo);
                                    oh.setLandmark(LandMark);
                                    oh.setLatitude(Latitude);
                                    oh.setLongitude(Longitude);
                                    oh.setAddressName(houseName);
                                    oh.setAddressImage(AddressImage);

                                    addressList.add(oh);

                                }
                            } catch (JSONException je) {
                                String msg = jo.getString("Failure");
                                if (msg.contains("No RecordFound")) {
                                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddressActivity.this);
                                    // ...Irrelevant code for customizing the buttons and title
                                    LayoutInflater inflater = getLayoutInflater();
                                    int layout = R.layout.alert_dialog;
                                    View dialogView = inflater.inflate(layout, null);
                                    dialogBuilder.setView(dialogView);
                                    dialogBuilder.setCancelable(false);

                                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                    View vert = (View) dialogView.findViewById(R.id.vert_line);
                                    TextView title = (TextView) dialogView.findViewById(R.id.title);

                                    if (language.equalsIgnoreCase("En")) {
                                        title.setText(getResources().getString(R.string.dajen));
                                        yes.setText(getResources().getString(R.string.ok));
                                        no.setText(getResources().getString(R.string.add_address));
                                        desc.setText(getResources().getString(R.string.no_saved_address));
                                    } else {
                                        title.setText(getResources().getString(R.string.dajen_ar));
                                        yes.setText(getResources().getString(R.string.ok_ar));
                                        no.setText(getResources().getString(R.string.add_address_ar));
                                        desc.setText(getResources().getString(R.string.no_saved_address_ar));
                                    }

                                    yes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                        }
                                    });

                                    no.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            customDialog.dismiss();
                                            Intent i = new Intent(AddressActivity.this, MapsActivity.class);
                                            startActivityForResult(i, PLACE_PICKER_REQUEST);
                                        }
                                    });

                                    customDialog = dialogBuilder.create();
                                    customDialog.show();
                                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                    Window window = customDialog.getWindow();
                                    lp.copyFrom(window.getAttributes());
                                    //This makes the dialog take up the full width
                                    Display display = getWindowManager().getDefaultDisplay();
                                    Point size = new Point();
                                    display.getSize(size);
                                    int screenWidth = size.x;

                                    double d = screenWidth * 0.85;
                                    lp.width = (int) d;
                                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                    window.setAttributes(lp);
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(AddressActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }


    public class DeleteAddress extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(AddressActivity.this);
            dialog = new MaterialDialog.Builder(AddressActivity.this)
                    .title(R.string.app_name)
                    .content(R.string.deleting_address)
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {

                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
//                    response = sb.toString();

                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(AddressActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);
                            String s = jo.getString("Success");
                            new GetAddressDetails().execute(Constants.SAVED_ADDRESS_URL + userId);
                            Toast.makeText(AddressActivity.this, "Address deleted successfully", Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(AddressActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            } else {
                Toast.makeText(AddressActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    public class GetStoresInfo extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        //        MaterialDialog dialog;
        String dayOfWeek;

        @Override
        protected void onPreExecute() {
            storesList.clear();
            networkStatus = NetworkUtil.getConnectivityStatusString(AddressActivity.this);
//            dialog = new MaterialDialog.Builder(AddressActivity.this)
//                    .title("TelePizza")
//                    .content("Fetching nearby stores...")
//                    .progress(true, 0)
//                    .progressIndeterminateStyle(true)
//                    .show();
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            // full name form of the day
            dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0] + dayOfWeek);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(AddressActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray ja = jsonObject.getJSONArray("Success");
                            for (int i = 0; i < ja.length(); i++) {
                                StoreInfo si = new StoreInfo();
                                JSONObject jo = ja.getJSONObject(i);
//                                lat = 24.70321657;
//                                longi = 46.68097073;
                                si.setStoreId(jo.getString("storeId"));
                                si.setOnlineOrderStatus(jo.getString("OnlineOrderStatus"));
                                si.setStartTime(jo.getString("ST"));
                                si.setEndTime(jo.getString("ET"));
                                si.setStoreName(jo.getString("StoreName"));
                                si.setStoreAddress(jo.getString("StoreAddress"));
                                si.setLatitude(jo.getDouble("Latitude"));
                                si.setLongitude(jo.getDouble("Longitude"));
                                si.setCountryName(jo.getString("CountryName"));
                                si.setCityName(jo.getString("CityName"));
                                si.setImageURL(jo.getString("imageURL"));
                                si.setDeliverydistance(jo.getInt("DeliveryDistance"));
                                si.setFamilySection(jo.getString("FamilySection"));
                                si.setWifi(jo.getString("Wifi"));
                                si.setPatioSitting(jo.getString("PatioSitting"));
                                si.setDriveThru(jo.getString("DriveThru"));
                                si.setMeetingSpace(jo.getString("MeetingSpace"));
                                si.setHospital(jo.getString("Hospital"));
                                si.setUniversity(jo.getString("University"));
                                si.setOffice(jo.getString("Office"));
                                si.setShoppingMall(jo.getString("ShoppingMall"));
                                try {
                                    si.setAirPort(jo.getString("Airport"));
                                } catch (Exception e) {
                                    si.setAirPort("false");
                                }
                                try {
                                    si.setDineIn(jo.getString("DineIn"));
                                } catch (Exception e) {
                                    si.setDineIn("false");
                                }
                                try {
                                    si.setLadies(jo.getString("Ladies"));
                                } catch (Exception e) {
                                    si.setLadies("false");
                                }

                                si.setNeighborhood(jo.getString("Neighborhood"));
                                si.setStoreNumber(jo.getString("phone"));
                                si.setIs24x7(jo.getString("is24x7"));
                                si.setStatus(jo.getString("status"));
                                si.setOgCountry(jo.getString("OGCountry"));
                                si.setOgCity(jo.getString("OGCity"));
                                si.setStoreName_ar(jo.getString("StoreName_ar"));
                                si.setStoreAddress_ar(jo.getString("StoreAddress_ar"));
                                si.setStoreNumber(jo.getString("phone"));
                                try {
                                    si.setMessage(jo.getString("Message"));
                                } catch (Exception e) {
                                    si.setMessage("");
                                }

                                try {
                                    si.setMessage_ar(jo.getString("Message_ar"));
                                } catch (Exception e) {
                                    si.setMessage_ar("");
                                }

                                Location me = new Location("");
                                Location dest = new Location("");

                                me.setLatitude(lat);
                                me.setLongitude(longi);

                                dest.setLatitude(jo.getDouble("Latitude"));
                                dest.setLongitude(jo.getDouble("Longitude"));
                                float dist = (me.distanceTo(dest)) / 1000;
                                si.setDistance(dist);
//                                storesList.add(si);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
                                SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                                SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy hh:mm a", Locale.US);
                                SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mma", Locale.US);
                                SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);

                                Calendar c = Calendar.getInstance();
                                serverTime = timeResponse;
                                String startTime = si.getStartTime();
                                String endTime = si.getEndTime();

                                if (dist <= si.getDeliverydistance() && (jo.getBoolean("OnlineOrderStatus"))) {

                                    if (startTime.equals("null") && endTime.equals("null")) {
//                                        si.setOpenFlag(-1);
//                                        storesList.add(si);
                                    } else {

                                        if (endTime.equals("00:00AM")) {
                                            si.setOpenFlag(1);
                                            storesList.add(si);

                                            continue;
                                        } else if (endTime.equals("12:00AM")) {
                                            endTime = "11:59PM";
                                        }

                                        Calendar now = Calendar.getInstance();

                                        int hour = now.get(Calendar.HOUR_OF_DAY);
                                        int minute = now.get(Calendar.MINUTE);


                                        Date serverDate = null;
                                        Date end24Date = null;
                                        Date start24Date = null;
                                        Date current24Date = null;
                                        Date dateToday = null;
                                        Calendar dateStoreClose = Calendar.getInstance();
                                        try {
                                            end24Date = dateFormat2.parse(endTime);
                                            start24Date = dateFormat2.parse(startTime);
                                            serverDate = dateFormat.parse(serverTime);
                                            dateToday = dateFormat.parse(serverTime);
                                            Log.e("TAG", "server time " + serverTime);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        Date startDate = null;
                                        Date endDate = null;
                                        try {
                                            dateStoreClose.setTime(dateToday);
                                            dateStoreClose.add(Calendar.DATE, 1);
                                            String current24 = timeFormat1.format(serverDate);
                                            String end24 = timeFormat1.format(end24Date);
                                            String start24 = timeFormat1.format(start24Date);
                                            String startDateString = dateFormat1.format(dateToday);
                                            String endDateString = dateFormat1.format(dateToday);
                                            String endDateTomorrow = dateFormat1.format(dateStoreClose.getTime());
                                            dateStoreClose.add(Calendar.DATE, -2);
                                            String endDateYesterday = dateFormat1.format(dateStoreClose.getTime());

                                            try {
                                                end24Date = timeFormat1.parse(end24);
                                                start24Date = timeFormat1.parse(start24);
                                                current24Date = timeFormat1.parse(current24);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            String[] parts2 = start24.split(":");
                                            int startHour = Integer.parseInt(parts2[0]);
                                            int startMinute = Integer.parseInt(parts2[1]);

                                            String[] parts = end24.split(":");
                                            int endHour = Integer.parseInt(parts[0]);
                                            int endMinute = Integer.parseInt(parts[1]);

                                            String[] parts1 = current24.split(":");
                                            int currentHour = Integer.parseInt(parts1[0]);
                                            int currentMinute = Integer.parseInt(parts1[1]);


//                    Log.i("DATE TAG", "" + start24Date.toString() + "  " + current24Date.toString() + " ");


                                            if (startTime.contains("AM") && endTime.contains("AM")) {
                                                if (startHour < endHour) {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateString + "  " + endTime;
                                                } else if (startHour > endHour) {
                                                    if (serverTime.contains("AM")) {
                                                        if (currentHour > endHour) {
                                                            startDateString = startDateString + " " + startTime;
                                                            endDateString = endDateTomorrow + "  " + endTime;
                                                        } else {
                                                            startDateString = endDateYesterday + " " + startTime;
                                                            endDateString = endDateString + "  " + endTime;
                                                        }
                                                    } else {
                                                        startDateString = startDateString + " " + startTime;
                                                        endDateString = endDateTomorrow + "  " + endTime;
                                                    }
                                                }
                                            } else if (startTime.contains("AM") && endTime.contains("PM")) {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateString + "  " + endTime;
                                            } else if (startTime.contains("PM") && endTime.contains("AM")) {
                                                if (serverTime.contains("AM")) {
                                                    if (currentHour <= endHour) {
                                                        startDateString = endDateYesterday + " " + startTime;
                                                        endDateString = endDateString + "  " + endTime;
                                                    } else {
                                                        startDateString = startDateString + " " + startTime;
                                                        endDateString = endDateTomorrow + "  " + endTime;
                                                    }
                                                } else {
                                                    startDateString = startDateString + " " + startTime;
                                                    endDateString = endDateTomorrow + "  " + endTime;
                                                }

                                            } else if (startTime.contains("PM") && endTime.contains("PM")) {
                                                startDateString = startDateString + " " + startTime;
                                                endDateString = endDateString + "  " + endTime;
                                            }

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        try {
                                            startDate = dateFormat2.parse(si.getStartTime());
                                            endDate = dateFormat2.parse(si.getEndTime());
                                            Log.i("TAG","start date "+startDate);
                                            Log.i("TAG","end date "+endDate);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        String serverDateString = null;
                                        try {
                                             serverDateString = dateFormat.format(serverDate);
                                             Log.i("TAG","server date"+serverDateString);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        try {
                                            serverDate = dateFormat.parse(serverDateString);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        try {
                                            if (serverDate.after(startDate) && serverDate.before(endDate)) {
                                                si.setOpenFlag(1);
                                                storesList.add(si);
                                            } else {
                                            si.setOpenFlag(0);
                                            storesList.add(si);
                                            }
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Collections.sort(storesList, StoreInfo.storeDistance);
                        if (storesList.size() > 0) {
                            StoreInfo si = storesList.get(0);

                            int distance = si.getDeliverydistance();

                            double lat, longi;
                            lat = Double.parseDouble(addressList.get(mPosition).getLatitude());
                            longi = Double.parseDouble(addressList.get(mPosition).getLongitude());
                            Intent intent = new Intent(AddressActivity.this, Confirmation.class);
                            intent.putExtra("your_address", addressList.get(mPosition).getAddress());
                            intent.putExtra("address_id", addressList.get(mPosition).getId());
                            intent.putExtra("lat", lat);
                            intent.putExtra("longi", longi);
                            intent.putExtra("landmark", addressList.get(mPosition).getLandmark());
                            intent.putExtra("storeName", si.getStoreName());
                            intent.putExtra("storeAddress", si.getStoreAddress());
                            intent.putExtra("storePhone", si.getStoreNumber());
                            intent.putExtra("storeId", si.getStoreId());
                            intent.putExtra("latitude", si.getLatitude());
                            intent.putExtra("longitude", si.getLongitude());
                            intent.putExtra("start_time", si.getStartTime());
                            intent.putExtra("end_time", si.getEndTime());
                            intent.putExtra("full_hours", si.getIs24x7());
                            intent.putExtra("order_type", Constants.ORDER_TYPE);
                            intent.putExtra("distance", distance);
                            startActivity(intent);
                        } else {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AddressActivity.this);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getLayoutInflater();
                            int layout = R.layout.alert_dialog;
                            View dialogView = inflater.inflate(layout, null);
                            dialogBuilder.setView(dialogView);
                            dialogBuilder.setCancelable(false);

                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                            View vert = (View) dialogView.findViewById(R.id.vert_line);
                            TextView title = (TextView) dialogView.findViewById(R.id.title);

                            no.setVisibility(View.GONE);
                            vert.setVisibility(View.GONE);
                            if (language.equalsIgnoreCase("En")) {
                                title.setText(getResources().getString(R.string.dajen));
                                yes.setText(getResources().getString(R.string.ok));
                                desc.setText(getResources().getString(R.string.no_store_found));
                            } else {
                                title.setText(getResources().getString(R.string.dajen_ar));
                                yes.setText(getResources().getString(R.string.ok_ar));
                                desc.setText(getResources().getString(R.string.no_store_found_ar));
                            }

                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    customDialog.dismiss();
                                }
                            });

                            customDialog = dialogBuilder.create();
                            customDialog.show();
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = customDialog.getWindow();
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int screenWidth = size.x;

                            double d = screenWidth * 0.85;
                            lp.width = (int) d;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                        }
                    }
                }

            } else {
                Toast.makeText(AddressActivity.this, getResources().getString(R.string.cannot_reach_server), Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
//            mAdapter.notifyDataSetChanged();

            super.onPostExecute(result);

        }

    }


    public class GetCurrentTime extends AsyncTask<String, String, String> {
        URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(AddressActivity.this);
            dialog = new MaterialDialog.Builder(AddressActivity.this)
                    .title(R.string.app_name)
                    .content(R.string.fetching_stores)
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            } else {
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if (serverTime == null) {
//                dialog.dismiss();
            } else if (serverTime.equals("no internet")) {
//                dialog.dismiss();
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            } else {
//                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
//                    timeResponse = "06/02/2018 05:40 PM";
                } catch (JSONException je) {
                    je.printStackTrace();
                }
                new GetStoresInfo().execute(Constants.STORES_URL);
//                if(language.equalsIgnoreCase("En")) {

//                }else if(language.equalsIgnoreCase("Ar")){
//                    mStoreListAdapterArabic = new StoreListAdapterArabic(getActivity(), totalStoresList, timeResponse);
//                    mStoresListView.setAdapter(mStoreListAdapterArabic);
//                }

            }


            super.onPostExecute(result1);
        }
    }

}
