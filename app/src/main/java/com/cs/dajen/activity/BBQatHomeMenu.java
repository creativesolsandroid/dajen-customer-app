package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Adapters.BBQMenuAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.MenuItems;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

/**
 * Created by CS on 02-06-2017.
 */

public class BBQatHomeMenu extends Activity implements View.OnClickListener {

    GridView gridView;
    int Itemid;
    ImageView back_btn;
    BBQMenuAdapter menuItemsAdapter;
    private DataBaseHelper myDbHelper;
    TextView cart_count,footerAmount,footerQty,footerCheckout,footer_addmore;
    RelativeLayout cart;
    Context ctx;
    SharedPreferences languagePrefs;
    String language;
    AlertDialog customDialog;

    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.bbq_at_home_menu);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.bbq_at_home_menu_ar);
        }
        ctx = this;

        Constants.CurrentOrderActivity = "bbqMenu";
        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;

        if (language.equalsIgnoreCase("En")) {
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        } else {
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        gridView = (GridView) findViewById(R.id.products_grid);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        cart = (RelativeLayout) findViewById(R.id.cart);
        cart_count = (TextView) findViewById(R.id.cart_count);

        footer_addmore = (TextView) findViewById(R.id.footer_addmore);
        footerAmount = (TextView) findViewById(R.id.footer_amount);
        footerQty = (TextView) findViewById(R.id.footer_qty);
        footerCheckout = (TextView) findViewById(R.id.footer_checkout);

        if(Constants.BBQMenu!=null && Constants.BBQMenu.size()>0) {

        }
        else{
            new GetMenuItems().execute(Constants.GET_ITEMS + "9");
        }

        back_btn.setOnClickListener(this);
        cart.setOnClickListener(this);
        footerCheckout.setOnClickListener(this);
        footer_addmore.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.footer_addmore:
                Intent mainIntent1 = new Intent(BBQatHomeMenu.this, BBQatHome.class);
                mainIntent1.putExtra("type","main");
                startActivity(mainIntent1);
                finish();
                break;

            case R.id.footer_checkout:
                if(myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHomeMenu.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.cart_noitems));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.cart_noitems_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else {
                    startActivity(new Intent(BBQatHomeMenu.this, CheckoutActivity.class));
                }
                break;

            case R.id.back_btn:
//                if(myDbHelper.getTotalOrderQty() > 0) {
//                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHomeMenu.this);
//                    // ...Irrelevant code for customizing the buttons and title
//                    LayoutInflater inflater = getLayoutInflater();
//                    int layout = R.layout.alert_dialog;
//                    View dialogView = inflater.inflate(layout, null);
//                    dialogBuilder.setView(dialogView);
//                    dialogBuilder.setCancelable(false);
//
//                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//                    View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//                    no.setText(getResources().getString(R.string.no));
//                    yes.setText(getResources().getString(R.string.yes));
//                    desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
//
//                    yes.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            customDialog.dismiss();
//                            myDbHelper.deleteOrderTable();
//                            startActivity(new Intent(BBQatHomeMenu.this, MenuType.class));
//                            finish();
//                        }
//                    });
//
//                    no.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            customDialog.dismiss();
//                        }
//                    });
//
//                    customDialog = dialogBuilder.create();
//                    customDialog.show();
//                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//                    Window window = customDialog.getWindow();
//                    lp.copyFrom(window.getAttributes());
//                    //This makes the dialog take up the full width
//                    Display display = getWindowManager().getDefaultDisplay();
//                    Point size = new Point();
//                    display.getSize(size);
//                    int screenWidth = size.x;
//
//                    double d = screenWidth*0.85;
//                    lp.width = (int) d;
//                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//                    window.setAttributes(lp);
//                }
//                else{
                startActivity(new Intent(BBQatHomeMenu.this, BBQatHome.class));
                finish();
//                }
                break;

            case R.id.cart:
                if (myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHomeMenu.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.cart_noitems));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.cart_noitems_ar));
                    }
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                } else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHomeMenu.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.cart_clear));
                        no.setText(getResources().getString(R.string.checkout_title));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.cart_clear_ar));
                        no.setText(getResources().getString(R.string.checkout_title_ar));
                        desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            menuItemsAdapter.notifyDataSetChanged();
                            FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
                            cart_count.setText("" + myDbHelper.getTotalOrderQty());
                            footerQty.setText("" + myDbHelper.getTotalOrderQty());
                            footerAmount.setText(""+ decimalFormat.format(myDbHelper.getTotalOrderPrice()));
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            Intent checkoutIntent = new Intent(BBQatHomeMenu.this, CheckoutActivity.class);
                            startActivity(checkoutIntent);

                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;
        }
    }

    public class GetMenuItems extends AsyncTask<String, Integer, String> {

        String response;
        String networkStatus;
        MaterialDialog dialog;

        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            try {
                Constants.BBQMenu.clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog = new MaterialDialog.Builder(ctx)
                    .title(R.string.app_name)
                    .content("Loading Items...")
                    .progress(true, 0)
                    .cancelable(false)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();

        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    Toast.makeText(getApplicationContext(), "Connection Error! Please check the internet connection", Toast.LENGTH_SHORT).show();

                } else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONArray jA = new JSONArray(result);
                            for (int i = 0; i < jA.length(); i++) {
                                JSONObject jsonObject = jA.getJSONObject(i);
                                MenuItems items = new MenuItems();

                                JSONObject keyObj = jsonObject.getJSONObject("Key");
                                items.setCatId(keyObj.getString("CategoryId"));
                                items.setItemId(keyObj.getString("ItemId"));
                                items.setItemName(keyObj.getString("ItemName"));
                                items.setModifierID(keyObj.getString("ModifierId"));
                                items.setItemName_ar(keyObj.getString("ItemName_Ar"));
                                items.setDesc(keyObj.getString("Description"));
                                items.setDesc_ar(keyObj.getString("Description_Ar"));
                                items.setAdditionalId(keyObj.getString("AdditionalsId"));
                                items.setImage(keyObj.getString("Images"));

                                JSONObject priceObj = jsonObject.getJSONObject("Value");
                                if (priceObj.has("1")) {
                                    items.setPrice1(priceObj.getString("1"));
                                } else {
                                    items.setPrice1("-1");
                                }

                                Constants.BBQMenu.add(items);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                menuItemsAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        cart_count.setText("" + myDbHelper.getTotalOrderQty());
        footerQty.setText("" + myDbHelper.getTotalOrderQty());
        footerAmount.setText(""+ decimalFormat.format(myDbHelper.getTotalOrderPrice()));

        menuItemsAdapter = new BBQMenuAdapter(BBQatHomeMenu.this, Constants.BBQMenu, language);
        gridView.setAdapter(menuItemsAdapter);
        menuItemsAdapter.notifyDataSetChanged();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }

    @Override
    public void onBackPressed() {
//        if(myDbHelper.getTotalOrderQty() > 0) {
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHomeMenu.this);
//            // ...Irrelevant code for customizing the buttons and title
//            LayoutInflater inflater = getLayoutInflater();
//            int layout = R.layout.alert_dialog;
//            View dialogView = inflater.inflate(layout, null);
//            dialogBuilder.setView(dialogView);
//            dialogBuilder.setCancelable(false);
//
//            TextView title = (TextView) dialogView.findViewById(R.id.title);
//            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
//            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
//            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
//            View vert = (View) dialogView.findViewById(R.id.vert_line);
//
//            if(language.equalsIgnoreCase("En")) {
//                title.setText(getResources().getString(R.string.dajen));
//                yes.setText(getResources().getString(R.string.yes));
//                no.setText(getResources().getString(R.string.no));
//                desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
//            }
//            else{
//                title.setText(getResources().getString(R.string.dajen_ar));
//                yes.setText(getResources().getString(R.string.yes_ar));
//                no.setText(getResources().getString(R.string.no_ar));
//                desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
//            }
//
//            yes.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    customDialog.dismiss();
//                    myDbHelper.deleteOrderTable();
//                    startActivity(new Intent(BBQatHomeMenu.this, MenuType.class));
//                    finish();
//                }
//            });
//
//            no.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    customDialog.dismiss();
//                }
//            });
//
//            customDialog = dialogBuilder.create();
//            customDialog.show();
//            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//            Window window = customDialog.getWindow();
//            lp.copyFrom(window.getAttributes());
//            //This makes the dialog take up the full width
//            Display display = getWindowManager().getDefaultDisplay();
//            Point size = new Point();
//            display.getSize(size);
//            int screenWidth = size.x;
//
//            double d = screenWidth*0.85;
//            lp.width = (int) d;
//            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//            window.setAttributes(lp);
//        }
//        else{
            startActivity(new Intent(BBQatHomeMenu.this, BBQatHome.class));
            finish();
//        }
    }
}
