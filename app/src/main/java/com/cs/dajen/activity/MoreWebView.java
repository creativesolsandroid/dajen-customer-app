package com.cs.dajen.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cs.dajen.R;

/**
 * Created by CS on 06-07-2016.
 */
public class MoreWebView extends AppCompatActivity {
    private ProgressBar mProgressBar;
    Toolbar toolbar;
    ImageView back_btn;
    TextView screenTitle;
    String title;
    SharedPreferences languagePrefs;
    SharedPreferences.Editor languagePrefsEditor;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        languagePrefsEditor  = languagePrefs.edit();
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.more_webview);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.more_webview_ar);
        }

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);

        if(getIntent().getStringExtra("class").equals("menu") || getIntent().getStringExtra("class").equals("register")) {
            FooterActivity.tabBarPosition = 2;
            if(language.equalsIgnoreCase("En")){
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
            }
            else{
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
            }
        }
        else if(getIntent().getStringExtra("class").equals("main")){
            FooterActivity.tabBarPosition = 1;
            if(language.equalsIgnoreCase("En")){
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar1);
            }
            else{
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
            }

        }
        else{
            FooterActivity.tabBarPosition = 4;
            if(language.equalsIgnoreCase("En")){
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
            }
            else{
                FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar1);
            }
        }

        screenTitle = (TextView) findViewById(R.id.title);
        String info = getIntent().getExtras().getString("webview_toshow");


        try {
            title = (getIntent().getExtras().getString("title"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(title!=null && title.length()>0){
            screenTitle.setText(title);
        }

        String registerTerms = "<HTML><HEAD><style type=\"text/css\" media=\"all\">@import \"dc1.css\";</style><TITLE>My Web Page</TITLE></HEAD><BODY><p><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><u><b>PLEASE READ THE TERMS AND CONDITIONS OF USER AGREEMENT CAREFULLY BEFORE USING THIS APPLICATION.</b></u></span><br><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>INTRODUCTION</b></span><p align=\"justify\">Dear our valued guest, you are most welcome to Dajen Application, before the user uses the Dajen Application please ensure that the user has read, understood and complied with all the terms and conditions of using the Dajen Application, which is free for use . Moreover through the Dajen Application the user will be able to know more about the distinctive and new products of Dajen, as well as the user can view the Dajen menu and can order and receive your order from nearest Dajen store to you by easier and comfortable way and also the user can find the nearest store to user’s location, however these terms and conditions applied to the Dajen application, and all of its divisions, subsidiaries and operated which references these Terms and Conditions.\n" +
                "\n" +
                "<br><br>However, by downloading or installing the Dajen application, the user acknowledges and agrees to be bound by these terms and conditions.\n</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Eligibility to Use the Application</b></span><p align=\"justify\">Our esteemed customers Dajen grants user a license to use the Dajen Application, under the Terms and Conditions described, for shopping Dajen products sold on this Application. It should also be alarmed to notify that are not allowed to use this application or any of the products or services available at it for any commercial purposes, directly or indirectly, or use under the name of any third party, and any of these terms and conditions puncture lead to the immediate cancellation of all licenses granted in this paragraph without any notice and Return legally back to the user to claim for damages which the company deems appropriate.\n" +
                "\n" +
                "<br><br>Moreover,the user may view, download for reviewing purposes only, and printing pages from the application for the user's own personal use, informational and shopping purpose only; this application contains material trade mark which is owned by or licensed to Dajen Company only.  This material includes, but is not limited to, the design, layout, appearance and graphics of Dajen Company. Reproduction is extremely prohibited other than in accordance with the copyright notice, subject to the restrictions set out below and elsewhere in these terms of use.\n</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>The User Must Not</b></span><br> <p align=\"justify\">•\tRepublish material from this application (including republication on another application).\n" +
                "<br>•\tSell, rent or sub-license material from the application\n" +
                "<br>•\tDisplay any material from the application in public or social media or any other publications\n" +
                "<br>•\tReproduce, duplicate, copy or otherwise exploit material on the application for a commercial purpose\n" +
                "<br>•\tEdit or otherwise modify any material on the application\n" +
                "<br>•\tRedistribute material from this application\n" +
                "<br>•\tThe collection and use of any product listing, picture or descriptions or wise versa\n" +
                "<br><br>Any use of the application or applications' materials other than as specifically authorized herein, without the prior written permission from Dajen Company, I will be strictly prohibited and shall be immediately terminated the license granted herein. Such unauthorized use may also violate applicable laws, including without limitation, copyright and trademark laws and applicable communications regulations.\n.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Acceptance of User Term</b></span><p align=\"justify\"> The user acknowledges and agrees that the user must not use the application in any way that causes or may cause, damage to the application or impairment of the availability or accessibility of the application; or in any way which is unlawful, illegal, fraudulent or harmful or in connection with any harmful purpose of activities.</p> <b>Copyright</b></span><p align=\"justify\">All intellectual property rights, including trademarks, copyrights, regardless of whether they are registered or not, in addition to all the information and designs contained on this application, are the exclusive property and lonely for the Dajen company, for example but not limitation, phrases and graphics, including pictures, recording video and sounds, and selection and coordination, as well as all software compilations and symbols sources and major programs would also like to remind the user that all the contents of the application are also protected by copyright laws and is subject to copyright laws applicable in Kingdom of Saudi Arabia.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Deleting and Modification</b></span><p align=\"justify\"> The Dajen Company reserve the right in our sole discretion, without any obligation and without any notice requirement to the user, to edit or delete any documents, information or other content appearing on the application including this agreement.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Indemnification</b></span><p align=\"justify\">The user agrees to indemnify, defend and hold our officers, our shareholders, our partners, attorneys and employees harmless from any and all liability, loss, damages, claim and expenses including reasonable attorney’s fee, related to the user violation of this agreement or use of the application.</p>   <br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Disclaimer</b></span><p align=\"justify\">The user acknowledges that all the contents, services, free product samples and offers from or listed through the application are provided as available, and all warranties, express or implied are disclaimed including but not limited to the disclaimer of any implied warranties of title, non-infringement, merchantability, quality and fitness for a particular purpose, with respect to this application and any applications which is being linked within.  The information and services may contain bugs, errors, problems or other limitations.  The Dajen has no liability whatsoever for the use of any information or services, in particular, but not as a limitation.  The Dajen not liable for any indirect, incidental or consequential damages (including damages for loss of business, loss of profits, loss of money, litigation, or the like), whether based on breach of contract, breach of warranty, negligence, product liability or otherwise, even if advised of the possibility of such damages.  The negation of damages set forth above is fundamental elements of the basis of the bargain between Dajen and the user. This application and the information would not be provided without such limitation, any advice or information, whether oral or written, obtained by the user from Dajen through the application shall create any warranty, representation or guarantee not expressly stated in this agreement.  The information and all other materials on the application are provided for general information purposes only and do not constitute professional advice; it is the user responsibility to evaluate the accuracy and completeness of all information available on this application or any applications which is being linked within.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Limit</b></span><p align=\"justify\">All responsibility or liability for any damages caused by virus contained within the electronic file containing the form or document is disclaimed. Dajen will not be liable to the user for any incidental, special or consequential damages or any kind that may result from use of or inability to use the application.</p>   <br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Privacy</b></span><p align=\"justify\">Please do read the Privacy Policy carefully in order to understand how Dajen collects, uses and discloses personally identifiable information from its users.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Location Services</b></span><p align=\"justify\">Location service is served by the third party and it is not in our jurisdiction and not controlled by Dajen.  Sometimes due to unavailability of the service by the provider, the Dajen or the application might not work or function under these circumstances and Dajen is not responsible for the said issue.</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Terms of Sale</b></span><p align=\"justify\">The user acknowledged and agreed that all the sales from the applications are being governed by Dajen terms of sale.  Please do refer below to Dajen terms of sale for the terms, conditions and policies applicable to the user purchase of products and materials. By ordering products through the applications, the user agrees to be bound by and accepted the terms of sale.  The terms of sale are subject to change without prior notice at any time, in Dajen sole discretion; so that the user should review the terms of sale each time the user makes a purchase.\n" +
                "\n" +
                "<br><br>\uF0A7•\tThis application is designed to serve Dajen products to its customers through:\n" +
                "<br>1.\tDine-In\n" +
                "<br>2.\tCarry Out\n" +
                "<br>3.\tHome Delivery\n" +
                "<br>4.\tCatering \n" +
                "<br>5.\tEvents\n" +
                "\uF0A7<br>•\tThe order will be processed and delivered through cloud system.\n" +
                "\uF0A7<br>•\tThe order will not be refundable and no money exchange.\n" +
                "\uF0A7<br>•\tIf the user does not comes to take the order from the Dajen Store, the user account will be block / frozen until the user comes and pays the due amount.\n" +
                "\uF0A7<br>•\tNo delivery during Salah/prayer time\n" +
                "\uF0A7<br>•\tThe order may or may not be available as per your order and it’s subject to availability of the products.  \n" +
                "\uF0A7<br>•\tIn a few cases, may not be the adoption and approval of the request for several reasons, the company has the right to keep those orders which resulted in the rejection or cancellation for any reason and at any time.\n" +
                "\uF0A7<br>•\tThe Dajen is determined to provide the most accurate pricing information on the application to our users; however, errors may still occur, such as cases when the price of an item is not displayed correctly on the application. As such, we reserve the right to refuse or cancel any order.\n" +
                "\uF0A7<br>•\tPayment will be done at Dajen Store itself In Case of Dine-In, Carry-out, Catering and Events. However in Home Delivery the customer has to pay to the Dajen Delivery person.\n</p><br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Submissions</b></span><p align=\"justify\"> All suggestions, ideas, notes, concepts and other information the user may send to Dajen (collectively, \"submissions\") shall be deemed and shall remain our Dajen sole property and shall not be subject to any obligation of confidence on Dajen.  Without limiting the foregoing, the Dajen shall be deemed to own all known and hereafter existing right of every kind and nature regarding the submissions and shall be entitled to unrestricted use of the submissions for any purpose, without compensation to the provider of the submissions.  This information is for internal purposes only and is not sold or otherwise transferred to third parties of Dajen or to other entities who are not involved in the operation of this application.  Information submitted via a number of areas in this application, therefore, the above right to use.  Submissions are subject to this limited use of this information and exclude non-retained information.  The user acknowledges that the user is responsible for whatever material the user submits. The Dajen has the fullest responsibility for the message including its legality, reliability, appropriateness, originality and copyright.</p>    <br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Accounts</b></span><p align=\"justify\"> The user may be required to create an account and specify a password to use certain features on the applications.  The user agrees to provide, maintain and update true, accurate, current and complete information about the user-self as prompted by the registration processes.  The user may not impersonate any person or individual or entity or misrepresent the identity or affiliation with any person or entity, including using another person's username, password or other account information.\n" +
                "<br><br>The user is entirely responsible for maintaining the confidentiality of the password and the personal account and all activities made by the user or anyone that uses the user account.  The user agrees to safeguard the password from access by others.  Should the user believe that the account has been compromised; the user must immediately contact Dajen by email accordingly.  The user agrees to indemnify and hold harmless Dajen for losses incurred by Dajen or another party due to someone else suing the account as a result of user's failure to use reasonable care to safeguard the password.\n</p>   <br><span  style=\"font-family:arial;color:#000000;font-size:15px;\"><b>Jurisdiction</b></span><p align=\"justify\">The application is controlled and operated in Saudi Arabia. Any terms and conditions concerning the usage of this application will be governed by the Saudi Arabian Law and Order and any dispute concerning use of this application will be determined exclusively by Saudi Arabian Courts at Riyadh City.</p><br></BODY></HTML>";



        back_btn = (ImageView) findViewById(R.id.back_btn);
        WebView wv = (WebView) findViewById(R.id.webView);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
        mProgressBar.setVisibility(View.VISIBLE);

        if (info.equalsIgnoreCase("register_terms")) {
            screenTitle.setText("Terms and Conditions");
            wv.loadDataWithBaseURL("", registerTerms, "image/html", "UTF-8", "");
        } else if (info.equalsIgnoreCase("card_terms")) {
            screenTitle.setText("Terms and Conditions");
//            wv.loadDataWithBaseURL("", cardTerms, "image/html", "UTF-8", "");
        } else  {
            wv.loadUrl(getIntent().getExtras().getString("url"));
//            screenTitle.setText("Disclaimer");
//            wv.loadDataWithBaseURL("", disclaimer, "image/html", "UTF-8", "");
        }

        wv.setWebViewClient(new MyWebViewClient());
        wv.getSettings().setLoadsImagesAutomatically(true);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mProgressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }
}
