package com.cs.dajen.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Constants;
import com.cs.dajen.JSONParser;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 14-07-2016.
 */
public class ForgotPasswordActivity extends AppCompatActivity {

    Toolbar toolbar;
    private String response = null;
    TextView mCancel;
    LinearLayout mSend;
    EditText mEmail;
    SharedPreferences languagePrefs;
    String language;
    AlertDialog customDialog;
    ImageView back_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.forgot_pwd);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.forgot_pwd_ar);
        }

        mSend = (LinearLayout) findViewById(R.id.send_button);
        mEmail = (EditText) findViewById(R.id.mobile_number);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString();
                if(email.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        mEmail.setError(getResources().getString(R.string.pls_enter_mobile_num));
                    }
                    else{
                        mEmail.setError(getResources().getString(R.string.pls_enter_mobile_num_ar));
                    }
                }else{
                    new GetVerificationDetails().execute(Constants.FORGOT_PASSWORD_URL+"966"+email);
                }
            }
        });
    }

    public class GetVerificationDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(ForgotPasswordActivity.this);
            dialog = new MaterialDialog.Builder(ForgotPasswordActivity.this)
                    .title(R.string.app_name)
                    .content("Sending OTP...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(ForgotPasswordActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {

                            JSONObject jo = new JSONObject(result);
                            try {
                                JSONObject jo1 = jo.getJSONObject("Success");
                                String otp = jo1.getString("OTP");
                                String mobile = jo1.getString("MobileNo");
                                dialog.dismiss();
                                Intent loginIntent = new Intent(ForgotPasswordActivity.this, VerifyOTP.class);
                                loginIntent.putExtra("phone_number", mobile);
                                loginIntent.putExtra("OTP", otp);
                                loginIntent.putExtra("forgot", true);
                                startActivity(loginIntent);
                            }catch (JSONException je){

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ForgotPasswordActivity.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);
                                yes.setText(getResources().getString(R.string.ok));
                                desc.setText(getResources().getString(R.string.mobile_not_exist));

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(ForgotPasswordActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

}
