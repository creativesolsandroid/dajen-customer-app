package com.cs.dajen.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Constants;
import com.cs.dajen.JSONParser;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by CS on 23-08-2016.
 */
public class LiveTrackingActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

    SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
    SimpleDateFormat timeFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
    SimpleDateFormat timeFormat2 = new SimpleDateFormat("hh:mm a", Locale.US);
    SimpleDateFormat timeFormat3 = new SimpleDateFormat("hh:mma", Locale.US);

    Toolbar toolbar;
    TextView driverNameTxt, callNow, expTime, deliveryTime;
    String storePhone, driverPhone;
    Double storeLat, storeLong, userLat, userLong, driverLat, driverLong;
    String driverName, driverNumber, driverId, orderId, expectedTime;

    private String timeResponse = null;

    private static final String[] PHONE_PERMS = {
            android.Manifest.permission.CALL_PHONE
    };
    private static final int PHONE_REQUEST = 3;

    String URL_DISTANCE = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    private Timer timer = new Timer();

    SharedPreferences languagePrefs;
    String language;
    AlertDialog customDialog;
    ImageView live_cancel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")){
            setContentView(R.layout.live_tracking);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.live_tracking_ar);
        }

        driverName = getIntent().getStringExtra("driver_name");
        driverNumber = getIntent().getStringExtra("driver_number");
        driverId = getIntent().getStringExtra("driver_id");
        orderId = getIntent().getStringExtra("order_id");
        expectedTime = getIntent().getStringExtra("exp_time");

        callNow = (TextView) findViewById(R.id.call_now);
        deliveryTime = (TextView) findViewById(R.id.delivery_time);
        expTime = (TextView) findViewById(R.id.expected_time);
        driverNameTxt = (TextView) findViewById(R.id.driver_name);
        live_cancel = (ImageView) findViewById(R.id.live_cancel);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(LiveTrackingActivity.this);
        driverNameTxt.setText(driverName);

//        if(language.equalsIgnoreCase("En")){
//            expTime.setText("Arrival Time : "+expectedTime);
//        }else if(language.equalsIgnoreCase("Ar")){
//            expTime.setText("الوقت المتوقع  : "+expectedTime);
//        }

        callNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= Build.VERSION_CODES.M) {
                    if (!canAccessPhonecalls()) {
                        requestPermissions(PHONE_PERMS, PHONE_REQUEST);
                    } else {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" +"+" +driverNumber));
                        startActivity(intent);
                    }
                }else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"  +"+" +driverNumber));
                    startActivity(intent);
                }
            }
        });

        timer.schedule(new MyTimerTask(), 60000, 60000);

        live_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }



    private class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new getTrackingDetails().execute(Constants.LIVE_TRACKING_URL + orderId);
                }
            });
        }
    }

    private boolean canAccessPhonecalls() {
        return (hasPermission(android.Manifest.permission.CALL_PHONE));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(LiveTrackingActivity.this, perm));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {


            case PHONE_REQUEST:
                if (canAccessPhonecalls()) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"  +"+" +driverNumber));
                    startActivity(intent);
                }
                else {
                    Toast.makeText(LiveTrackingActivity.this, "Call phone permission denied, Unable to make call", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        new getTrackingDetails().execute(Constants.LIVE_TRACKING_URL + orderId);
//        LatLng driver = new LatLng(driverLat, driverLong);
//        LatLng store = new LatLng(storeLat, storeLong);
//        LatLng user = new LatLng(userLat, userLong);
//
//        mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)));
//        mMap.addMarker(new MarkerOptions().position(store).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.store_marker)));
//        mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
    }

    public class getTrackingDetails extends AsyncTask<String, Integer, String> {
        MaterialDialog dialog;
        String networkStatus;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);
            dialog = new MaterialDialog.Builder(LiveTrackingActivity.this)
                    .title(R.string.app_name)
                    .content("Please Wait...")
                    .progress(true, 0)
                    .cancelable(false)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();
                response = jParser.getJSONFromUrl(params[0]);
                Log.i("TAG", "user response: " + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {

                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LiveTrackingActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    no.setVisibility(View.GONE);
                    vert.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.connection_error));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.connection_error_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth * 0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);

                } else {
                    try {
                        JSONObject jo = new JSONObject(result);

                        try {
                            JSONArray ja = jo.getJSONArray("Success");
                            for (int i = 0; i < ja.length(); i++) {
                                JSONObject jo1 = ja.getJSONObject(i);
                                storeLat = jo1.getDouble("sLatitude");
                                storeLong = jo1.getDouble("sLongitude");
                                userLat = jo1.getDouble("uLatitude");
                                userLong = jo1.getDouble("uLongitude");
                                driverLat = jo1.getDouble("dLatitude");
                                driverLong = jo1.getDouble("dLongitude");
                                storePhone = jo1.getString("sPhone");
                                driverPhone = jo1.getString("dPhone");
                                if(mMap != null){
                                    LatLng driver = new LatLng(driverLat, driverLong);
                                    LatLng store = new LatLng(storeLat, storeLong);
                                    LatLng user = new LatLng(userLat, userLong);

                                    mMap.clear();

                                    mMap.addMarker(new MarkerOptions().position(driver).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.direction_marker)));
                                    mMap.addMarker(new MarkerOptions().position(store).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker)));
                                    mMap.addMarker(new MarkerOptions().position(user).title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.store_marker)));
                                    mMap.moveCamera(CameraUpdateFactory.newLatLng(driver));
                                    mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
                                }

                            }

                        } catch (JSONException je) {
                            je.printStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            if(dialog != null) {
                dialog.dismiss();
            }

            new GetCurrentTime().execute();
            super.onPostExecute(result);

        }

    }


    public class GetCurrentTime extends AsyncTask<String, String, String> {
        java.net.URL url = null;
        String cardNumber = null, password = null;
        double lat, longi;
        String networkStatus;
        String serverTime;
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.US);
        SimpleDateFormat timeFormat1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);
            dialog = new MaterialDialog.Builder(LiveTrackingActivity.this)
                    .title(R.string.app_name)
                    .content("Loading. Please Wait....")
                    .progress(true, 0)
                    .cancelable(false)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... arg0) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
//                    Calendar c = Calendar.getInstance();
//                    System.out.println("Current time => "+c.getTime());

//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    timeResponse = timeFormat.format(c.getTime());
                    JSONParser jParser = new JSONParser();
                    serverTime = jParser.getJSONFromUrl(Constants.GET_CURRENT_TIME_URL);


                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("Responce", "" + serverTime);
            }else{
                serverTime = "no internet";
            }
            return serverTime;
        }

        @Override
        protected void onPostExecute(String result1) {
            if(serverTime == null){
                dialog.dismiss();
            } else if(serverTime.equals("no internet")){
                dialog.dismiss();
                if (language.equalsIgnoreCase("En")) {
                    Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                }
            } else {
                dialog.dismiss();
                try {
                    JSONObject jo = new JSONObject(result1);
                    timeResponse = jo.getString("DateTime");
                }catch (JSONException je){
                    je.printStackTrace();
                }
                new getTrafficTime().execute();

            }
            super.onPostExecute(result1);
        }
    }


    public class getTrafficTime extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        String distanceResponse;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(LiveTrackingActivity.this);

        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                distanceResponse = jParser
                        .getJSONFromUrl(URL_DISTANCE + userLat +","+ userLong +"&destinations="+ driverLat +","+ driverLong+"&departure_time=now&duration_in_traffic=true&mode=driving&language=en-EN&mode=driving&key=AIzaSyDEgxGmjLVPAJJYWM3f9G3JuZRPbL5OYPM");
                Log.i("TAG", "user response: " + distanceResponse);
                return distanceResponse;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    try {
                        JSONObject jo = new JSONObject(result);
                        JSONArray ja = jo.getJSONArray("rows");
                        JSONObject jo1 = ja.getJSONObject(0);
                        JSONArray ja1 = jo1.getJSONArray("elements");
                        JSONObject jo2 = ja1.getJSONObject(0);
                        JSONObject jo3 = jo2.getJSONObject("duration_in_traffic");
                        String secs = jo3.getString("text");
                        String value = jo3.getString("value");
//                        if(language.equalsIgnoreCase("En")) {
                        if(language.equalsIgnoreCase("En")){
                            deliveryTime.setText("Enjoy your food in : "+secs);
                        }else if(language.equalsIgnoreCase("Ar")){
                            deliveryTime.setText("استمتع بوجبتك في : "+secs);
                        }

                        Date current24Date = null, currentServerDate = null;
                        Date expectedTimeDate = null, expectedTime24 = null;
                        try {
                            current24Date = timeFormat.parse(timeResponse);
                            expectedTime24 = timeFormat2.parse(expectedTime);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        String currentTime =timeFormat1.format(current24Date);
                        String expTimeStr = timeFormat1.format(expectedTime24);
                        try {
                            currentServerDate = timeFormat1.parse(currentTime);
                            expectedTimeDate = timeFormat1.parse(expTimeStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        long diff = expectedTimeDate.getTime() - currentServerDate.getTime();

                        long diffSeconds = diff / 1000 % 60;
                        long diffMinutes = diff / (60 * 1000) % 60;
                        long diffHours = diff / (60 * 60 * 1000) % 24;
                        int expMins = (int) diffMinutes * 60 * 1000;
                        Log.i("TAG", "mins response: " + expMins);
                        int mins = (Integer.parseInt(value)/60)+1;
                        Calendar now = Calendar.getInstance();
                        now.setTime(currentServerDate);
                        now.add(Calendar.MINUTE, mins);
                        currentServerDate = now.getTime();
                        String CTimeString = timeFormat2.format(currentServerDate);
                        if(language.equalsIgnoreCase("En")){
                            expTime.setText("Arrival Time : "+CTimeString);
                        }else if(language.equalsIgnoreCase("Ar")){
                            expTime.setText("الوقت المتوقع  : "+CTimeString);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            super.onPostExecute(result);

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
        new getTrackingDetails().cancel(true);
    }
}
