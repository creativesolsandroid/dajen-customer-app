package com.cs.dajen.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cs.dajen.Adapters.OrderHistoryAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.OrderHistory;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by CS on 13-02-2017.
 */

public class OrderHistoryActivity extends AppCompatActivity {
    private DataBaseHelper myDbHelper;
    private ArrayList<OrderHistory> transactionsList = new ArrayList<>();
    public static SwipeMenuListView orderHistoryListView;
    TextView emptyView;
    private OrderHistoryAdapter mAdapter;
    String userId;
    TextView title;
    Toolbar toolbar;
    String orderStatus = null;
    ImageView back_btn;
    AlertDialog customDialog;
    SharedPreferences languagePrefs;
    String language;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if (language.equalsIgnoreCase("En")) {
            setContentView(R.layout.order_history);
        } else if (language.equalsIgnoreCase("Ar")) {
            setContentView(R.layout.order_history_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor = userPrefs.edit();

        myDbHelper = new DataBaseHelper(OrderHistoryActivity.this);

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;
        if (language.equalsIgnoreCase("En")) {
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        } else {
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }
        Constants.CurrentOrderActivity = "history";

        userId = userPrefs.getString("userId", null);

        orderHistoryListView = (SwipeMenuListView) findViewById(R.id.fav_order_listview);
        emptyView = (TextView) findViewById(R.id.empty_view);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        mAdapter = new OrderHistoryAdapter(OrderHistoryActivity.this, transactionsList, language);
        orderHistoryListView.setAdapter(mAdapter);

        if (userId != null) {
            new GetOrderDetails().execute(Constants.HISTORY_URL + userId);
        }

        orderHistoryListView.setEmptyView(emptyView);

//        if(language.equalsIgnoreCase("En")){
//            title.setText("Order History");
//        }else if(language.equalsIgnoreCase("Ar")){
//            title.setText("جميع الطلبات");
//            emptyView.setText("لا يوجد منتجات في السلة");
//        }

        orderHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Constants.TRACK_ID = transactionsList.get(position).getOrderId();
                Intent intent = new Intent(OrderHistoryActivity.this, TrackOrder.class);
                intent.putExtra("orderId", transactionsList.get(position).getOrderId());
                intent.putExtra("screen", "history");
                startActivity(intent);
            }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OrderHistoryActivity.this, OrderActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        // step 1. create a MenuCreator
        SwipeMenuCreator creator2 = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                switch (menu.getViewType()) {
                    case 0:
                        // create menu of type 0
                        SwipeMenuItem viewItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        viewItem.setBackground(new ColorDrawable(Color.rgb(0x80,
                                0x80, 0x88)));
                        // set item width
                        viewItem.setWidth(dp2px(90));

                        if (language.equalsIgnoreCase("En")) {
                            viewItem.setTitle("View");
                        } else if (language.equalsIgnoreCase("Ar")) {
                            viewItem.setTitle("مراجعة");
                        }
                        // set item title fontsize
                        viewItem.setTitleSize(18);
                        // set item title font color
                        viewItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(viewItem);

                        // create "delete" item
                        SwipeMenuItem reorderItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        reorderItem.setBackground(new ColorDrawable(Color.rgb(0x25,
                                0xAE, 0x88)));
                        // set item width
                        reorderItem.setWidth(dp2px(90));

                        if (language.equalsIgnoreCase("En")) {
                            reorderItem.setTitle("Re-order");
                        } else if (language.equalsIgnoreCase("Ar")) {
                            reorderItem.setTitle("إعادة الطلب");
                        }
                        // set item title fontsize
                        reorderItem.setTitleSize(18);
                        // set item title font color
                        reorderItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(reorderItem);

                        // create "delete" item
                        SwipeMenuItem deleteItem = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                                0x3F, 0x25)));
                        // set item width
                        deleteItem.setWidth(dp2px(90));

                        if (language.equalsIgnoreCase("En")) {
                            deleteItem.setTitle("Delete");
                        } else if (language.equalsIgnoreCase("Ar")) {
                            deleteItem.setTitle("حذف");
                        }
                        // set item title fontsize
                        deleteItem.setTitleSize(18);
                        // set item title font color
                        deleteItem.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(deleteItem);
                        break;
                    case 1:
                        // create menu of type 1
                        SwipeMenuItem viewItem1 = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        viewItem1.setBackground(new ColorDrawable(Color.rgb(0x80,
                                0x80, 0x88)));
                        // set item width
                        viewItem1.setWidth(dp2px(90));

                        if (language.equalsIgnoreCase("En")) {
                            viewItem1.setTitle("View");
                        } else if (language.equalsIgnoreCase("Ar")) {
                            viewItem1.setTitle("مراجعة");
                        }
                        // set item title fontsize
                        viewItem1.setTitleSize(18);
                        // set item title font color
                        viewItem1.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(viewItem1);

                        // create "delete" item
                        SwipeMenuItem reorderItem1 = new SwipeMenuItem(
                                getApplicationContext());
                        // set item background
                        reorderItem1.setBackground(new ColorDrawable(Color.rgb(0x25,
                                0xAE, 0x88)));
                        // set item width
                        reorderItem1.setWidth(dp2px(90));

                        if (language.equalsIgnoreCase("En")) {
                            reorderItem1.setTitle("Re-order");
                        } else if (language.equalsIgnoreCase("Ar")) {
                            reorderItem1.setTitle("إعادة الطلب");
                        }
                        // set item title fontsize
                        reorderItem1.setTitleSize(18);
                        // set item title font color
                        reorderItem1.setTitleColor(Color.WHITE);
                        // set a icon
//                deleteItem.setIcon(R.drawable.ic_delete);
                        // add to menu
                        menu.addMenuItem(reorderItem1);
                        break;
                }
            }

        };
        orderHistoryListView.setMenuCreator(creator2);

        // step 2. listener item click event
        orderHistoryListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {

                    case 0:
                        String orderId1 = transactionsList.get(position).getOrderId();
                        Intent intent = new Intent(OrderHistoryActivity.this, ViewDetailsActivity.class);
                        intent.putExtra("orderid", orderId1);
                        startActivity(intent);
                        break;
                    case 1:
                        String orderId = transactionsList.get(position).getOrderId();
                        Log.i("TAG","order id" +orderId);
                        Log.i("TAG","user id" +userId);
                        new GetCheckoutOrderDetails().execute(Constants.ORDERD_DETAILS_URL + userId + "&oId=" + orderId);
                        break;
                    case 2:
                        if (transactionsList.get(position).getOrderStatus().equals("Cancel") || transactionsList.get(position).getOrderStatus().equals("Close")) {
                            new DeleteFavOrder().execute(Constants.DELETE_ORDER_FROM_HISTORY + transactionsList.get(position).getOrderId());
                        } else {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderHistoryActivity.this);
                            // ...Irrelevant code for customizing the buttons and title
                            LayoutInflater inflater = getLayoutInflater();
                            int layout = R.layout.alert_dialog;
                            View dialogView = inflater.inflate(layout, null);
                            dialogBuilder.setView(dialogView);
                            dialogBuilder.setCancelable(false);

                            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                            View vert = (View) dialogView.findViewById(R.id.vert_line);

                            no.setVisibility(View.GONE);
                            vert.setVisibility(View.GONE);
                            yes.setText(getResources().getString(R.string.ok));
                            desc.setText("This order is still pending!!");

                            yes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    customDialog.dismiss();
                                }
                            });

                            customDialog = dialogBuilder.create();
                            customDialog.show();
                            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                            Window window = customDialog.getWindow();
                            lp.copyFrom(window.getAttributes());
                            //This makes the dialog take up the full width
                            Display display = getWindowManager().getDefaultDisplay();
                            Point size = new Point();
                            display.getSize(size);
                            int screenWidth = size.x;

                            double d = screenWidth * 0.85;
                            lp.width = (int) d;
                            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                            window.setAttributes(lp);
                        }
                        break;
                }
                return false;
            }
        });
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }

    public class DeleteFavOrder extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        String response;
        InputStream inputStream = null;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistoryActivity.this);
            dialog = new MaterialDialog.Builder(OrderHistoryActivity.this)
                    .title(R.string.app_name)
                    .content("Deleting Order...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(params[0]);


                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity("", "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if (inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "delete order response:" + response);
                return response;
            } else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            Log.i("TAG", "delete order response1:" + result);
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

//                        try {
//                            JSONObject jo= new JSONObject(result);
//                            String s = jo.getString("Success");
                        new GetOrderDetails().execute(Constants.HISTORY_URL + userId);
//                            Toast.makeText(FavoriteOrdersActivity.this, "Order deleted successfully", Toast.LENGTH_SHORT).show();

//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                            Toast.makeText(FavoriteOrdersActivity.this, "Can not reach server", Toast.LENGTH_SHORT).show();
//                        }

                    }
                }

            } else {
                Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    public class GetCheckoutOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        String response;
        String menuType = "";

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistoryActivity.this);
            dialog = new MaterialDialog.Builder(OrderHistoryActivity.this)
                    .title(R.string.app_name)
                    .content("Loading Items...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            HashMap<String, String> values = new HashMap<>();

                            JSONObject jo = new JSONObject(result);

                            JSONArray jaa = jo.getJSONArray("MainItem");
                            JSONObject joo = jaa.getJSONObject(0);
                            menuType = joo.getString("MenuType");

                            JSONArray ja = jo.getJSONArray("SubItem");
                            myDbHelper.deleteOrderTable();
                            for (int i = 0; i < ja.length(); i++) {
                                String ids = "0", additionalsStr = "", additionalsStrAr = "", additionalsPrice = "", additionalQty = "", itemTypeName = "";
                                String categoryId = "", itemId = "", itemName = "", itemNameAr = "", itemImage = "", itemDesc = "", itemDescAr = "", itemType = "";
                                String price = "";
                                float additionPrice = 0;
                                float priceAd = 0, priceAd1 = 0, finalPrice = 0;
                                int quantity = 0;
                                JSONArray ja1 = ja.getJSONArray(i);
                                for (int j = 0; j < ja1.length(); j++) {
                                    if (j == 0) {
                                        JSONObject jo1 = ja1.getJSONObject(0);
                                        itemId = jo1.getString("ItemId");
                                        itemName = jo1.getString("ItemName");
                                        itemNameAr = jo1.getString("ItemName_Ar");
                                        itemImage = jo1.getString("Images");
                                        itemDesc = jo1.getString("Description");
                                        itemDescAr = jo1.getString("Description_Ar");
                                        itemType = jo1.getString("Size");
                                        quantity = jo1.getInt("Qty");
                                        price = jo1.getString("ItemPrice");
                                        categoryId = jo1.getString("CategoryId");
                                    } else {
                                        JSONArray ja2 = ja1.getJSONArray(j);
                                        for (int k = 0; k < ja2.length(); k++) {
                                            JSONObject jo2 = ja2.getJSONObject(k);
                                            if (!jo2.getString("AdditionalID").equals("0")) {
                                                if (ids.equalsIgnoreCase("0")) {
                                                    ids = jo2.getString("AdditionalID");
                                                    additionalsStr = jo2.getString("AdditionalName");
                                                    additionalsStrAr = jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = jo2.getString("AdditionalPrice");
                                                    additionalQty = jo2.getString("AddQty");
                                                } else {
                                                    ids = ids + "," + jo2.getString("AdditionalID");
                                                    additionalsStr = additionalsStr + "," + jo2.getString("AdditionalName");
                                                    additionalsStrAr = additionalsStrAr + "," + jo2.getString("AdditionalName_Ar");
                                                    additionalsPrice = additionalsPrice + "," + jo2.getString("AdditionalPrice");
                                                    additionalQty = additionalQty + "," + jo2.getString("AddQty");
                                                }
                                                additionPrice = additionPrice + ((Float.parseFloat((jo2.getString("AdditionalPrice"))) * Integer.parseInt(jo2.getString("AddQty"))));
                                            }

                                        }

                                        priceAd = Float.parseFloat(price);
                                        priceAd1 = Float.parseFloat(price) + additionPrice;
                                    }
                                }

                                if (priceAd != 0) {
                                    finalPrice = priceAd1 * quantity;
                                } else {
                                    finalPrice = Float.parseFloat(price) * quantity;
                                }

                                if (itemType.equals("1")) {
                                    itemTypeName = "REGULAR";
                                } else if (itemType.equals("2")) {
                                    itemTypeName = "SPICY";
                                } else if (itemType.equals("3")) {
                                    itemTypeName = "JUNIOR";
                                } else if (itemType.equals("4")) {
                                    itemTypeName = "REGULAR";
                                } else if (itemType.equals("5")) {
                                    itemTypeName = "SMALL";
                                } else if (itemType.equals("6")) {
                                    itemTypeName = "MEDIUM";
                                } else if (itemType.equals("7")) {
                                    itemTypeName = "LARGE";
                                }

                                values.put("CategoryId", categoryId);
                                values.put("ItemId", itemId);
                                values.put("ItemName", itemName);
                                values.put("ItemName_Ar", itemNameAr);
                                values.put("ItemDescription_En", itemDesc);
                                values.put("ItemDescription_Ar", itemDescAr);
                                values.put("ItemImage", itemImage);
                                values.put("ItemTypeId", itemType);
                                values.put("TypeName", itemTypeName);
                                values.put("Price", Float.toString(priceAd));
                                values.put("ModifierId", "0");
                                values.put("ModifierName", "0");
                                values.put("ModifierName_Ar", "0");
                                values.put("MdfrImage", "0");
                                values.put("AdditionalID", ids);
                                values.put("AdditionalName", additionalsStr);
                                values.put("AdditionalName_Ar", additionalsStrAr);
                                values.put("AddlImage", "0");
                                values.put("AddlTypeId", "0");
                                values.put("AdditionalPrice", additionalsPrice);
                                values.put("Qty", Integer.toString(quantity));
                                values.put("TotalAmount", Float.toString(finalPrice));
                                values.put("Comment", "");
                                values.put("Custom", "0");
                                values.put("SinglePrice", additionalQty);
                                myDbHelper.insertOrder(values);

                                if (menuType.equals("1")) {
                                    Constants.MENU_TYPE = "main";
                                    userPrefEditor.putString("menu", "main");
                                    userPrefEditor.commit();
                                } else if (menuType.equals("2")) {
                                    Constants.MENU_TYPE = "bbq";
                                    userPrefEditor.putString("menu", "bbq");
                                    userPrefEditor.commit();
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }
            Intent intent = new Intent(OrderHistoryActivity.this, CheckoutActivity.class);
            startActivity(intent);

            super.onPostExecute(result);

        }

    }

    public class GetOrderDetails extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String networkStatus;
        MaterialDialog dialog;
        String response;

        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(OrderHistoryActivity.this);
            dialog = new MaterialDialog.Builder(OrderHistoryActivity.this)
                    .title(R.string.app_name)
                    .content("Loading History...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if (!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "user response:" + response);
                return response;
            } else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {
            transactionsList.clear();
            if (result != null) {
                if (result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (result.equals("")) {
                        Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {

                        try {
                            JSONObject jo = new JSONObject(result);

                            try {
                                JSONArray ja = jo.getJSONArray("Success");
                                for (int i = 0; i < ja.length(); i++) {

                                    OrderHistory oh = new OrderHistory();
                                    JSONObject jo1 = ja.getJSONObject(i);
                                    String orderId = jo1.getString("odrId");

                                    String storeName = jo1.getString("Sname");
                                    String storeName_ar = jo1.getString("Sname_ar");
                                    String userAddress = jo1.getString("UserAddress");
                                    String orderDate = jo1.getString("Odate");
//                                    boolean isFavourite = jo1.getBoolean("IsFavorite");
                                    String total_Price = jo1.getString("TotPrice");
                                    String orderType = jo1.getString("OrderType");
                                    orderStatus = jo1.getString("OrderStatus");

                                    String invoiceNo = jo1.getString("InvoiceNo");
                                    String itemDetails = "", itemDetailsAr = "";

                                    JSONArray ja1 = jo1.getJSONArray("items");
                                    for (int j = 0; j < ja1.length(); j++) {
                                        JSONObject jo2 = ja1.getJSONObject(j);
                                        if (itemDetails.equalsIgnoreCase("") && itemDetailsAr.equalsIgnoreCase("")) {
                                            itemDetails = jo2.getString("ItmName");
                                            itemDetailsAr = jo2.getString("ItmName_ar");
                                        } else {
                                            itemDetails = itemDetails + ", " + jo2.getString("ItmName");
                                            itemDetailsAr = itemDetailsAr + ", " + jo2.getString("ItmName_ar");
                                        }
                                    }


                                    oh.setOrderId(orderId);
                                    oh.setUserAddress(userAddress);
                                    oh.setOrderDate(orderDate);
                                    oh.setOrderId(orderId);

                                    oh.setStoreName(storeName);
                                    oh.setStoreName_ar(storeName_ar);
//                                    oh.setIsFavorite(isFavourite);
                                    oh.setTotalPrice(total_Price);
//                                    oh.setStatus(status);
                                    oh.setOrderStatus(orderStatus);
                                    oh.setInvoiceNo(invoiceNo);
                                    oh.setItemDetails(itemDetails);
                                    oh.setItemDetails_ar(itemDetailsAr);
                                    oh.setOrderType(orderType);
//                                txn.setTransactionDate(transactionDate);
//                                txn.setTotalBalance(checkTotal);

                                    transactionsList.add(oh);

                                }
                            } catch (JSONException je) {
                                je.printStackTrace();
//                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(OrderHistoryActivity.this, android.R.style.Theme_Material_Light_Dialog));
//
////                                if(language.equalsIgnoreCase("En")) {
//                                // set title
//                                alertDialogBuilder.setTitle("Oregano");
//
//                                // set dialog message
//                                alertDialogBuilder
//                                        .setMessage("No orders in your history")
//                                        .setCancelable(false)
//                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                            public void onClick(DialogInterface dialog, int id) {
//                                                dialog.dismiss();
//                                            }
//                                        });
////                                }else if(language.equalsIgnoreCase("Ar")){
////                                    // set title
////                                    alertDialogBuilder.setTitle("د. كيف");
////
////                                    // set dialog message
////                                    alertDialogBuilder
////                                            .setMessage("البريد الالكتروني أو كلمة المرور غير صحيح")
////                                            .setCancelable(false)
////                                            .setPositiveButton("تم", new DialogInterface.OnClickListener() {
////                                                public void onClick(DialogInterface dialog, int id) {
////                                                    dialog.dismiss();
////                                                }
////                                            });
////                                }
//
//
//                                // create alert dialog
//                                AlertDialog alertDialog = alertDialogBuilder.create();
//
//                                // show it
//                                alertDialog.show();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                Toast.makeText(OrderHistoryActivity.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if (dialog != null) {
                dialog.dismiss();
            }

            mAdapter.notifyDataSetChanged();
            super.onPostExecute(result);

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(OrderHistoryActivity.this, OrderActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }
}
