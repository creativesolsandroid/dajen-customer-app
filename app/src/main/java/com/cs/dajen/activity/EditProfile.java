package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;
import com.cs.dajen.SplashScreenActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by CS on 25-05-2017.
 */

public class EditProfile extends Activity {

    EditText name,mobile,email,nickName,familyName;
    String name_str,mobile_str, email_str,nickname_str,familyname_str;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    String userId;
    SharedPreferences languagePrefs;
    String language;
    String response,gender;
    ImageView gender_switch;
    boolean isSwitchOn;
    LinearLayout update;
    ImageView back_btn;
    AlertDialog customDialog;
    private DataBaseHelper myDbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");
        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.edit_profile);
        }
        else{
            setContentView(R.layout.edit_profile_ar);
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        Constants.CurrentMoreActivity = "edit";
        FooterActivity.tabBarPosition = 4;
        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar4);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar1);
        }

        name = (EditText) findViewById(R.id.register_name);
        mobile = (EditText) findViewById(R.id.mobile_number);
        email = (EditText) findViewById(R.id.email);
        nickName = (EditText) findViewById(R.id.nickname);
        familyName = (EditText) findViewById(R.id.family_name);
        update = (LinearLayout) findViewById(R.id.update_button);
        back_btn = (ImageView) findViewById(R.id.back_btn);
        gender_switch = (ImageView) findViewById(R.id.gender);

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();
        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userId = userPrefs.getString("userId", null);
        response = userPrefs.getString("user_profile",null);

        if(response != null) {
            try {
                JSONObject property = new JSONObject(response);
                JSONObject userObjuect = property.getJSONObject("profile");

                name.setText(userObjuect.getString("fullName"));
                mobile.setText("+"+userObjuect.getString("mobile"));
                email.setText(userObjuect.getString("email"));
                gender = userObjuect.getString("gender");
                mobile_str = userObjuect.getString("mobile");
                email_str = userObjuect.getString("email");
                nickName.setText(userObjuect.getString("NickName"));
                familyName.setText(userObjuect.getString("FamilyName"));

            } catch (JSONException e) {
                Log.d("TAG", "Error while parsing the results!");
                e.printStackTrace();
            }
        }

        name.setSelection(name.length());

        try {
            if(language.equalsIgnoreCase("En")) {
                if (gender.equalsIgnoreCase("Male")) {
                    gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.male));
                } else if (gender.equalsIgnoreCase("Female")) {
                    gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.female));
                }
            }
            else{
                if (gender.equalsIgnoreCase("Male")) {
                    gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.female));
                } else if (gender.equalsIgnoreCase("Female")) {
                    gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.male));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        gender_switch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(language.equalsIgnoreCase("En")) {
                    if (gender.equalsIgnoreCase("Male")) {
                        gender = "Female";
                        gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.female));
                    } else if (gender.equalsIgnoreCase("Female")) {
                        gender = "Male";
                        gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.male));
                    }
                }
                else{
                    if (gender.equalsIgnoreCase("Male")) {
                        gender = "Female";
                        gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.male));
                    } else if (gender.equalsIgnoreCase("Female")) {
                        gender = "Male";
                        gender_switch.setImageDrawable(getResources().getDrawable(R.drawable.female));
                    }
                }
                }
        });

        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject parent = new JSONObject();
                name_str = name.getText().toString();
                nickname_str = nickName.getText().toString();
                familyname_str = familyName.getText().toString();
                if(name_str.length() == 0){
                    if(language.equalsIgnoreCase("En")) {
                        name.setError("Please enter Name");
                    }else if(language.equalsIgnoreCase("Ar")){
                        name.setError("من فضلك ارسل الاسم بالكامل");
                    }
                }else{
                    try {
                        JSONArray mainItem = new JSONArray();

                        JSONObject mainObj = new JSONObject();
                        mainObj.put("FullName",name_str);
                        mainObj.put("FamilyName", familyname_str);
                        mainObj.put("NickName", nickname_str);
                        mainObj.put("Gender", gender);
                        mainObj.put("Email", email_str);
                        mainObj.put("DeviceToken", SplashScreenActivity.regId);
                        mainObj.put("Language", language);
                        mainObj.put("UserId", userId);
                        mainItem.put(mainObj);

                        parent.put("UserProfile", mainItem);
                        Log.i("TAG", parent.toString());
                    }catch (JSONException je){

                    }
                    new InsertRegistration().execute(parent.toString());
                }
            }
        });
    }

    public class InsertRegistration extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(EditProfile.this);
            dialog = new MaterialDialog.Builder(EditProfile.this)
                    .title(R.string.app_name)
                    .content("Please wait...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPut httpPost = new HttpPut(Constants.UPDATE_PROFILE_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(EditProfile.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        Log.i("TAG", "user response:" + result);
                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String fullName = jo1.getString("FullName");
                                String gender = jo1.getString("Gender");
                                String email = jo1.getString("Email");
                                String mobile = jo1.getString("Mobile");
                                String Language = jo1.getString("Language");
                                String nickname = jo1.getString("NickName");
                                String familyName = jo1.getString("FamilyName");

                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", Language);
                                    jsonObject.put("gender", gender);
                                    jsonObject.put("NickName", nickname);
                                    jsonObject.put("FamilyName", familyName);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

                                    Toast.makeText(EditProfile.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                                    finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
                                je.printStackTrace();

                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EditProfile.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);
                                yes.setText(getResources().getString(R.string.ok));
                                desc.setText(result);

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(EditProfile.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
    }

    public final static boolean isValidEmail(CharSequence target) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();

    }
}
