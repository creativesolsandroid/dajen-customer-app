package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.R;

/**
 * Created by CS on 26-04-2017.
 */

public class BBQatHome extends Activity implements View.OnClickListener{

    TextView main_menu_text, testing_menu_text, bbqathome_text;
    ImageView main_menu_image, testing_menu_image, bbqathome_image, back_btn;
    LinearLayout main_menu, testing_menu, bbqathome;
    RelativeLayout menu1, menu2, menu3, menu4, menu5, menu6, menu7, menu8;
    TextView menu1_count, menu2_count, menu3_count, menu4_count, menu5_count, menu6_count, menu7_count, menu8_count;
    String menuType;
    TextView cart_count;
    RelativeLayout cart, website;
    private DataBaseHelper myDbHelper;
    AlertDialog customDialog;
    SharedPreferences languagePrefs;
    SharedPreferences userPrefs;
    String language;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.bbq_at_home);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.bbq_at_home_ar);
        }

        Constants.CurrentOrderActivity = "bbq";

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        myDbHelper = new DataBaseHelper(getApplicationContext());

        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        menuType = getIntent().getStringExtra("type");

        main_menu = (LinearLayout) findViewById(R.id.main_menu);
        testing_menu = (LinearLayout) findViewById(R.id.testing_menu);
        bbqathome = (LinearLayout) findViewById(R.id.bbqathome);

        main_menu_image = (ImageView) findViewById(R.id.main_menu_image);
        testing_menu_image = (ImageView) findViewById(R.id.testing_menu_image);
        bbqathome_image = (ImageView) findViewById(R.id.bbqathome_image);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        main_menu_text = (TextView) findViewById(R.id.main_menu_text);
        testing_menu_text = (TextView) findViewById(R.id.testing_menu_text);
        bbqathome_text = (TextView) findViewById(R.id.bbqathome_text);

        website = (RelativeLayout) findViewById(R.id.website);
        cart = (RelativeLayout) findViewById(R.id.cart);
        cart_count = (TextView) findViewById(R.id.cart_count);

        menu1_count = (TextView) findViewById(R.id.menu1_count);
        menu2_count = (TextView) findViewById(R.id.menu2_count);
        menu3_count = (TextView) findViewById(R.id.menu3_count);
        menu4_count = (TextView) findViewById(R.id.menu4_count);
        menu5_count = (TextView) findViewById(R.id.menu5_count);
        menu6_count = (TextView) findViewById(R.id.menu6_count);
        menu7_count = (TextView) findViewById(R.id.menu7_count);
        menu8_count = (TextView) findViewById(R.id.menu8_count);

        menu1 = (RelativeLayout) findViewById(R.id.menu1);
        menu2 = (RelativeLayout) findViewById(R.id.menu2);
        menu3 = (RelativeLayout) findViewById(R.id.menu3);
        menu4 = (RelativeLayout) findViewById(R.id.menu4);
        menu5 = (RelativeLayout) findViewById(R.id.menu5);
        menu6 = (RelativeLayout) findViewById(R.id.menu6);
        menu7 = (RelativeLayout) findViewById(R.id.menu7);
        menu8 = (RelativeLayout) findViewById(R.id.menu8);

        menu1.setOnClickListener(this);
        menu2.setOnClickListener(this);
        menu3.setOnClickListener(this);
        menu4.setOnClickListener(this);
        menu5.setOnClickListener(this);
        menu6.setOnClickListener(this);
        menu7.setOnClickListener(this);
        menu8.setOnClickListener(this);
        back_btn.setOnClickListener(this);

        main_menu.setOnClickListener(this);
        testing_menu.setOnClickListener(this);
        bbqathome.setOnClickListener(this);
        cart.setOnClickListener(this);
        website.setOnClickListener(this);

//        try {
//            if(menuType.equals("main")){
//                MainmenuSelected();
//            }
//            else if(menuType.equals("test")){
//                TestingMenuSelected();
//            }
//            else if(menuType.equals("bbq")){
                BBQatHomeSelected();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.menu1:
                Constants.Itemid = 12;
                Intent intent1 = new Intent(BBQatHome.this,ProductActivity.class);
                intent1.putExtra("title",""+getResources().getString(R.string.salads));
                intent1.putExtra("id",12);
                startActivity(intent1);
                break;

            case R.id.menu2:
                Intent menuIntent = new Intent(BBQatHome.this, BBQatHomeMenu.class);
                startActivity(menuIntent);
                break;

            case R.id.menu3:
                Constants.Itemid = 13;
                Intent intent2 = new Intent(BBQatHome.this,ProductActivity.class);
                intent2.putExtra("title",""+getResources().getString(R.string.salads));
                intent2.putExtra("id",13);
                startActivity(intent2);
                break;

            case R.id.menu4:
                Constants.Itemid = 5;
                Intent saladsIntent = new Intent(BBQatHome.this,ProductActivity.class);
                saladsIntent.putExtra("title",""+getResources().getString(R.string.salads));
                saladsIntent.putExtra("id",5);
                startActivity(saladsIntent);

                break;

            case R.id.menu5:
                Constants.Itemid = 4;
                Intent broastedIntent = new Intent(BBQatHome.this,ProductActivity.class);
                broastedIntent.putExtra("title",""+getResources().getString(R.string.rice)+" "+getResources().getString(R.string.macaroni));
                broastedIntent.putExtra("id",4);
                startActivity(broastedIntent);
                break;

            case R.id.menu6:
                Constants.Itemid = 6;
                Intent riceIntent = new Intent(BBQatHome.this,ProductActivity.class);
                riceIntent.putExtra("title",""+getResources().getString(R.string.sideitems));
                riceIntent.putExtra("id",6);
                startActivity(riceIntent);
                break;

            case R.id.menu7:
                Constants.Itemid = 8;
                Intent intent7 = new Intent(BBQatHome.this,ProductActivity.class);
                intent7.putExtra("title",""+getResources().getString(R.string.deserts));
                intent7.putExtra("id",8);
                startActivity(intent7);
                break;

            case R.id.menu8:
                Constants.Itemid = 14;
                Intent beverageIntent = new Intent(BBQatHome.this,ProductActivity.class);
                beverageIntent.putExtra("title",""+getResources().getString(R.string.beverages));
                beverageIntent.putExtra("id",14);
                startActivity(beverageIntent);
                break;

            case R.id.main_menu:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHome.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.yes));
                        no.setText(getResources().getString(R.string.no));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " BBQ item(s) in your bag, by this action all items get clear. Do you want to continue with Main Menu?");
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.yes_ar));
                        no.setText(getResources().getString(R.string.no_ar));
                        desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من قائمة ال BBQ في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع القائمة الرئيسية ؟");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            startActivity(new Intent(BBQatHome.this, MenuActivity.class));
                            finish();
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else{
                    startActivity(new Intent(BBQatHome.this, MenuActivity.class));
                    finish();
                }
                break;

            case R.id.testing_menu:
                TestingMenuSelected();
                break;

            case R.id.bbqathome:
//                Intent menuIntent = new Intent(BBQatHome.this, BBQatHomeMenu.class);
//                startActivity(menuIntent);
//                finish();
                break;

            case R.id.website:
                Intent AboutIntent = new Intent(BBQatHome.this, MoreWebView.class);
                AboutIntent.putExtra("class","menu");
                AboutIntent.putExtra("title","Dajen");
                AboutIntent.putExtra("url","http://www.dajen-chain.com/");
                AboutIntent.putExtra("webview_toshow","main");
                startActivity(AboutIntent);
                break;

            case R.id.back_btn:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHome.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.yes));
                        no.setText(getResources().getString(R.string.no));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.yes_ar));
                        no.setText(getResources().getString(R.string.no_ar));
                        desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            startActivity(new Intent(BBQatHome.this, MenuType.class));
                            finish();
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else{
                    startActivity(new Intent(BBQatHome.this, MenuType.class));
                    finish();
                }
                break;

            case R.id.cart:
                if(myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHome.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.cart_noitems));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.cart_noitems_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHome.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.cart_clear));
                        no.setText(getResources().getString(R.string.checkout_title));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.cart_clear_ar));
                        no.setText(getResources().getString(R.string.checkout_title_ar));
                        desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            setCount();
                            FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
                            cart_count.setText("" + myDbHelper.getTotalOrderQty());
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            Intent checkoutIntent = new Intent(BBQatHome.this, CheckoutActivity.class);
                            startActivity(checkoutIntent);

                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;
        }
    }

    public void MainmenuSelected(){
        main_menu.setBackgroundResource(R.drawable.menu_selected);
        testing_menu.setBackgroundResource(R.drawable.menu_unselected);
        bbqathome.setBackgroundResource(R.drawable.menu_unselected);

        main_menu_text.setTextColor(Color.parseColor("#000000"));
        testing_menu_text.setTextColor(Color.parseColor("#ffffff"));
        bbqathome_text.setTextColor(Color.parseColor("#ffffff"));

        main_menu_image.setImageResource(R.drawable.menu_main_selected);
        testing_menu_image.setImageResource(R.drawable.home_testing_menu);
        bbqathome_image.setImageResource(R.drawable.home_bbq);
    }

    public void TestingMenuSelected(){

        if(myDbHelper.getTotalOrderQty() > 0) {
            if(userPrefs.getString("menu","").equals("bbq")){

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHome.this);
                // ...Irrelevant code for customizing the buttons and title
                LayoutInflater inflater = getLayoutInflater();
                int layout = R.layout.alert_dialog;
                View dialogView = inflater.inflate(layout, null);
                dialogBuilder.setView(dialogView);
                dialogBuilder.setCancelable(false);

                TextView title = (TextView) dialogView.findViewById(R.id.title);
                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                View vert = (View) dialogView.findViewById(R.id.vert_line);

                if(language.equalsIgnoreCase("En")) {
                    title.setText(getResources().getString(R.string.dajen));
                    yes.setText(getResources().getString(R.string.yes));
                    no.setText(getResources().getString(R.string.no));
                    desc.setText("You have " + myDbHelper.getTotalOrderQty() + " BBQ item(s) in your bag, by this action all items get clear. Do you want to continue with Main Menu?");
                }
                else{
                    title.setText(getResources().getString(R.string.dajen_ar));
                    yes.setText(getResources().getString(R.string.yes_ar));
                    no.setText(getResources().getString(R.string.no_ar));
                    desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من قائمة ال BBQ في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع القائمة الرئيسية ؟");
                     }

                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                        myDbHelper.deleteOrderTable();
                        Constants.MENU_TYPE = "main";
                        Constants.Itemid = 10;
                        Intent crispIntent = new Intent(BBQatHome.this,TestingMenuActivity.class);
                        startActivity(crispIntent);
                        finish();
                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        customDialog.dismiss();
                    }
                });

                customDialog = dialogBuilder.create();
                customDialog.show();
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = customDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                //This makes the dialog take up the full width
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int screenWidth = size.x;

                double d = screenWidth*0.85;
                lp.width = (int) d;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
            }
            else {
                Constants.MENU_TYPE = "main";
                Constants.Itemid = 10;
                Intent crispIntent = new Intent(BBQatHome.this,TestingMenuActivity.class);
                startActivity(crispIntent);
                finish();
            }
        }
        else {
            Constants.MENU_TYPE = "main";
            Constants.Itemid = 10;
            Intent crispIntent = new Intent(BBQatHome.this,TestingMenuActivity.class);
            startActivity(crispIntent);
            finish();
        }
    }

    public void BBQatHomeSelected(){
        main_menu.setBackgroundResource(R.drawable.menu_unselected);
        testing_menu.setBackgroundResource(R.drawable.menu_unselected);
        bbqathome.setBackgroundResource(R.drawable.menu_selected);

        main_menu_text.setTextColor(Color.parseColor("#ffffff"));
        testing_menu_text.setTextColor(Color.parseColor("#ffffff"));
        bbqathome_text.setTextColor(Color.parseColor("#000000"));

        main_menu_image.setImageResource(R.drawable.home_main_menu);
        testing_menu_image.setImageResource(R.drawable.home_testing_menu);
        bbqathome_image.setImageResource(R.drawable.bbq_selected);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.MENU_TYPE = "bbq";
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());

        cart_count.setText(""+myDbHelper.getTotalOrderQty());
        setCount();
    }

    @Override
    public void onBackPressed() {
        if(myDbHelper.getTotalOrderQty() > 0) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(BBQatHome.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.alert_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            TextView title = (TextView) dialogView.findViewById(R.id.title);
            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
            View vert = (View) dialogView.findViewById(R.id.vert_line);

            if(language.equalsIgnoreCase("En")) {
                title.setText(getResources().getString(R.string.dajen));
                yes.setText(getResources().getString(R.string.yes));
                no.setText(getResources().getString(R.string.no));
                desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
            }
            else{
                title.setText(getResources().getString(R.string.dajen_ar));
                yes.setText(getResources().getString(R.string.yes_ar));
                no.setText(getResources().getString(R.string.no_ar));
                desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
            }

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customDialog.dismiss();
                    myDbHelper.deleteOrderTable();

                    startActivity(new Intent(BBQatHome.this, MenuType.class));
                    finish();
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customDialog.dismiss();
                }
            });

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth*0.85;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }
        else{
            startActivity(new Intent(BBQatHome.this, MenuType.class));
            finish();
        }
    }

    public void setCount(){
        if(myDbHelper.getCatOrderCount("9")>0){
            menu1_count.setText(""+myDbHelper.getCatOrderCount("9"));
        }
        else{
            menu1_count.setText("");
        }

        if(myDbHelper.getCatOrderCount("4")>0){
            menu3_count.setText(""+myDbHelper.getCatOrderCount("4"));
        }
        else{
            menu3_count.setText("");
        }

        if(myDbHelper.getCatOrderCount("6")>0){
            menu4_count.setText(""+myDbHelper.getCatOrderCount("6"));
        }
        else{
            menu4_count.setText("");
        }

        if(myDbHelper.getCatOrderCount("8")>0){
            menu5_count.setText(""+myDbHelper.getCatOrderCount("8"));
        }
        else{
            menu5_count.setText("");
        }

        if(myDbHelper.getCatOrderCount("5")>0){
            menu2_count.setText(""+myDbHelper.getCatOrderCount("5"));
        }
        else{
            menu2_count.setText("");
        }

        if(myDbHelper.getCatOrderCount("14")>0){
            menu8_count.setText(""+myDbHelper.getCatOrderCount("14"));
        }
        else{
            menu8_count.setText("");
        }
    }
}
