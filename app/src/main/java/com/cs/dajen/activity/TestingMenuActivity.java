package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Adapters.TestingMenuItemsAdapter;
import com.cs.dajen.Constants;
import com.cs.dajen.DataBaseHelper;
import com.cs.dajen.JSONParser;
import com.cs.dajen.Models.MenuItems;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by CS on 27-04-2017.
 */

public class TestingMenuActivity extends Activity implements View.OnClickListener{

    TextView title;
    GridView gridView;
    int Itemid;
    ImageView back_btn;
    Context ctx;
    TestingMenuItemsAdapter menuItemsAdapter;
    public static long sec;
    TextView footer_qty, footer_amount, footer_addmore, footer_checkout;
    private DataBaseHelper myDbHelper;
    TextView cart_count;
    RelativeLayout cart;
    AlertDialog customDialog;
    SharedPreferences languagePrefs;
    String language;
    SharedPreferences userPrefs;
    LinearLayout main_menu, testing_menu, bbqathome;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.testing_menu);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.testing_menu_ar);
        }
        ctx = this;

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);

        Constants.CurrentOrderActivity = "testing";
        FooterActivity footer = (FooterActivity) findViewById(R.id.layoutFooter);
        footer.setActivity(this);
        FooterActivity.tabBarPosition = 2;

        if(language.equalsIgnoreCase("En")){
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar2);
        }
        else{
            FooterActivity.tabbar.setBackgroundResource(R.drawable.tabbar3);
        }

        myDbHelper = new DataBaseHelper(getApplicationContext());

        Itemid = 10;

        main_menu = (LinearLayout) findViewById(R.id.main_menu);
        testing_menu = (LinearLayout) findViewById(R.id.testing_menu);
        bbqathome = (LinearLayout) findViewById(R.id.bbqathome);

        title = (TextView) findViewById(R.id.title);
        gridView = (GridView) findViewById(R.id.products_grid);
        back_btn = (ImageView) findViewById(R.id.back_btn);

        footer_addmore = (TextView) findViewById(R.id.footer_addmore);
        footer_qty = (TextView) findViewById(R.id.footer_qty);
        footer_amount = (TextView) findViewById(R.id.footer_amount);
        footer_checkout = (TextView) findViewById(R.id.footer_checkout);

        cart = (RelativeLayout) findViewById(R.id.cart);
        cart_count = (TextView) findViewById(R.id.cart_count);

        new GetMenuItems().execute(Constants.GET_ITEMS+Itemid);

        footer_addmore.setOnClickListener(this);
        footer_checkout.setOnClickListener(this);
        back_btn.setOnClickListener(this);
        cart.setOnClickListener(this);
        main_menu.setOnClickListener(this);
        testing_menu.setOnClickListener(this);
        bbqathome.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.main_menu:
                Constants.MENU_TYPE = "main";
                startActivity(new Intent(TestingMenuActivity.this, MenuActivity.class));
                finish();
                break;

            case R.id.bbqathome:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    if(userPrefs.getString("menu","").equals("main")){

                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuActivity.this);
                        // ...Irrelevant code for customizing the buttons and title
                        LayoutInflater inflater = getLayoutInflater();
                        int layout = R.layout.alert_dialog;
                        View dialogView = inflater.inflate(layout, null);
                        dialogBuilder.setView(dialogView);
                        dialogBuilder.setCancelable(false);

                        TextView title = (TextView) dialogView.findViewById(R.id.title);
                        TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                        TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                        TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                        View vert = (View) dialogView.findViewById(R.id.vert_line);

                        if(language.equalsIgnoreCase("En")) {
                            title.setText(getResources().getString(R.string.dajen));
                            yes.setText(getResources().getString(R.string.yes));
                            no.setText(getResources().getString(R.string.no));
                            desc.setText("You have " + myDbHelper.getTotalOrderQty() + " Main Menu item(s) in your bag, by this action all items get clear. Do you want to continue with BBQ Menu?");
                        }
                        else{
                            title.setText(getResources().getString(R.string.dajen_ar));
                            yes.setText(getResources().getString(R.string.yes_ar));
                            no.setText(getResources().getString(R.string.no_ar));
                            desc.setText(" لديك" + myDbHelper.getTotalOrderQty() + " طلبات من القائمة الرئيسية  في حقيبتك، من خلال هذه الخطوة كل الطلبات تكون جاهزة، هل تريد الاستمرار مع قائمة ال BBQ ؟");

                        }

                        yes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                                myDbHelper.deleteOrderTable();
                                Constants.MENU_TYPE = "bbq";
                                startActivity(new Intent(TestingMenuActivity.this, BBQatHome.class));
                                finish();
                            }
                        });

                        no.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                customDialog.dismiss();
                            }
                        });

                        customDialog = dialogBuilder.create();
                        customDialog.show();
                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = customDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        //This makes the dialog take up the full width
                        Display display = getWindowManager().getDefaultDisplay();
                        Point size = new Point();
                        display.getSize(size);
                        int screenWidth = size.x;

                        double d = screenWidth*0.85;
                        lp.width = (int) d;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

                    }
                    else{
                        Constants.MENU_TYPE = "bbq";
                        startActivity(new Intent(TestingMenuActivity.this, BBQatHome.class));
                        finish();
                    }
                }
                else {
                    Constants.MENU_TYPE = "bbq";
                    startActivity(new Intent(TestingMenuActivity.this, BBQatHome.class));
                    finish();
                }
                break;

            case R.id.footer_addmore:
                Intent mainIntent1 = new Intent(TestingMenuActivity.this, MenuActivity.class);
                mainIntent1.putExtra("type","main");
                startActivity(mainIntent1);
                finish();
                break;

            case R.id.back_btn:
                if(myDbHelper.getTotalOrderQty() > 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.yes));
                        no.setText(getResources().getString(R.string.no));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.yes_ar));
                        no.setText(getResources().getString(R.string.no_ar));
                        desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            menuItemsAdapter.notifyDataSetChanged();
                            startActivity(new Intent(TestingMenuActivity.this, MenuType.class));
                            finish();
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else{
                    startActivity(new Intent(TestingMenuActivity.this, MenuType.class));
                    finish();
                }
                break;

            case R.id.footer_checkout:
                if(myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.cart_noitems));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.cart_noitems_ar));
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else {
                    Intent checkoutIntent = new Intent(TestingMenuActivity.this, CheckoutActivity.class);
                    checkoutIntent.putExtra("class","product");
                    startActivity(checkoutIntent);
                }
                break;

            case R.id.cart:
                if(myDbHelper.getTotalOrderQty() == 0) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    vert.setVisibility(View.GONE);
                    no.setVisibility(View.GONE);
                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.ok));
                        desc.setText(getResources().getString(R.string.cart_noitems));
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.ok_ar));
                        desc.setText(getResources().getString(R.string.cart_noitems_ar));
                    }
                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                else {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuActivity.this);
                    // ...Irrelevant code for customizing the buttons and title
                    LayoutInflater inflater = getLayoutInflater();
                    int layout = R.layout.alert_dialog;
                    View dialogView = inflater.inflate(layout, null);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(false);

                    TextView title = (TextView) dialogView.findViewById(R.id.title);
                    TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                    TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                    TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                    View vert = (View) dialogView.findViewById(R.id.vert_line);

                    if(language.equalsIgnoreCase("En")) {
                        title.setText(getResources().getString(R.string.dajen));
                        yes.setText(getResources().getString(R.string.cart_clear));
                        no.setText(getResources().getString(R.string.checkout_title));
                        desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
                    }
                    else{
                        title.setText(getResources().getString(R.string.dajen_ar));
                        yes.setText(getResources().getString(R.string.cart_clear_ar));
                        no.setText(getResources().getString(R.string.checkout_title_ar));
                        desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
                    }

                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            myDbHelper.deleteOrderTable();
                            FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
                            cart_count.setText("" + myDbHelper.getTotalOrderQty());
                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customDialog.dismiss();
                            Intent checkoutIntent = new Intent(TestingMenuActivity.this, CheckoutActivity.class);
                            checkoutIntent.putExtra("class","product");
                            startActivity(checkoutIntent);

                        }
                    });

                    customDialog = dialogBuilder.create();
                    customDialog.show();
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = customDialog.getWindow();
                    lp.copyFrom(window.getAttributes());
                    //This makes the dialog take up the full width
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int screenWidth = size.x;

                    double d = screenWidth*0.85;
                    lp.width = (int) d;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
                break;
        }
    }

    public class GetMenuItems extends AsyncTask<String, Integer, String>{

        String response;
        String  networkStatus;
        MaterialDialog dialog;
        @Override
        protected void onPreExecute() {
            response = null;
            networkStatus = NetworkUtil.getConnectivityStatusString(getApplicationContext());
            Constants.menuItems.clear();
            dialog = new MaterialDialog.Builder(ctx)
                    .title(R.string.app_name)
                    .content("Loading Items...")
                    .progress(true, 0)
                    .cancelable(false)
                    .progressIndeterminateStyle(true)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .show();
        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                JSONParser jParser = new JSONParser();

                response = jParser
                        .getJSONFromUrl(params[0]);
                Log.i("TAG", "items response:" + response);
                return response;
            }else {
                return "no internet";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if (result.equals("")) {
                        Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            JSONArray jA = new JSONArray(result);
                            for (int i =0; i<jA.length(); i++){
                                JSONObject jsonObject = jA.getJSONObject(i);
                                MenuItems items = new MenuItems();

                                JSONObject keyObj = jsonObject.getJSONObject("Key");
                                items.setCatId(keyObj.getString("CategoryId"));
                                items.setItemId(keyObj.getString("ItemId"));
                                items.setItemName(keyObj.getString("ItemName"));
                                items.setModifierID(keyObj.getString("ModifierId"));
                                items.setItemName_ar(keyObj.getString("ItemName_Ar"));
                                items.setDesc(keyObj.getString("Description"));
                                items.setDesc_ar(keyObj.getString("Description_Ar"));
                                items.setAdditionalId(keyObj.getString("AdditionalsId"));
                                items.setImage(keyObj.getString("Images"));

                                int count = 0;
                                JSONObject priceObj = jsonObject.getJSONObject("Value");
                                if(priceObj.has("1")){
                                    count = count+1;
                                    items.setPrice1(priceObj.getString("1"));
                                }
                                else{
                                    items.setPrice1("-1");
                                }

                                if(priceObj.has("2")){
                                    count = count+1;
                                    items.setPrice2(priceObj.getString("2"));
                                }
                                else{
                                    items.setPrice2("-1");
                                }

                                if(priceObj.has("3")){
                                    count = count+1;
                                    items.setPrice3(priceObj.getString("3"));
                                }
                                else{
                                    items.setPrice3("-1");
                                }

                                if(priceObj.has("4")){
                                    count = count+1;
                                    items.setPrice4(priceObj.getString("4"));
                                }
                                else{
                                    items.setPrice4("-1");
                                }

                                if(priceObj.has("5")){
                                    count = count+1;
                                    items.setPrice5(priceObj.getString("5"));
                                }
                                else{
                                    items.setPrice5("-1");
                                }

                                if(priceObj.has("6")){
                                    count = count+1;
                                    items.setPrice6(priceObj.getString("6"));
                                }
                                else{
                                    items.setPrice6("-1");
                                }

                                if(priceObj.has("7")){
                                    count = count+1;
                                    items.setPrice7(priceObj.getString("7"));
                                }
                                else{
                                    items.setPrice7("-1");
                                }

                                if(count>1){
                                    items.setTwoCount(true);
                                }
                                else{
                                    items.setTwoCount(false);
                                }
                                Constants.menuItems.add(items);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                menuItemsAdapter.notifyDataSetChanged();
            }
            else {
                Toast.makeText(getApplicationContext(), "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        FooterActivity.mBadge.setNumber(myDbHelper.getTotalOrderQty());
        footer_qty.setText(""+myDbHelper.getTotalOrderQty());
        cart_count.setText(""+myDbHelper.getTotalOrderQty());
        footer_amount.setText(""+Constants.decimalFormat.format(myDbHelper.getTotalOrderPrice()));

        menuItemsAdapter = new TestingMenuItemsAdapter(TestingMenuActivity.this, Constants.menuItems, language);
        gridView.setAdapter(menuItemsAdapter);
        menuItemsAdapter.notifyDataSetChanged();
    }
    @Override
    public void onBackPressed() {
        if(myDbHelper.getTotalOrderQty() > 0) {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TestingMenuActivity.this);
            // ...Irrelevant code for customizing the buttons and title
            LayoutInflater inflater = getLayoutInflater();
            int layout = R.layout.alert_dialog;
            View dialogView = inflater.inflate(layout, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            TextView title = (TextView) dialogView.findViewById(R.id.title);
            TextView desc = (TextView) dialogView.findViewById(R.id.desc);
            TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
            TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
            View vert = (View) dialogView.findViewById(R.id.vert_line);

            if(language.equalsIgnoreCase("En")) {
                title.setText(getResources().getString(R.string.dajen));
                yes.setText(getResources().getString(R.string.yes));
                no.setText(getResources().getString(R.string.no));
                desc.setText("You have " + myDbHelper.getTotalOrderQty() + " item(s) in your bag, by this action all items get clear");
            }
            else{
                title.setText(getResources().getString(R.string.dajen_ar));
                yes.setText(getResources().getString(R.string.yes_ar));
                no.setText(getResources().getString(R.string.no_ar));
                desc.setText("لديك" + myDbHelper.getTotalOrderQty() + "منتج في الحقيبة ، من خلال هذا الاجراء ستصبح حقيبتك خالية من المنتجات");
            }

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customDialog.dismiss();
                    myDbHelper.deleteOrderTable();
                    startActivity(new Intent(TestingMenuActivity.this, MenuType.class));
                    finish();
                }
            });

            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    customDialog.dismiss();
                }
            });

            customDialog = dialogBuilder.create();
            customDialog.show();
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = customDialog.getWindow();
            lp.copyFrom(window.getAttributes());
            //This makes the dialog take up the full width
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int screenWidth = size.x;

            double d = screenWidth*0.85;
            lp.width = (int) d;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
        }
        else{
            startActivity(new Intent(TestingMenuActivity.this, MenuType.class));
            finish();
        }
    }
}
