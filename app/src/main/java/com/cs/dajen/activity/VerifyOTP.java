package com.cs.dajen.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.cs.dajen.Constants;
import com.cs.dajen.NetworkUtil;
import com.cs.dajen.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import swarajsaaj.smscodereader.interfaces.OTPListener;
import swarajsaaj.smscodereader.receivers.OtpReader;

import static android.R.attr.phoneNumber;

/**
 * Created by cs android on 07-02-2017.
 */

public class VerifyOTP extends Activity implements OTPListener {

    String mobilenumber;
    TextView mMobile_tv;
    EditText motp_et;
    LinearLayout verify_btn;
    String userOTP,serverOTP,userData;
    private String response = null;
    SharedPreferences userPrefs;
    SharedPreferences.Editor userPrefEditor;
    boolean forgot;
    AlertDialog customDialog;
    SharedPreferences languagePrefs;
    String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        languagePrefs = getSharedPreferences("LANGUAGE_PREFS", Context.MODE_PRIVATE);
        language = languagePrefs.getString("language", "En");

        if(language.equalsIgnoreCase("En")) {
            setContentView(R.layout.verify_random_number);
        }else if(language.equalsIgnoreCase("Ar")){
            setContentView(R.layout.verify_random_number_ar);
        }

        userPrefs = getSharedPreferences("USER_PREFS", Context.MODE_PRIVATE);
        userPrefEditor  = userPrefs.edit();

        mMobile_tv = (TextView) findViewById(R.id.mobile_number);
        verify_btn = (LinearLayout) findViewById(R.id.verify_button);
        motp_et = (EditText) findViewById(R.id.number1);
        OtpReader.bind(this,"CS Test");

        if(!(ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED)) {
            Toast.makeText(VerifyOTP.this, "SMS read permission denied, Please enter OTP manually", Toast.LENGTH_LONG).show();
        }

//        SharedPreferences prefs = getSharedPreferences("user_details", MODE_PRIVATE);
//        mobilenumber = prefs.getString("mobile", "");
        serverOTP = getIntent().getStringExtra("OTP");
        userData = getIntent().getStringExtra("user_data");
        mobilenumber = getIntent().getExtras().getString("phone_number").replace(" ","");
        forgot = getIntent().getBooleanExtra("forgot", false);

        mMobile_tv.setText("+"+mobilenumber);

        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userOTP = motp_et.getText().toString();
                if(userOTP.length() == 0){
                    motp_et.setError("Please enter otp");
                    motp_et.requestFocus();
                }
                else if(!serverOTP.equals(userOTP)){
                    motp_et.setError("Enter correct otp");
                    motp_et.requestFocus();
                }
                else{
                    if(forgot){
                        Intent i = new Intent(VerifyOTP.this, ResetPassword.class);
                        i.putExtra("mobile", mobilenumber);
                        startActivity(i);
                        finish();
                    }else {
                        new RegisterUser().execute(userData);
                    }
                }
            }
        });

    }

    @Override
    public void otpReceived(String smsText) {
        //Do whatever you want to do with the text
        Log.d("Otp",smsText);
        String[] str = null;
        try {
            str = smsText.split(":");
//            if(str[1].contains("'")){
//                str[1].replace("'","");
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        motp_et.setText(""+str[1]);
        motp_et.setSelection(motp_et.length());
        if (str[1].equals(serverOTP)) {
            if (forgot) {
                Intent i = new Intent(VerifyOTP.this, ResetPassword.class);
                i.putExtra("mobile", phoneNumber);
                startActivity(i);
                finish();
            } else {
                new RegisterUser().execute(userData);
            }
        }

//        Toast.makeText(this,"Got "+smsText,Toast.LENGTH_LONG).show();
    }

    public class RegisterUser extends AsyncTask<String, Integer, String> {
        ProgressDialog pDialog;
        String  networkStatus;
        MaterialDialog dialog;

        InputStream inputStream = null;
        @Override
        protected void onPreExecute() {
            networkStatus = NetworkUtil.getConnectivityStatusString(VerifyOTP.this);
            dialog = new MaterialDialog.Builder(VerifyOTP.this)
                    .title(R.string.app_name)
                    .content("Registering user...")
                    .progress(true, 0)
                    .widgetColor(getResources().getColor(R.color.homecolor))
                    .progressIndeterminateStyle(true)
                    .show();


        }

        @Override
        protected String doInBackground(String... params) {
            if(!networkStatus.equalsIgnoreCase("Not connected to Internet")) {
                try {
                    try {

                        // 1. create HttpClient
                        HttpClient httpclient = new DefaultHttpClient();

                        // 2. make POST request to the given URL
                        HttpPost httpPost = new HttpPost(Constants.REGISTRATION_URL);



                        // ** Alternative way to convert Person object to JSON string usin Jackson Lib
                        // ObjectMapper mapper = new ObjectMapper();
                        // json = mapper.writeValueAsString(person);

                        // 5. set json to StringEntity
                        StringEntity se = new StringEntity(params[0], "UTF-8");

                        // 6. set httpPost Entity
                        httpPost.setEntity(se);

                        // 7. Set some headers to inform server about the type of the content
                        httpPost.setHeader("Accept", "application/json");
                        httpPost.setHeader("Content-type", "application/json");

                        // 8. Execute POST request to the given URL
                        HttpResponse httpResponse = httpclient.execute(httpPost);

                        // 9. receive response as inputStream
                        inputStream = httpResponse.getEntity().getContent();

                        // 10. convert inputstream to string
                        if(inputStream != null) {
                            response = convertInputStreamToString(inputStream);
                            Log.i("TAGs", "user response:" + response);
                            return response;
                        }

                    } catch (Exception e) {
                        Log.d("InputStream", e.getLocalizedMessage());
                    }
                } catch (Exception e) {
                    Log.e("Buffer Error", "Error converting result " + e.toString());
                }
                Log.i("TAG", "user response:" + response);
                return response;
            }else {
                return "no internet";
            }

        }


        @Override
        protected void onPostExecute(String result) {

            if (result != null) {
                if(result.equalsIgnoreCase("no internet")) {
                    if (language.equalsIgnoreCase("En")) {
                        Toast.makeText(getApplicationContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), R.string.connection_error_ar, Toast.LENGTH_SHORT).show();
                    }
                }else{
                    if(result.equals("")){
                        Toast.makeText(VerifyOTP.this, "cannot reach server", Toast.LENGTH_SHORT).show();
                    }else {

                        try {
                            JSONObject jo= new JSONObject(result);

                            try{
                                JSONArray ja = jo.getJSONArray("Success");
                                JSONObject jo1 = ja.getJSONObject(0);
                                String userId = jo1.getString("UserId");
                                String email = jo1.getString("Email");
                                String fullName = jo1.getString("FullName");
                                String mobile = jo1.getString("Mobile");
                                String language = jo1.getString("Language");
                                String gender = jo1.getString("Gender");
                                String nickname = jo1.getString("NickName");
                                String familyName = jo1.getString("FamilyName");
//                                boolean isVerified = jo1.getBoolean("IsVerified");


                                try {
                                    JSONObject parent = new JSONObject();
                                    JSONObject jsonObject = new JSONObject();
                                    JSONArray jsonArray = new JSONArray();
                                    jsonArray.put("lv1");
                                    jsonArray.put("lv2");

                                    jsonObject.put("userId", userId);
                                    jsonObject.put("fullName", fullName);
                                    jsonObject.put("mobile", mobile);
                                    jsonObject.put("email", email);
                                    jsonObject.put("language", language);
                                    jsonObject.put("gender", gender);
                                    jsonObject.put("NickName", nickname);
                                    jsonObject.put("FamilyName", familyName);
//                                    jsonObject.put("isVerified", isVerified);
                                    jsonObject.put("user_details", jsonArray);
                                    parent.put("profile", jsonObject);
                                    Log.d("output", parent.toString());
                                    userPrefEditor.putString("user_profile", parent.toString());
                                    userPrefEditor.putString("userId", userId);

//                                    userPrefEditor.putString("user_email", email);
//                                    userPrefEditor.putString("user_password", password);

                                    userPrefEditor.commit();

//                                    if(isVerified) {
                                    userPrefEditor.putString("login_status","loggedin");
                                    userPrefEditor.commit();
                                    setResult(RESULT_OK);
                                    finish();
//                                    Intent loginIntent = new Intent(VerifyOTP.this, HomeScreenActivity.class);
//                                    startActivity(loginIntent);
//                                    }



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }catch (JSONException je){
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(VerifyOTP.this);
                                // ...Irrelevant code for customizing the buttons and title
                                LayoutInflater inflater = getLayoutInflater();
                                int layout = R.layout.alert_dialog;
                                View dialogView = inflater.inflate(layout, null);
                                dialogBuilder.setView(dialogView);
                                dialogBuilder.setCancelable(false);

                                TextView desc = (TextView) dialogView.findViewById(R.id.desc);
                                TextView yes = (TextView) dialogView.findViewById(R.id.pos_btn);
                                TextView no = (TextView) dialogView.findViewById(R.id.ngt_btn);
                                View vert = (View) dialogView.findViewById(R.id.vert_line);

                                no.setVisibility(View.GONE);
                                vert.setVisibility(View.GONE);
                                yes.setText(getResources().getString(R.string.ok));
                                desc.setText(result);

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        customDialog.dismiss();
                                    }
                                });

                                customDialog = dialogBuilder.create();
                                customDialog.show();
                                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                                Window window = customDialog.getWindow();
                                lp.copyFrom(window.getAttributes());
                                //This makes the dialog take up the full width
                                Display display = getWindowManager().getDefaultDisplay();
                                Point size = new Point();
                                display.getSize(size);
                                int screenWidth = size.x;

                                double d = screenWidth*0.85;
                                lp.width = (int) d;
                                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                                window.setAttributes(lp);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }else {
                Toast.makeText(VerifyOTP.this, "cannot reach server", Toast.LENGTH_SHORT).show();
            }
            if(dialog != null) {
                dialog.dismiss();
            }

            super.onPostExecute(result);

        }

    }


    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
